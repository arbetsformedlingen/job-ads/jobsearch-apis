import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.taxonomy_replace_helper import ad_ids_from_response
from tests.test_resources.concept_ids.municipality import get_muncipalities_with_hits, \
    get_ten_random_municipalities_with_hits

# marks all tests as jobsearch
pytestmark = [pytest.mark.jobsearch]


@pytest.mark.slow
@pytest.mark.parametrize("municipality", get_muncipalities_with_hits())
def test_municipality_all( municipality):
    _check_municipality( municipality)


@pytest.mark.smoke
@pytest.mark.parametrize("municipality", get_ten_random_municipalities_with_hits())
def test_municipality_smoke_random( municipality):
    _check_municipality( municipality)


def _check_municipality( municipality):
    concept_id = municipality['id']
    response = get_search({'municipality': concept_id})

    assert (hits := response['hits'])
    ad_ids = ad_ids_from_response(response)
    assert ad_ids.sort() == municipality['ad_ids'].sort()
    for hit in hits:
        expected_municip = municipality['code']
        actual_municip = hit['workplace_address']['municipality_code']
        assert actual_municip == expected_municip, f"Expected {actual_municip} but got {expected_municip}"
        assert hit['workplace_address']['region_code'] == municipality[
            'region_code'], f"Expected {hit['workplace_address']['region_code']} but got {municipality['region_code']}"
