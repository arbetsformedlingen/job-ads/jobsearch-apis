import logging

log = logging.getLogger(__name__)

# Swedish Constants
OCCUPATION_SV = 'yrkesroll'
GROUP_SV = 'yrkesgrupp'
FIELD_SV = 'yrkesomrade'
COLLECTION_SV = 'yrkessamlingar'
SKILL_SV = 'kompetens'
PLACE_SV = 'plats'
MUNICIPALITY_SV = 'kommun'
REGION_SV = 'lan'
COUNTRY_SV = 'land'
LANGUAGE_SV = 'sprak'
WORKTIME_EXTENT_SV = 'arbetstidsomfattning'
EMPLOYMENT_TYPE_SV = 'anstallningstyp'
DRIVING_LICENCE_SV = 'korkort'
WAGE_TYPE_SV = 'lonetyp'
EDUCATION_LEVEL_SV = 'utbildningsniva'
EDUCATION_FIELD_SV = 'utbildningsinriktning'
DURATION_SV = 'varaktighet'
OCCUPATION_EXPERIENCE_SV = 'erfarenhetsniva'

# English Constants
OCCUPATION = 'occupation-name'
GROUP = 'occupation-group'
FIELD = 'occupation-field'
COLLECTION = 'occupation-collection'
SKILL = 'skill'
PLACE = 'place'
MUNICIPALITY = 'municipality'
REGION = 'region'
COUNTRY = 'country'
LANGUAGE = 'language'
WORKTIME_EXTENT = 'worktime-extent'
EMPLOYMENT_TYPE = 'employment-type'
DRIVING_LICENCE = 'driving-license'
DRIVING_LICENCE_REQUIRED = 'driving-license-required'
WAGE_TYPE = 'wage-type'
EDUCATION_LEVEL = 'educationlevel'
DEPRECATED_EDUCATION_LEVEL = 'deprecated_educationlevel'
EDUCATION_FIELD = 'educationfield'
DEPRECATED_EDUCATION_FIELD = 'deprecated_educationfield'
DURATION = 'employment-duration'
OCCUPATION_EXPERIENCE = 'experience'


# Valuestore


class JobtechTaxonomy:
    REGION = 'region'
    COUNTRY = 'country'
    DEPRECATED_EDUCATION_LEVEL = 'deprecated-education-level'
    DEPRECATED_EDUCATION_FIELD = 'deprecated-education-field'
    DRIVING_LICENCE = 'driving-license'
    EMPLOYMENT_DURATION = 'employment-duration'
    EMPLOYMENT_TYPE = 'employment-type'
    LANGUAGE = 'language'
    MUNICIPALITY = 'municipality'
    OCCUPATION_GROUP = 'occupation-group'
    OCCUPATION_EXPERIENCE_YEARS = 'occupation-experience-years'
    OCCUPATION_FIELD = 'occupation-field'
    OCCUPATION_NAME = 'occupation-name'
    SKILL = 'skill'
    SUN_EDUCATION_LEVEL_1 = 'sun-education-level-1'
    SUN_EDUCATION_LEVEL_2 = 'sun-education-level-2'
    SUN_EDUCATION_LEVEL_3 = 'sun-education-level-3'
    SUN_EDUCATION_FIELD_1 = 'sun-education-field-1'
    SUN_EDUCATION_FIELD_2 = 'sun-education-field-2'
    SUN_EDUCATION_FIELD_3 = 'sun-education-field-3'
    WAGE_TYPE = 'wage-type'
    WORKTIME_EXTENT = 'worktime-extent'


tax_type = {
    OCCUPATION: JobtechTaxonomy.OCCUPATION_NAME,
    OCCUPATION_SV: JobtechTaxonomy.OCCUPATION_NAME,
    'yrkesroll': JobtechTaxonomy.OCCUPATION_NAME,
    GROUP: JobtechTaxonomy.OCCUPATION_GROUP,
    GROUP_SV: JobtechTaxonomy.OCCUPATION_GROUP,
    FIELD: JobtechTaxonomy.OCCUPATION_FIELD,
    FIELD_SV: JobtechTaxonomy.OCCUPATION_FIELD,
    SKILL: JobtechTaxonomy.SKILL,
    SKILL_SV: JobtechTaxonomy.SKILL,
    MUNICIPALITY: JobtechTaxonomy.MUNICIPALITY,
    MUNICIPALITY_SV: JobtechTaxonomy.MUNICIPALITY,
    REGION: JobtechTaxonomy.REGION,
    REGION_SV: JobtechTaxonomy.REGION,
    COUNTRY: JobtechTaxonomy.COUNTRY,
    COUNTRY_SV: JobtechTaxonomy.COUNTRY,
    WORKTIME_EXTENT: JobtechTaxonomy.WORKTIME_EXTENT,
    WORKTIME_EXTENT_SV: JobtechTaxonomy.WORKTIME_EXTENT,
    PLACE: 'place',
    PLACE_SV: 'place',
    LANGUAGE: JobtechTaxonomy.LANGUAGE,
    LANGUAGE_SV: JobtechTaxonomy.LANGUAGE,
    EMPLOYMENT_TYPE: JobtechTaxonomy.EMPLOYMENT_TYPE,
    EMPLOYMENT_TYPE_SV: JobtechTaxonomy.EMPLOYMENT_TYPE,
    DRIVING_LICENCE: JobtechTaxonomy.DRIVING_LICENCE,
    DRIVING_LICENCE_SV: JobtechTaxonomy.DRIVING_LICENCE,
    WAGE_TYPE: JobtechTaxonomy.WAGE_TYPE,
    WAGE_TYPE_SV: JobtechTaxonomy.WAGE_TYPE,
    EDUCATION_LEVEL: JobtechTaxonomy.DEPRECATED_EDUCATION_LEVEL,
    EDUCATION_LEVEL_SV: JobtechTaxonomy.DEPRECATED_EDUCATION_LEVEL,
    EDUCATION_FIELD: JobtechTaxonomy.DEPRECATED_EDUCATION_FIELD,
    EDUCATION_FIELD_SV: JobtechTaxonomy.DEPRECATED_EDUCATION_FIELD,
    DURATION: JobtechTaxonomy.EMPLOYMENT_DURATION,
    DURATION_SV: JobtechTaxonomy.EMPLOYMENT_DURATION,
    OCCUPATION_EXPERIENCE: JobtechTaxonomy.OCCUPATION_EXPERIENCE_YEARS,
    OCCUPATION_EXPERIENCE_SV: JobtechTaxonomy.OCCUPATION_EXPERIENCE_YEARS,
}


annons_key_to_jobtech_taxonomy_key = {
    'yrkesroll': JobtechTaxonomy.OCCUPATION_NAME,
    OCCUPATION: JobtechTaxonomy.OCCUPATION_NAME,
    GROUP: JobtechTaxonomy.OCCUPATION_GROUP,
    FIELD: JobtechTaxonomy.OCCUPATION_FIELD,
    OCCUPATION_SV: JobtechTaxonomy.OCCUPATION_NAME,
    GROUP_SV: JobtechTaxonomy.OCCUPATION_GROUP,
    FIELD_SV: JobtechTaxonomy.OCCUPATION_FIELD,
    'anstallningstyp': JobtechTaxonomy.EMPLOYMENT_TYPE,
    'lonetyp': JobtechTaxonomy.WAGE_TYPE,
    'varaktighet': JobtechTaxonomy.EMPLOYMENT_DURATION,
    'arbetstidstyp': JobtechTaxonomy.WORKTIME_EXTENT,
    'kommun': JobtechTaxonomy.MUNICIPALITY,
    'lan': JobtechTaxonomy.REGION,
    'land': JobtechTaxonomy.COUNTRY,
    REGION: JobtechTaxonomy.REGION,
    COUNTRY: JobtechTaxonomy.COUNTRY,
    MUNICIPALITY: JobtechTaxonomy.MUNICIPALITY,
    'korkort': JobtechTaxonomy.DRIVING_LICENCE,
    'kompetens': JobtechTaxonomy.SKILL,
    SKILL: JobtechTaxonomy.SKILL,
    SKILL_SV: JobtechTaxonomy.SKILL,
    'sprak': JobtechTaxonomy.LANGUAGE,
    'deprecated_educationlevel': JobtechTaxonomy.DEPRECATED_EDUCATION_LEVEL,
    'deprecated_educationfield': JobtechTaxonomy.DEPRECATED_EDUCATION_FIELD,
}
