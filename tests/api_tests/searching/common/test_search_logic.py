import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.helper import get_search, get_search_check_number_of_results, check_freetext_concepts, \
    compare_multiple
from tests.test_resources.api_test_runner import run_test_case
from tests.test_resources.is_historical import historical

HISTORICAL = historical()


# TODO: adapt for historical ads test data

@pytest.mark.jobsearch
@pytest.mark.parametrize("query, municipality, code, municipality_concept_id, expected_number_of_hits", [
    ('bagare stockholm', 'Stockholm', '0180', geo.stockholm, 0),
    ('lärare stockholm', 'Stockholm', '0180', geo.stockholm, 14),
    ('lärare göteborg', 'Göteborg', '1480', geo.goteborg, 4),
])
def test_freetext_work_and_location_details( query, municipality, code, municipality_concept_id,
                                            expected_number_of_hits):
    params = {'q': query, 'limit': '100'}
    list_of_ads = get_search_check_number_of_results( expected_number_of_hits, params)
    compare_these = []
    for ad in list_of_ads:
        compare_these.extend([
            (ad['workplace_address']['municipality'], municipality, 'Wrong municipality'),
            (ad['workplace_address']['municipality_code'], code, 'Wrong code'),
            (ad['workplace_address']['municipality_concept_id'], municipality_concept_id, 'Wrong concept id'),
        ])
    compare_multiple(compare_these)


@pytest.mark.historical
@pytest.mark.parametrize("query, expected_ids_and_relevance", [
    ('sjuksköterska läkare Stockholm Göteborg', [
        ('2f6d035edba6e502682141996cdfdfe3b8cc0f65', 1.0),
        ('da0a229d4cf30bd099f72e1c1984231904c94824', 0.8673489355734281),
        ('3a3c57aa013c0263b6664f74a3f1e2fd6425d28e', 0.8673489355734281),
        ('df3e834b7be8d60c5f9a9e50e875125673d45d98', 0.8673489355734281),
        ('c9d6860077abad6e6698e820d142d9bc626a4980', 0.8377983096923707),
        ('9244d3969387ee8022acfdaf68ab14cfadd569af', 0.6913343074086772),
        ('320f5c95ef1b43fa04d3b652b7a304a227ddc787', 0.6913343074086772),
        ('908aa63e0a84432710409bddef69cc28a1a94ea8', 0.6913343074086772),
        ('aa54603f1cc7aa56fd3e367ded4abd06084c0228', 0.6913343074086772),
        ('230a31bc8261c1463ac3d7cbfa9c549ca37655ab', 0.6913343074086772),
    ])])
def test_freetext_two_work_and_two_locations_check_order_historical(query, expected_ids_and_relevance):
    """
    Tests that the sorting order of hits is as expected and that relevance value has not changed
    This documents current behavior
    """
    _freetext_two_work_and_two_locations_check_order(query, expected_ids_and_relevance)


@pytest.mark.jobsearch
@pytest.mark.parametrize("query, expected_ids_and_relevance", [
    ('sjuksköterska läkare Stockholm Göteborg', [
        ('24645625', 1.0),
        ('24648363', 0.7595474201549399),
        ('24648149', 0.7595474201549399),
        ('24645837', 0.7595474201549399),
        ('24643198', 0.7595474201549399),
        ('24642963', 0.7595474201549399),
        ('24636423', 0.7595474201549399),
        ('24631812', 0.7595474201549399),
        ('24627762', 0.7595474201549399),
        ('24624501', 0.7595474201549399)
    ])])
def test_freetext_two_work_and_two_locations_check_order_jobsearch(query, expected_ids_and_relevance):
    """
    Tests that the sorting order of hits is as expected and that relevance value has not changed
    This documents current behavior
    """
    _freetext_two_work_and_two_locations_check_order(query, expected_ids_and_relevance)


def _freetext_two_work_and_two_locations_check_order(query, expected_ids_and_relevance):
    params = {'q': query, 'limit': '10'}
    response_json = get_search(params)
    relevance = 0
    old_relevance = 1
    for index, hit in enumerate(response_json['hits']):
        relevance = hit['relevance']
        print(f"('{hit['id']}', {relevance}),")
    for index, hit in enumerate(response_json['hits']):
        assert old_relevance >= relevance  # check that results are presented in ascending relevance order
        assert hit['id'] == expected_ids_and_relevance[index][0]
        assert hit['relevance'] == expected_ids_and_relevance[index][1], hit['id']
        old_relevance = relevance


@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.parametrize("test_case", [
    {"params":
         {'q': 'bagare kock Stockholm Göteborg', 'limit': '100'}, "jobsearch": 4, "historical": 55,
     "jobsearch_top_id": '24599773', "historical_top_id": "a3adf1b4bc57067cbf1902809601f5b6fe76f8b9"},
    {"params":
         {'q': 'kock bagare Stockholm Göteborg', 'limit': '100'},
     "jobsearch": 4,
     "historical": 55,
     "jobsearch_top_id": '24599773',
     "historical_top_id": "a3adf1b4bc57067cbf1902809601f5b6fe76f8b9"},
    {"params":
         {'q': 'kallskänka kock Stockholm Göteborg', 'limit': '100'},
     "jobsearch": 4,
     "historical": 59,
     "jobsearch_top_id": '24599773',
     "historical_top_id": "790d31324803e58cb0d240251ffe0d5862728a78"}
])
def test_freetext_two_work_and_two_locations(test_case):
    """
    Test that the top hit for a search has not changed and that the number of hits for query has not changed
    This documents current behavior
    """
    hits = run_test_case(test_case)["hits"]
    top_id = hits[0]['id']
    if HISTORICAL:
        assert top_id == test_case["historical_top_id"]
    else:
        assert top_id == test_case["jobsearch_top_id"]


@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.parametrize("test_case", [
    {"params": {'q': 'Sirius crew'},
     "jobsearch": 4,
     "historical": 11,
     "jobsearch_top_id": '24625524',
     "historical_top_id": "a90a2273102361c0028996c93f1e14dddfac980a"},
    {"params": {'q': 'Säsong'},
     "jobsearch": 20,
     "historical": 51,
     "jobsearch_top_id": '24570725',
     "historical_top_id": "953c8a785ce835f02a5ad501b1a3805798645d18"}
])
def test_freetext_search(test_case):
    """
    Tests from examples
    Test that specific queries should return only one hit (identified by id)
    and that freetext concepts are not included in search result
    """
    response_json = run_test_case(test_case)
    top_id = response_json["hits"][0]['id']
    if HISTORICAL:
        assert top_id == test_case["historical_top_id"]
    else:
        assert top_id == test_case["jobsearch_top_id"]

    # freetext concepts should be empty
    check_freetext_concepts(response_json['freetext_concepts'], [[], [], [], [], [], [], [], [], []])


@pytest.mark.jobsearch
def test_search_rules():
    params = {'q': "systemutvecklare python java stockholm 'klarna bank ab'", 'limit': '1'}
    response_json = get_search(params=params)
    hit = response_json['hits'][0]
    check_freetext_concepts(response_json['freetext_concepts'],
                            [['python', 'java', 'bank'], ['systemutvecklare'], ['stockholm'], [], [], [], [], [], []])
    assert 'klarna' in hit['employer']['name'].lower()
    assert 'klarna' in hit['employer']['workplace'].lower()
    assert 'Systemutvecklare/Programmerare' in hit['occupation']['label']
    assert hit['workplace_address']['municipality'] == 'Stockholm'
