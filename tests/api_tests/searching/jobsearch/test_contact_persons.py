import pytest

from tests.test_resources.helper import get_by_id

# mark all tests in this file as @pytest.mark.jobsearch
pytestmark = [pytest.mark.jobsearch]


@pytest.mark.parametrize('ad_id, expected', [(23448227, 1), (23699999, 3), (23791205, 1), (23837044, 1), (23910624, 1)])
def test_search_freetext_and_occupation_name( ad_id, expected):
    """
    get ads which had union representatives in LA mock data (should not be imported)
    and check field 'application_contact'
    """
    json_response = get_by_id( ad_id)
    assert len(json_response['application_contacts']) == expected
    assert 'facklig representant' not in str(json_response['application_contacts'])
