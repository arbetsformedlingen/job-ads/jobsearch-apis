import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.taxonomy_replace_helper import ad_ids_from_response

from tests.test_resources.concept_ids.taxonomy_replace.test_data_with_expected_hits import \
    get_occupations_with_expected_hits

OCCUPATION = 'occupation-name'

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize('test_data', get_occupations_with_expected_hits())
def test_occupation( test_data):
    new_concept_id = test_data['replaced_by']
    response_new = get_search({OCCUPATION: new_concept_id})
    new_hits = ad_ids_from_response(response_new)

    old_hits = []
    for old_concept_id in test_data['old_replaced']:
        response_old = get_search({OCCUPATION: old_concept_id})
        old_hits.append(ad_ids_from_response(response_old))

    for hits in old_hits:
        assert new_hits.sort() == hits.sort()

    assert new_hits.sort() == test_data['hits'].sort()
