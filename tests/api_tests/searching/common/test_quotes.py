import pytest
from tests.test_resources.api_test_runner import run_test_case
from tests.test_resources.test_settings import EXPECTED_GYMNASIE_LARARE

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    pytest.param({'params': {'q': '"gymnasielärare"'}, 'jobsearch': 18, 'historical': 101, 'comment': 'a'},marks=pytest.mark.smoke),
    {'params': {'q': "'gymnasielärare'"}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'b'},
    {'params': {'q': '"gymnasielärare"'}, 'jobsearch': 18, 'historical': 101, 'comment': 'c'},
    {'params': {'q': "'gymnasielärare'"}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'd'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'e'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'f'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'g'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'h'},
    {'params': {'q': 'gymnasielärare"'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'i'},
   pytest.param( {'params': {'q': "gymnasielärare'"}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'j'},marks=pytest.mark.smoke),
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'k'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'l'},
    {'params': {'q': 'gymnasielärare lärare'}, 'jobsearch': 255, 'historical': 892, 'comment': 'm'},
    {'params': {'q': "'gymnasielärare'"}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'n'},
    {'params': {'q': '"gymnasielärare" "lärare"'}, 'jobsearch': 357, 'historical': 1265, 'comment': 'o'},
    {'params': {'q': '"gymnasielärare lärare"'}, 'jobsearch': 0, 'historical': 0, 'comment': 'p'},
    {'params': {'q': '"gymnasielärare"'}, 'jobsearch': 18, 'historical': 101, 'comment': 'q'},
    {'params': {'q': '"gymnasielärare'}, 'jobsearch': 18, 'historical': 101, 'comment': 'r'},
    {'params': {'q': '"gymnasielärare"'}, 'jobsearch': 18, 'historical': 101, 'comment': 's'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 't'},
    {'params': {'q': 'gymnasielärare'}, 'jobsearch': EXPECTED_GYMNASIE_LARARE, 'historical': 132, 'comment': 'u'},

])
def test_different_quote_styles(test_case):
    # Different quotes gives different results.
    # Comment is to make the test case identifiable if it fails (quotes are not obvious in the error printout)
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    pytest.param({'params': {'q': '"c++'}, 'jobsearch': 62, 'historical': 138},marks=pytest.mark.smoke),
    {'params': {'q': '"c++"'}, 'jobsearch': 62, 'historical': 138},
    {'params': {'q': '"c+'}, 'jobsearch': 41, 'historical': 95},
    {'params': {'q': '"c( '}, 'jobsearch': 40, 'historical': 93},
])
def test_cplusplus_in_quotes(test_case):
    """
    Test for a bug where some quotes caused an 'internal server error' response
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    pytest.param({'params': {'q': 'python stockholm'}, 'jobsearch': 34, 'historical': 17},marks=pytest.mark.smoke),
    {'params': {'q': '"python stockholm"'}, 'jobsearch': 0, 'historical': 0},
   pytest.param( {'params': {'q': '"python" "stockholm"'}, 'jobsearch': 957, 'historical': 3069},marks=pytest.mark.smoke),
    {'params': {'q': '"python" stockholm'}, 'jobsearch': 843, 'historical': 3344},
    {'params': {'q': 'python "stockholm"'}, 'jobsearch': 97, 'historical': 73},
    {'params': {'q': '"python job in stockholm"'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': '"work from home" python stockholm'}, 'jobsearch': 34, 'historical': 17},
])
def test_query_with_quotes(test_case):
    run_test_case(test_case)
