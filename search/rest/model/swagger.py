from common import constants, taxonomy
from search.rest.model.queries import QF_CHOICES

swagger_doc_params = {
    constants.X_FEATURE_FREETEXT_BOOL_METHOD: "Boolean method to use for unclassified "
                                             "freetext words. Defaults to \"" + constants.DEFAULT_FREETEXT_BOOL_METHOD + "\"",
    constants.X_FEATURE_DISABLE_SMART_FREETEXT: "Disables machine learning enriched queries. "
                                               "Freetext becomes traditional freetext query according to the setting of "
                                               "\"%s\"" % constants.X_FEATURE_FREETEXT_BOOL_METHOD,
    constants.X_FEATURE_ENABLE_FALSE_NEGATIVE: "Enables extra search for the current known "
                                              "term in free text to avoid false negatives",
    constants.PUBLISHED_AFTER: "Fetch job ads published after specified date and time. "
                              "Accepts either datetime (format YYYY-mm-ddTHH:MM:SS) or number of minutes "
                              "(e.g 60 means published in the last hour)",
    constants.PUBLISHED_BEFORE: "Fetch job ads published before specified date and time (format YYYY-mm-ddTHH:MM:SS)",
    constants.FREETEXT_QUERY: "Freetext query. Search in ad headline, ad description and employer name",
    constants.FREETEXT_FIELDS: "Fields to freetext search in, in addition to default freetext search "
                              "(parameter " + constants.FREETEXT_QUERY + ")\n"
                                                                        "Valid input values: " + str(QF_CHOICES) + "\n"
                                                                                                                   "Default (no input): Search in ad headline, ad description and employer name",
    constants.EMPLOYER: "Name or organisation number (numbers only, no dashes or spaces) of employer",
    taxonomy.OCCUPATION: "One or more occupational codes according to the taxonomy",
    taxonomy.GROUP: "One or more occupational group codes according to the taxonomy",
    taxonomy.FIELD: "One or more occupational area codes according to the taxonomy",
    taxonomy.COLLECTION: "One or more occupational collections according to the taxonomy. "
                         "Excludes not matching occupations, groups, fields",
    taxonomy.SKILL: "One or more competency codes according to the taxonomy",
    taxonomy.LANGUAGE: "One or more language codes according to the taxonomy",
    taxonomy.DRIVING_LICENCE_REQUIRED: "Set to true if driving licence required, false if not",
    taxonomy.DRIVING_LICENCE: "One or more types of demanded driving licenses, code according to the taxonomy",
    taxonomy.EMPLOYMENT_TYPE: "Employment type, code according to the taxonomy",
    constants.EXPERIENCE_REQUIRED: "Input 'false' to filter jobs that don't require experience",
    taxonomy.WORKTIME_EXTENT: "One or more codes for worktime extent, code according to the taxonomy",
    constants.PARTTIME_MIN: "For part-time jobs, minimum extent in percent (for example 50 for 50%)",
    constants.PARTTIME_MAX: "For part-time jobs, maximum extent in percent (for example 100 for 100%)",
    taxonomy.MUNICIPALITY: "One or more municipality codes, code according to the taxonomy",
    taxonomy.REGION: "One or more region codes, code according to the taxonomy",
    taxonomy.COUNTRY: "One or more country codes, code according to the taxonomy",
    constants.UNSPECIFIED_SWEDEN_WORKPLACE: "True will return all ads with unspecified workplace in Sweden. False does nothing",
    constants.ABROAD: "True will return ads for work outside of Sweden even when searching for places "
                     "matching Swedish municipality/region/country. False does nothing",
    constants.REMOTE: "True will return ads which are likely to allow remote work based on phrase matching,\n"
                     "False will only return ads that do not match these phrases\n ",
    constants.TRAINEE: "True will return ads which are likely to allow trainee work based on phrase matching,\n"
                     "False will only return ads that do not match these phrases\n",
    constants.LARLING: "True will return ads which are likely to allow larling work based on phrase matching,\n"
                     "False will only return ads that do not match these phrases\n",
    constants.FRANCHISE: "True will return ads which are likely to allow franchise work based on phrase matching,\n"
                     "False will only return ads that do not match these phrases\n",
    constants.HIRE_WORK_PLACE: "True will return ads which are likely concern rental of a place / chair to work from based on phrase matching,\n"
                     "False will only return ads that do not match these phrases\n",
    constants.POSITION: "Latitude and longitude in the format \"59.329,18.068\" (latitude,longitude)",
    constants.POSITION_RADIUS: "Radius from the specified " + constants.POSITION +
                              " (latitude,longitude) in kilometers (km)",
    constants.OPEN_FOR_ALL: "True will return all ads matching the phrase 'Öppen för alla'",
    constants.DURATION: "One or more employment-duration codes, code according to the taxonomy"
}
swagger_filter_doc_params = {
    constants.MIN_RELEVANCE: "Set a result relevance threshold between 0 and 1",
    constants.DETAILS: "Show 'full' (default) or 'brief' results details",
    constants.OFFSET: "The offset parameter defines the offset from the first result you "
                     "want to fetch. Valid range is (0-%d)" % constants.MAX_OFFSET,
    constants.LIMIT: "Number of results to fetch. Valid range is (0-%d)" % constants.MAX_LIMIT,
    constants.SORT: "Sorting.\n"
                   "relevance: relevance (points) (default sorting)\n"
                   "pubdate-desc: published date, descending (newest job ad first)\n"
                   "pubdate-asc: published date, ascending (oldest job ad first)\n"
                   "applydate-desc: last apply date, descending (newest apply date first)\n"
                   "applydate-asc: last apply date, descending (oldest apply date first, "
                   "few days left for application)\n"
                   "updated: sort by update date (descending)\n",
    constants.STATISTICS: "Show statistics for specified fields "
                         "(available fields: %s, %s, %s, %s, %s and %s)" % (
                             taxonomy.OCCUPATION,
                             taxonomy.GROUP,
                             taxonomy.FIELD,
                             taxonomy.COUNTRY,
                             taxonomy.MUNICIPALITY,
                             taxonomy.REGION),
    constants.STAT_LMT: "Maximum number of statistical rows per field",
    constants.LABEL: "One or more label strings, to filter ads by label",
}
