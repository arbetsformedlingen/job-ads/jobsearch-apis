import pytest
from tests.test_resources import elastic_data
from tests.test_resources.helper import get_by_id


# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch]

def test_original():
    for test_data in elastic_data.occupations_from_elastic:
        # find occupation with legacy id and check that such value exists
        assert (original_occupation := next(
            (item for item in test_data['occupations'] if item["legacy_ams_taxonomy_id"] is not None),
            None)) is not None

        api_response = get_by_id( test_data['_id'])
        assert original_occupation['concept_id'] == api_response['occupation']['concept_id']
