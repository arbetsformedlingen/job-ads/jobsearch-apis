import sys
import pytest
import requests

from tests.test_resources.helper import get_search, get_search_expect_error, get_historical_stats
from tests.test_resources.ad_fields_data_type import check_historical_stats
from tests.test_resources.historical import compare_keys, check_default_values, all_stats, min_values, \
    min_values_employment_type
from tests.test_resources.test_settings import NUMBER_OF_HISTORICAL_ADS
from common.constants import TAXONOMY_TYPE_LIST

pytestmark = pytest.mark.historical


@pytest.mark.smoke
def test_all_stats_key_names(historical_stats):
    compare_keys(historical_stats)


def test_all_stats_format(historical_stats):
    """
    check types in the return value
    """
    for key, stats in historical_stats.items():
        check_historical_stats(stats)


def test_all_stats_sorting_order_in_stat_type(historical_stats):
    """
    Checks that items for each stats type are sorted descending
    """
    check_sort_order(historical_stats)


def check_sort_order(stats):
    for key, stats in stats.items():
        highest_value = sys.maxsize
        for item in stats:
            assert item['occurrences'] <= highest_value
            highest_value = item['occurrences']


@pytest.mark.parametrize('limit', [1, 2, 4, 6, 9, 10, 11, 99, 1025, '1'])
def test_all_stats_limit( limit):
    stats = get_historical_stats({'limit': limit})
    compare_keys(stats)


def test_all_stats_default_limit(historical_stats):
    """
    Check that there are correct number of items, different depending on which year the ad is from
    """
    check_default_values(historical_stats)


@pytest.mark.skipif(reason="bug, see GitLab issue 31")
def test_all_stats_zero_limit():
    """
    Check that all fields are present and their lists are empty when using limit: 0
    """
    stats = get_historical_stats({'limit': 0})
    compare_keys(stats)


@pytest.mark.skipif(reason="bug, see GitLab issue 31")
def test_all_stats_none_limit():
    """
    Check that all fields are present and their lists have default values when using limit: None
    """
    stats = get_historical_stats({'limit': None})
    compare_keys(stats)
    check_default_values(stats)


@pytest.mark.smoke
@pytest.mark.parametrize("taxonomy_type", TAXONOMY_TYPE_LIST)
def test_taxonomy_type_single( taxonomy_type):
    params = {'taxonomy-type': taxonomy_type}
    result = get_historical_stats(params)
    assert len(result) == 1
    if taxonomy_type == 'employment-type':
        assert len(result[taxonomy_type]) >= min_values_employment_type
    else:
        assert len(result[taxonomy_type]) >= min_values
    check_sort_order(result)


@pytest.mark.skip('issue #46 ')
def test_taxonomy_type_all():
    taxonomy_types = ','.join(TAXONOMY_TYPE_LIST)  # comma-separated string
    params = {'taxonomy-type': taxonomy_types}
    result = get_historical_stats(params)
    assert len(result) == min_values
    for key, value in result.items():
        if key == 'employment-type':
            assert len(value) >= min_values_employment_type
        else:
            assert len(value) == min_values
        check_sort_order(result)


#    SEARCH

@pytest.mark.smoke
@pytest.mark.parametrize("stats_type", ['occupation-name', 'occupation-group', 'municipality', 'region', 'country'])
def test_stats( stats_type):
    result = get_search({'stats': stats_type})
    assert result['total']['value'] == NUMBER_OF_HISTORICAL_ADS
    assert len(result) == 7
    assert len(result['stats'][0]['values']) == 5


@pytest.mark.parametrize('stats_type', ['employment-type', 'language', 'skill'])
def test_bad_request( stats_type):
    get_search_expect_error( {'stats': stats_type}, expected_http_code=requests.codes.bad_request)


def test_multiple_stats():
    """
    Do call to stats with increasing number of parameters
    """
    for ix, stats_type in enumerate(all_stats):
        current_len = ix + 1
        stats_list = all_stats[0:current_len]
        print(stats_list)

        params = {'stats': stats_list}
        result = get_search(params)
        result_stats = result['stats']
        assert len(result_stats) == current_len
        for item in result_stats:
            assert len(item['values']) == 5
