import json
import copy
import pytest
import requests
from tests.test_resources.test_settings import NUMBER_OF_ADS, NUMBER_OF_HISTORICAL_ADS, test_headers, TEST_URL
from tests.test_resources.api_test_runner import HISTORICAL

X_FIELDS = 'X-Fields'

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


def _get_with_additional_headers(additional_headers, params={}):
    headers = copy.deepcopy(test_headers)
    headers.update(additional_headers)
    response = requests.get(TEST_URL + "/search", params=params, headers=headers)
    response.raise_for_status()
    return json.loads(response.content.decode('utf8'))


def test_empty_x_field():
    """
    Expect the same response is if x-fields header is not used
    """
    r = _get_with_additional_headers({X_FIELDS: ''})
    assert r['total']['value'] == NUMBER_OF_HISTORICAL_ADS if HISTORICAL else NUMBER_OF_ADS
    assert len(r) == 7
    assert len(r['hits']) == 10
    expected_number = 37
    ad_length = len(r['hits'][0])
    assert ad_length== expected_number, f"expected {expected_number} fields but got {ad_length} "


def test_x_fields_ad_teaser():
    x_fields_for_ad_teaser = "hits{id, publication_date, publication_date, headline, occupation, employer, workplace_address, positions}"
    r = _get_with_additional_headers({X_FIELDS: x_fields_for_ad_teaser})
    assert len(r) == 1
    assert len(r['hits']) == 10
    assert len(r['hits'][0]) == 6


def test_x_fields_workplace_address():
    x_fields_for_address = "hits{workplace_address{municipality, municipality_code,region,region_code, country, country_code , street_address, postcode, city, coordinates}}"

    r = _get_with_additional_headers({X_FIELDS: x_fields_for_address})
    assert len(r) == 1
    assert len(r['hits']) == 10
    assert len(r['hits'][0]) == 1
    assert len(r['hits'][0]['workplace_address']) == 10


# noinspection PyUnusedLocal
def test_x_fields_with_query():
    params = {'q': 'sjuksköterska'}
    r = _get_with_additional_headers({X_FIELDS: "hits{headline, occupation, employer, workplace_address}"}, params)
    assert len(r['hits'][0]) == 4
    assert len(r['hits']) == 10
    for field in ['headline', 'occupation', 'employer', 'workplace_address']:
        no_key_error = r['hits'][0][field] # noqa F841 unused variable
