import logging
import datetime
from feedgen.feed import FeedGenerator
from common import constants, settings

log = logging.getLogger(__name__)


class AtomFeed():

    def generate(self, search_results: list) -> str:
        """
        Create an atom feed as an atom string response.
        """
        feed = self._create()
        self._create_and_add_entries(feed, search_results)

        return feed.atom_str(pretty=True)

    def _create(self) -> FeedGenerator:
        """
        Create and setup a new feed object.
        """
        feed = FeedGenerator()
        feed.generator(generator="JobTech", version=constants.API_VERSION, uri="https://jobstream.api.jobtechdev.se")
        feed.id('https://jobstream.api.jobtechdev.se/feed')
        feed.link(href='https://jobstream.api.jobtechdev.se', rel='self')
        feed.title('JobStream Feed')
        feed.subtitle('Realtidsdata från alla jobbannonser som är publicerade hos Arbetsförmedlingen')
        return feed

    def _create_and_add_entries(self, feed: FeedGenerator, search_results: list):
        """
        Create and add feed entries from search result hits.
        """
        counter = 0
        latest_ad_updated_at = datetime.datetime.fromtimestamp(0)
        for item in search_results:
            source = item['_source']
            if not self._is_removed(source):
                atom_ad = feed.add_entry()
                ad_uri = self._get_ad_id_uri(source)
                # These three entry rows are obligatory for Atom
                atom_ad.id(ad_uri)
                atom_ad.title(source.get("headline", ""))

                # Check if we need to update feed.update entry
                updated_datetime, updated_str = self._get_last_updated(source)
                if updated_datetime > latest_ad_updated_at:
                    latest_ad_updated_at = updated_datetime
                atom_ad.updated(updated_str)

                # Recommended but not obligatory entry rows
                publication_date = source.get("publication_date", None)
                if publication_date:
                    publication_date += ' UTC'
                    atom_ad.published(publication_date)

                # atom_ad.content(type='html', content=f"{settings.JOBSEARCH_HOST}/ad/{str(source['id'])}")
                atom_ad.content(type="html", content=self._create_content(source["description"]["text"]))
            counter += 1

        # Set feed updated entry
        feed.updated(str(latest_ad_updated_at) + " UTC")

        log.debug(f"(load_ads_atom_generator) Delivered: {counter} ads as stream")

    def _is_removed(self, ad: dict) -> bool:
        return ad.get("removed", False)

    def _get_ad_id_uri(self, source: dict):
        if ad_id := str(source.get("id", source.get("external_id", None))):
            return f"{settings.JOBSEARCH_HOST}/ad/{ad_id}"
        else:
            return None

    def _get_last_updated(self, source) -> tuple:
        """
        Sometimes 'updated' key is not set.
        Then we need to use the timestamp instead.
        """
        try:
            updated = source['updated']
        except KeyError:
            # Divide by 1000 to get seconds
            # since this value is milliseconds
            updated = source['timestamp'] / 1000
        updated_datetime = datetime.datetime.fromtimestamp(updated)
        return updated_datetime, str(datetime.datetime.fromtimestamp(updated)) + ' UTC'

    def _create_content(self, description: str) -> str:
        """
        Get a short summary from the two first sentences
        of the ad description.
        """
        all_sentences = []
        try:
            all_sentences = description.split("\n")
            if all_sentences:
                brief_description = " ".join(all_sentences[:2]).strip() + "..."
            else:
                brief_description = ""
        except KeyError:
            brief_description = ""

        return brief_description
