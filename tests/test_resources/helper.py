import json
import logging
import random
import time
import pickle
import requests
import xmltodict

from common import settings
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA, TEST_URL

log = logging.getLogger(__name__)


def compare_typeahead(actual_typeahead, expected_typeahead):
    error_msg = ''
    try:
        for index, suggestion in enumerate(actual_typeahead):
            error_msg = f"Comparison of {suggestion['value']} and {expected_typeahead[index]} failed"
            assert suggestion['value'] == expected_typeahead[index]
    except AssertionError as ex:
        actual = []
        for item in actual_typeahead:
            actual.append(item['value'])
        _handle_failed_comparison(ex, error_msg)


def compare_suggestions(actual, expected, query):
    msg = ''
    if len(actual) >= 50:
        # if 50 or more we can't be sure of order and content of suggestions
        return
    try:
        msg = f"\nQuery: {query}"
        compare(len(actual), len(expected), msg)
        for s in expected:
            msg = f"Did not find {s} in {actual} "
            assert s in actual
    except AssertionError as ex:
        _handle_failed_comparison(ex, msg)


def get_typeahead_values(response):
    return [item['value'] for item in response['typeahead']]


def compare_two_lists(actual_result_list, expected_result_list):
    """
    All items in list 1 must be in list 2
    """
    assert sorted(actual_result_list) == sorted(
        expected_result_list), f"Lists do not match.\nActual list: {actual_result_list}\nExpected list: {expected_result_list}"


def check_complete_results(query, actual, expected):
    try:
        compare_two_lists(actual_result_list=actual, expected_result_list=expected)
    except AssertionError:
        print(f"Query: {query}")
        print(f"Actual result: {actual}")
        print(f"Expected result: {expected}")
        raise


def compare_synonyms(synonyms, complete_values, expect_success):
    msg = ''
    try:
        for s in synonyms:
            if expect_success:
                msg = f"Synonym '{s}' not found in response"
                assert s in complete_values
            else:
                msg = f"Synonym '{s}' was found in response"
                assert s not in complete_values
    except AssertionError as ex:
        _handle_failed_comparison(ex, msg)


def compare(actual, expected, msg=""):
    error_msg = f"expected {expected} items but got {actual}. " + msg
    try:
        assert actual == expected
    except AssertionError as ex:
        print(error_msg)
        _handle_failed_comparison(ex, error_msg)


def _handle_failed_comparison(ex, msg="Error in comparison"):
    if TEST_USE_STATIC_DATA:
        log.error(msg)
        raise ex  # static test data == strict comparision
    else:  # staging and prod will have live data which will return a different number of results
        log.warning(f"Live data, ignore error. {msg}")


def check_len_more_than(check_this, compare_to):
    check_value_more_than(len(check_this), compare_to)


def check_value_more_than(check_this, compare_to):
    error_msg = f"{check_this} was not larger than {compare_to}."
    try:
        assert check_this > compare_to
    except AssertionError as ex:
        _handle_failed_comparison(ex, error_msg)


def get_stream_check_number_of_results(expected_number, params, headers={}):
    response = requests.get(f"{TEST_URL}/stream", params=params, headers=headers)
    return _check_ok_response_and_number_of_ads(response, expected_number)


def get_with_path_return_json(path, params):
    response = requests.get(f"{TEST_URL}/{path}", params=params)
    return _check_ok_return_content(response)


def get_feed_raw(params):
    return requests.get(f"{TEST_URL}/feed", params=params)


def get_feed(params):
    response = get_feed_raw(params)
    return response.content.decode('utf8')


def get_stream(params):
    response = requests.get(f"{TEST_URL}/stream", params=params)
    return _check_ok_return_content(response)


def get_search(params):
    response = requests.get(f"{TEST_URL}/search", params=params)
    return _check_ok_return_content(response)


def get_historical_stats(params):
    # historical
    response = requests.get(f"{TEST_URL}/stats", params=params)
    return _check_ok_return_content(response)['stats']


def get_search_expect_error(params, expected_http_code):
    response = requests.get(f"{TEST_URL}/search", params=params)
    assert response.status_code == expected_http_code, f"Expected http return code to be {expected_http_code} , but got {response.status_code}"
    return response


def get_search_check_number_of_results(expected_number, params):
    response = requests.get(f"{TEST_URL}/search", params=params)
    return _check_ok_response_and_number_of_ads(response, expected_number)


def get_total(params):
    return get_search(params)['total']['value']


def get_complete(params):
    response = requests.get(f"{TEST_URL}/complete", params=params)
    return _check_ok_return_content(response)


def get_complete_with_headers(params, headers):
    response = requests.get(f"{TEST_URL}/complete", params=params, headers=headers)
    return _check_ok_return_content(response)


def get_complete_expect_error(params, expected_http_code):
    response = requests.get(f"{TEST_URL}/complete", params=params)
    assert response.status_code == expected_http_code, f"Expected http return code to be {expected_http_code} , but got {response.status_code}"
    return response


def get_stream_expect_error(params, expected_http_code):
    r = requests.get(f"{TEST_URL}/stream", params=params)
    status = r.status_code
    assert status == expected_http_code, f"Expected http return code to be {expected_http_code} , but got {status}"


def get_snapshot():
    return _check_ok_return_content(get_snapshot_raw())


def get_snapshot_raw():
    return requests.get(f"{TEST_URL}/snapshot")


def _check_ok_return_content(response):
    response.raise_for_status()
    return json.loads(response.content.decode('utf8'))


def _check_ok_response_and_number_of_ads(response, expected_number):
    response.raise_for_status()
    assert response.content is not None
    list_of_ads = json.loads(response.content.decode('utf8'))
    if '/search' in response.url:
        list_of_ads = list_of_ads['hits']
    if expected_number is not None:
        compare(len(list_of_ads), expected_number)
    _check_list_of_ads(list_of_ads)
    return list_of_ads


def validate_feed(xml_str: str, expected_number: int):
    xml_feed = _parse_feed(xml_str)
    entries = xml_feed['entry']
    assert len(entries) == expected_number, f"Got {len(entries)} entries, expected {expected_number}"
    for entry in entries:
        validate_single_entry(entry)


def validate_single_entry(entry: dict):
    assert len(entry) == 5, f"Entry length was {len(entry)}"
    id_len = len(f"{settings.JOBSEARCH_HOST}/ad/00000000")
    assert len(entry['id']) == id_len, f"expected id length to be {id_len} but it was {len(entry['id'])}"
    assert entry['title']
    assert entry['updated']
    assert entry['published']
    assert entry['content']
    assert entry['content']['@type'] == 'html'
    assert entry['content']['#text']


def validate_feed_single_entry(xml_str: str):
    xml_feed = _parse_feed(xml_str)
    validate_single_entry(xml_feed['entry'])


def validate_feed_header(xml_str: str):
    xml_feed = _parse_feed(xml_str)
    for key in ['@xmlns', 'id', 'title', 'updated', 'link', 'generator', 'subtitle']:
        assert xml_feed.get(key, False)


def _parse_feed(xml_str: str) -> dict:
    return xmltodict.parse(xml_str)['feed']


def _check_list_of_ads(list_of_ads):
    for ad in list_of_ads:
        assert isinstance(ad['id'], str)
        checks = []
        checks.append(ad['id'])
        if not ad['removed']:
            checks.append(ad['headline'])
            checks.append(ad['description'])
            checks.append(ad['occupation'])
            checks.append(ad['workplace_address']['country'])
            for c in checks:
                assert c is not None, ad


def check_freetext_concepts(free_text_concepts, list_of_expected):
    assert free_text_concepts['skill'] == list_of_expected[0]
    assert free_text_concepts['occupation'] == list_of_expected[1]
    assert free_text_concepts['location'] == list_of_expected[2]
    assert free_text_concepts['skill_must'] == list_of_expected[3]
    assert free_text_concepts['occupation_must'] == list_of_expected[4]
    assert free_text_concepts['location_must'] == list_of_expected[5]
    assert free_text_concepts['skill_must_not'] == list_of_expected[6]
    assert free_text_concepts['occupation_must_not'] == list_of_expected[7]
    assert free_text_concepts['location_must_not'] == list_of_expected[8]


def _fetch_and_validate_result(query, resultfield, expected, non_negative=True):
    json_response = get_search(query)
    hits = json_response['hits']
    check_len_more_than(hits, 0)
    for hit in hits:
        for i in range(len(resultfield)):
            if non_negative:
                assert _get_nested_value(resultfield[i], hit) == expected[i]
            else:
                assert not _get_nested_value(resultfield[i], hit) == expected[i]


def _get_nested_value(path, dictionary):
    keypath = path.split('.')
    value = None
    for i in range(len(keypath)):
        element = dictionary.get(keypath[i])
        if isinstance(element, dict):
            dictionary = element
        else:
            value = element
            break
    return value


def get_search_with_headers(params, headers):
    response = requests.get(TEST_URL + '/search', params, headers=headers)
    return _check_ok_return_content(response)


def check_ads_for_country_in_address(hits, abroad):
    for hit in hits:
        country = hit['workplace_address']['country']
        country_concept_id = hit['workplace_address']['country_concept_id']
        assert (country == 'Sverige') != abroad
        assert (country_concept_id == 'i46j_HmG_v64') != abroad


def is_dst():
    """
    Daylight Savings Time
    1 = DST right now
    0 = not DST right now
    """
    return time.localtime().tm_isdst


def compare_multiple(list_of_items_to_compare):
    errors = []
    for tpl in list_of_items_to_compare:
        actual = tpl[0]
        expected = tpl[1]
        message = tpl[2]
        if not actual == expected:
            errors.append(f"Expected {expected} but got {actual}. {message}")
    assert not errors, f"ERRORS: {errors}"


def select_random_from_list(full_list, how_many):
    if how_many > len(full_list):
        how_many = len(full_list)
    return random.sample(full_list, how_many)


def pickle_to_file(file_name, pickle_this):
    with open(file_name, 'wb') as f:
        pickle.dump(pickle_this, f, protocol=pickle.HIGHEST_PROTOCOL)


def unpickle_file(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f)


def get_by_id(ad_id, check_for_error=True):
    r = requests.get(f"{TEST_URL}/ad/{ad_id}")
    if check_for_error:
        r.raise_for_status()
    return json.loads(r.content.decode('utf8'))


def get_by_id_raw(ad_id):
    return requests.get(f"{TEST_URL}/ad/{ad_id}")


def get_logo(ad_id, verify_response_ok=True):
    url = f"{TEST_URL}/ad/{ad_id}/logo"
    response = requests.get(url)
    if verify_response_ok:
        response.raise_for_status()
    return response


def temp_dump_testcase(file_name, query, jobsearch, actual):
    test_case = {"query": query,
                 "jobsearch": jobsearch,
                 "historical": actual}
    with open(file_name, "a", encoding="utf8") as f:
        json.dump(test_case, f, ensure_ascii=False)
