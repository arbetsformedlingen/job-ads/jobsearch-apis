class MultipleHistoricalResultsError(Exception):
    """
    Thrown when a search in historical endpoint /ad/<id>
    results in multiple hits.
    """

    pass
