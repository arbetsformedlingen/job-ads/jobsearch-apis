def format_hits_with_only_original_values(hits):
    for job_ad in hits:  # TODO write unit tests for None and not-None values
        job_ad = format_one_ad(job_ad)
    return hits


def format_one_ad(job_ad):
    if occupations := job_ad.get('occupation', []):
        job_ad['occupation'] = get_original_value_from_list(occupations)

    occ_group = job_ad.get('occupation_group', None)
    if occ_group and isinstance(occ_group, list):
        job_ad['occupation_group'] = occ_group[0]

    occ_field = job_ad.get('occupation_field', None)
    if occ_field and isinstance(occ_field, list):
        job_ad['occupation_field'] = occ_field[0]

    if skills_must := job_ad.get('must_have', {}).get('skills', None):
        job_ad['must_have']['skill'] = _keep_multiple_original_values_in_list(skills_must)
    if skills_nice := job_ad.get('nice_to_have', {}).get('skills', None):
        job_ad['nice_to_have']['skill'] = _keep_multiple_original_values_in_list(skills_nice)
    if employment_type := job_ad.get('employment_type', {}):
        job_ad['employment_type'] = get_original_value_from_list(employment_type)
    if application_contacts := job_ad.get('application_contacts', []):
        job_ad['application_contacts'] = _remove_contacts_if_null(application_contacts)

    return job_ad


def _remove_contacts_if_null(value_list):
    try:
        for dicts in value_list:
            for value in dicts.values():
                if not value is None:
                    return value_list
    except AttributeError:
        return []

    return []


def _keep_multiple_original_values_in_list(values):
    """
    Used where there can be one or more 'original values', e.g. skills
    """
    result = []
    for value in values:
        if value.get('original_value', False):
            result.append({
                "concept_id": value.get('concept_id'),
                "label": value.get('label'),
                "legacy_ams_taxonomy_id": value.get('legacy_ams_taxonomy_id')
            })
    return result


def get_original_value_from_list(full_list):
    if not full_list:
        return {
            "concept_id": None,
            "label": None,
            "legacy_ams_taxonomy_id": None}

    # workaround until only original value is added by importer
    if isinstance(full_list, dict):
        return {
            "concept_id": full_list.get('concept_id'),
            "label": full_list.get('label'),
            "legacy_ams_taxonomy_id": full_list.get('legacy_ams_taxonomy_id')}

    for value in full_list:
        if value.get('original_value', False):
            return {
                "concept_id": value.get('concept_id'),
                "label": value.get('label'),
                "legacy_ams_taxonomy_id": value.get('legacy_ams_taxonomy_id')}
    return None
