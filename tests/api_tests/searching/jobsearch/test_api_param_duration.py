# -*- coding: utf-8 -*-
import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_ADS

# marks all tests as jobsearch
pytestmark = [pytest.mark.jobsearch]


@pytest.mark.smoke
@pytest.mark.parametrize(
    "duration,expected_count,comment",
    [
        ["a7uU_j21_mkL", 3207, "Tills vidare"],
        ["9RGe_UxD_FZw", 0, "12 månader - upp till 2 år"],
        ["gJRb_akA_95y", 0, "6 månader - upp till 12 månader"],
        ["Xj7x_7yZ_jEn", 361, "3 månader - upp till 6 månader"],
        ["Sy9J_aRd_ALx", 502, "11 dagar - upp till 3 månader"],
        ["cAQ8_TpB_Tdv", 9, "Upp till 10 dagar"],
    ],
)
def test_employment_valid_duration(duration, expected_count, comment):
    number_of_hits = search_for_duration_and_get_number_of_hits(duration)

    assert (
        number_of_hits == expected_count
    ), f'Expected {str(expected_count)} hits for "{comment}" but got {str(number_of_hits)}'


@pytest.mark.parametrize(
    "duration,expected_count,comment",
    [
        [1, 3207, "Tills vidare"],
        [9, 0, "12 månader - upp till 2 år"],
        [0, 0, "6 månader - upp till 12 månader"],
        [3, 361, "3 månader - upp till 6 månader"],
        [7, 502, "11 dagar - upp till 3 månader"],
        [8, 9, "Upp till 10 dagar"],
    ],
)
def test_employment_valid_legacy_duration(duration, expected_count, comment):
    number_of_hits = search_for_duration_and_get_number_of_hits(duration)

    assert (
        number_of_hits == expected_count
    ), f'Expected {str(expected_count)} hits for "{comment}" but got {str(number_of_hits)}'


def test_employment_no_duration():
    search_for_duration_and_get_number_of_hits(
        {
            "duration": None,
            "jobsearch": NUMBER_OF_ADS,
            "comment": "Empty duration parameter",
        }
    )


@pytest.mark.parametrize(
    "duration,expected_count,comment",
    [
        ["not-a-concept-id", 0, "Invalid parameter string."],
        [True, 0, "Invalid parameter, boolean."],
        [False, 0, "Invalid parameter, boolean."],
    ],
)
def test_employment_on_invalid_duration(duration, expected_count, comment):
    number_of_hits = search_for_duration_and_get_number_of_hits(duration)

    assert (
        number_of_hits == expected_count
    ), f'Expected {str(expected_count)} hits for "{comment}" but got {str(number_of_hits)}'


def search_for_duration_and_get_number_of_hits(duration: dict) -> int:
    response = get_search({"duration": duration})
    number_of_hits = response["total"]["value"]
    return number_of_hits
