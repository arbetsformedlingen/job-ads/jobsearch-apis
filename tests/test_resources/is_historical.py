import json
import requests
from tests.test_resources.test_settings import TEST_URL

def get_swagger():
    response = requests.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    return json.loads(response.content.decode('utf8'))


def historical():
    swagger = get_swagger()
    result = swagger["info"]["title"] == 'Historical job ads'
    print(f"Historical: {result}")
    return result
