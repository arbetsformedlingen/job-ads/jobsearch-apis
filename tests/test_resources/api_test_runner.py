from tests.test_resources.helper import get_search
from tests.test_resources.is_historical import historical

HISTORICAL = historical()


def write_tc(tc, actual_number_of_hits):
    with open("temp.txt", "a", encoding="utf8") as f:
        if HISTORICAL:
            tc["historical"] = actual_number_of_hits
        else:
            tc["jobsearch"] = actual_number_of_hits
        f.write(f"{tc},\n")


def run_test_case(test_case: dict) -> dict:
    params = test_case["params"]
    response = get_search(params)
    number_of_hits = response["total"]["value"]
    if HISTORICAL:
        expected = test_case["historical"]
    else:
        expected = test_case["jobsearch"]
    assert number_of_hits == expected, f'ERROR: Actual number of hits: {number_of_hits}. Expected: {expected}. Test case {test_case}'
    return response
