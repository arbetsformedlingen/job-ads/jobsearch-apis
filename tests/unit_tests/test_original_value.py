import pytest
from common.ad_formatter import get_original_value_from_list, _keep_multiple_original_values_in_list


def test_original_value():
    ad = {'occupation':
        [
            {'concept_id': 'YHB5_wmX_UCt', 'label': 'Juridisk ombudsman', 'legacy_ams_taxonomy_id': '4424',
             'original_value': True},
            {'concept_id': '7YVF_kE4_aPm', 'label': 'Ombudsman', 'legacy_ams_taxonomy_id': '5775'}
        ]
    }
    result = get_original_value_from_list(ad['occupation'])
    assert result == {'concept_id': 'YHB5_wmX_UCt',
                      'label': 'Juridisk ombudsman',
                      'legacy_ams_taxonomy_id': '4424'}


def test_original_value_extra_attribute():
    ad = {'occupation':
        [
            {'something': True, 'concept_id': 'YHB5_wmX_UCt', 'label': 'Juridisk ombudsman',
             'legacy_ams_taxonomy_id': '4424',
             'original_value': True},
            {'concept_id': '7YVF_kE4_aPm', 'label': 'Ombudsman', 'legacy_ams_taxonomy_id': '5775'}
        ]
    }
    result = get_original_value_from_list(ad['occupation'])
    assert result == {'concept_id': 'YHB5_wmX_UCt',
                      'label': 'Juridisk ombudsman',
                      'legacy_ams_taxonomy_id': '4424'}


def test_original_value_in_list_1():
    values = [{'concept_id': 'K8eq_sEz_eXV', 'label': 'HTML, programmeringsspråk', 'weight': 5,
               'legacy_ams_taxonomy_id': '200029', 'original_value': True},
              {'concept_id': 'Giw8_cWm_RWU', 'label': 'WordPress, publicerings- och portalverktyg', 'weight': 5,
               'legacy_ams_taxonomy_id': '608562', 'original_value': True}]
    expected = [
        {'concept_id': 'K8eq_sEz_eXV', 'label': 'HTML, programmeringsspråk', 'legacy_ams_taxonomy_id': '200029'},
        {'concept_id': 'Giw8_cWm_RWU', 'label': 'WordPress, publicerings- och portalverktyg',
         'legacy_ams_taxonomy_id': '608562'}]
    result = _keep_multiple_original_values_in_list(values)
    assert result == expected


def test_original_value_in_list_2():
    values = [{'concept_id': 'K8eq_sEz_eXV', 'label': 'HTML, programmeringsspråk', 'weight': 5,
               'legacy_ams_taxonomy_id': '200029', 'original_value': True},
              {'concept_id': 'Giw8_cWm_RWU', 'label': 'WordPress, publicerings- och portalverktyg', 'weight': 5,
               'legacy_ams_taxonomy_id': '608562', 'original_value': False}]
    expected = [
        {'concept_id': 'K8eq_sEz_eXV', 'label': 'HTML, programmeringsspråk', 'legacy_ams_taxonomy_id': '200029'},
    ]
    result = _keep_multiple_original_values_in_list(values)
    assert result == expected


def test_original_value_in_list_3():
    """
    'concept_id' missing in the list item with 'original_value': True
    """
    values = [{'something': True, 'label': 'HTML, programmeringsspråk', 'weight': 5,
               'legacy_ams_taxonomy_id': '200029', 'original_value': True},
              {'concept_id': 'Giw8_cWm_RWU', 'label': 'WordPress, publicerings- och portalverktyg', 'weight': 5,
               'legacy_ams_taxonomy_id': '608562', 'original_value': False}]
    expected = [{'concept_id': None,
                 'label': 'HTML, programmeringsspråk',
                 'legacy_ams_taxonomy_id': '200029'}]
    result = _keep_multiple_original_values_in_list(values)
    assert result == expected


@pytest.mark.parametrize("value", [None, False, 0])
def test_type_error(value):
    with pytest.raises(TypeError):
        _keep_multiple_original_values_in_list(value)


@pytest.mark.parametrize("value", [None, False, 0, 1, 'some_string'])
def test_attribute_error(value):
    value_as_list = [value]
    with pytest.raises(AttributeError):
        _keep_multiple_original_values_in_list(value_as_list)


def test_no_valid_keys():
    hits = [
        {'my_key': True},
        {'my_key': False}
    ]
    assert _keep_multiple_original_values_in_list(hits) == []
