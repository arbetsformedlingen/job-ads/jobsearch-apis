import logging

from elasticsearch import RequestError

from common import settings
from common.main_elastic_client import elastic_client
from common.elastic_connection_with_retries import elastic_search_with_retry

log = logging.getLogger(__name__)


tax_value_cache = {}

taxonomy_type_dictionary = {
    'legacy_ams_taxonomy_id': 'legacy_ams_taxonomy_id',
    'concept_id': 'concept_id',
    'label': 'label.keyword'
}


def get_taxonomy_info(stats_by, type, value, occurrences, not_found_response=None):
    key = f"concept-{type}-{value}-{not_found_response}"
    if key not in tax_value_cache:
        try:
            query = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "term": {
                                    taxonomy_type_dictionary[stats_by]: {
                                        "value": value,
                                    }
                                }
                            },
                            {
                                "term": {
                                    "type": {
                                        "value": type,
                                    }
                                }
                            }
                        ]
                    }
                }
            }
            response = elastic_search_with_retry(client=elastic_client(), query=query, index=settings.ES_TAX_INDEX_ALIAS)
            hit = response.get('hits', {}).get('hits', [])
            item = hit[0].get('_source', {}) if hit else {}
        except RequestError as e:
            log.warning(f'Taxonomy RequestError for request with arguments type: {type} and id: {value}. {e}')
            item = not_found_response
            log.info(f"(get_legacy_by_concept_id) set value: {not_found_response}")
        if item:
            tax_value_cache[key] = item
        else:
            tax_value_cache[key] = {}
            log.warning(f"(get_legacy_by_concept_id) set empty value to tax_value_cache[key]: [{key}]")
    result = dict(tax_value_cache.get(key, {}))
    if not result:
        result['type'] = type
        result[stats_by] = value
    result['occurrences'] = occurrences
    return result
