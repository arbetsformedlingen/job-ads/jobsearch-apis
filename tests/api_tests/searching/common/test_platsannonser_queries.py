import pytest
import requests
import json
from dateutil import parser

from common import constants, fields, taxonomy
from tests.test_resources.test_settings import NUMBER_OF_ADS, NUMBER_OF_HISTORICAL_ADS, TEST_USE_STATIC_DATA
from tests.test_resources.helper import get_search, get_search_expect_error, compare, _fetch_and_validate_result, \
    check_value_more_than
from tests.test_resources.api_test_runner import run_test_case, HISTORICAL
from tests.test_resources.concept_ids import occupation as work, occupation_field as field, \
    occupation_group as group, taxonomy_values as other
from tests.test_resources.test_settings import EXPECTED_GYMNASIE_LARARE

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.smoke
def test_freetext_query_one_param():
    test_case = {"params": {"q": "gymnasielärare"}, "jobsearch": EXPECTED_GYMNASIE_LARARE, "historical": 132}
    run_test_case(test_case)


def test_enrich():
    test_case = {"params": {"q": "stresstålig"}, "jobsearch": 164, "historical": 640}
    run_test_case(test_case)


# Todo: different queries
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("minimum_relevance, expect_to_get_results",
                         [(0, True), (1, True), (2, False), (3, False), (4, False), (5, False), (6, False), (7, False),
                          (8, False), (9, False)])
def test_min_relevance_new(minimum_relevance, expect_to_get_results):
    query = 'sjuksköterska grundutbildad'
    params = {'q': query, constants.MIN_RELEVANCE: minimum_relevance}
    json_response = get_search(params)
    hits_total = json_response['total']['value']
    if expect_to_get_results:
        assert int(hits_total) > 0, f"no hits for query '{query}' with 'relevance-threshold' {minimum_relevance}"
    else:
        assert int(hits_total) == 0, f"Expected no hits for query '{query}' but got {int(hits_total)}"


@pytest.mark.parametrize("test_case", [
    pytest.param({'params': {'q': 'python php'}, 'jobsearch': 105, 'historical': 102}, marks=pytest.mark.smoke),
    {'params': {'q': '+python php'}, 'jobsearch': 97, 'historical': 73},
    {'params': {'q': '+python -php'}, 'jobsearch': 95, 'historical': 71},
    {'params': {'q': '-python -php'}, 'jobsearch': 4924, 'historical': 17648},
    {'params': {'q': 'php'}, 'jobsearch': 10, 'historical': 31},
    {'params': {'q': 'systemutvecklare +python java linux mac'}, 'jobsearch': 32, 'historical': 21},
    {'params': {'q': 'systemutvecklare +python -java linux mac'}, 'jobsearch': 22, 'historical': 18},
    {'params': {'q': 'systemutvecklare python java php'}, 'jobsearch': 68, 'historical': 76},
    {'params': {'q': 'systemutvecklare -python java php'}, 'jobsearch': 36, 'historical': 55},
    {'params': {'q': 'systemutvecklare python java -php'}, 'jobsearch': 60, 'historical': 63},
    {'params': {'q': 'lärarexamen'}, 'jobsearch': 30, 'historical': 134},
    {'params': {'q': 'lärarexamen -lärare'}, 'jobsearch': 8, 'historical': 53},
    {'params': {'q': 'sjuksköterska'}, 'jobsearch': 494, 'historical': 869},
    {'params': {'q': 'sjuksköterska -stockholm'}, 'jobsearch': 431, 'historical': 735},
    {'params': {'q': 'sjuksköterska -malmö'}, 'jobsearch': 473, 'historical': 827},
    {'params': {'q': 'sjuksköterska -stockholm -malmö'}, 'jobsearch': 410, 'historical': 695},
    pytest.param(
        {'params': {'q': 'sjuksköterska -stockholm -malmö -göteborg -eskilstuna'}, 'jobsearch': 381, 'historical': 649},
        marks=pytest.mark.smoke),
    {'params': {'q': 'sjuksköterska Helsingborg -stockholm -malmö -göteborg -eskilstuna'}, 'jobsearch': 8,
     'historical': 24},
    {'params': {'q': '+it-arkitekt'}, 'jobsearch': 12, 'historical': 10},
    {'params': {'q': '-it-arkitekt'}, 'jobsearch': 5017, 'historical': 17740},
    {'params': {'q': 'arkitekt -it-arkitekt'}, 'jobsearch': 6, 'historical': 14},
    {'params': {'q': 'arkitekt +it-arkitekt'}, 'jobsearch': 12, 'historical': 10},
    {'params': {'q': '+it-arkitekt arkitekt'}, 'jobsearch': 12, 'historical': 10},
    {'params': {'q': 'arkitekt -"it-arkitekt"'}, 'jobsearch': 7, 'historical': 13},
    {'params': {'q': '"it-arkitekt" arkitekt'}, 'jobsearch': 9, 'historical': 15},
    {'params': {'q': '"it-arkitekt" arkitekt'}, 'jobsearch': 9, 'historical': 15},
    pytest.param({'params': {'q': 'c-körkort'}, 'jobsearch': 30, 'historical': 63}, marks=pytest.mark.smoke),
    {'params': {'q': '-c-körkort'}, 'jobsearch': 4999, 'historical': 17687},
    {'params': {'q': 'cad-verktyg'}, 'jobsearch': 1, 'historical': 5},
    {'params': {'q': '-cad-verktyg'}, 'jobsearch': 5028, 'historical': 17745},
    {'params': {'q': 'erp-system'}, 'jobsearch': 7, 'historical': 11},
    {'params': {'q': '-erp-system'}, 'jobsearch': 5022, 'historical': 17739},
    {'params': {'q': 'it-tekniker'}, 'jobsearch': 24, 'historical': 53},
    {'params': {'q': '-it-tekniker'}, 'jobsearch': 5005, 'historical': 17697},
    {'params': {'q': 'backend-utvecklare'}, 'jobsearch': 19, 'historical': 13},
    pytest.param({'params': {'q': '-backend-utvecklare'}, 'jobsearch': 5010, 'historical': 17737},marks=pytest.mark.smoke),

])
def test_freetext_plus_minus( test_case):
    """
    Tests query with plus and minus modifiers
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {"q": "sjukssköterska"}, "jobsearch": 494, "historical": 863},
    {"params": {"q": "javasscript"}, "jobsearch": 61, "historical": 93},
    {"params": {"q": "montesori"}, "jobsearch": 2, "historical": 17}
])
def test_freetext_query_misspelled_param(test_case):
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {"q": "c++"}, "jobsearch": 44, "historical": 81},
    {"params": {"q": "c#"}, "jobsearch": 52, "historical": 94}])
def test_freetext_query_with_special_characters(test_case):
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'kista'}, 'jobsearch': 12, 'historical': 114},
    {'params': {'q': 'skåne'}, 'jobsearch': 656, 'historical': 951},
    {'params': {'q': 'värmland'}, 'jobsearch': 106, 'historical': 156},
    {'params': {'q': 'örebro'}, 'jobsearch': 85, 'historical': 272},
    {'params': {'q': 'örebro län'}, 'jobsearch': 129, 'historical': 117},
])
def test_freetext_query_geo_param(test_case):
    run_test_case(test_case)


def test_bugfix_reset_query_rewrite_location():
    json_response = get_search(params={'q': 'rissne', 'limit': '0'})
    check_value_more_than(json_response['total']['value'], 0)


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'kista kallhäll'}, 'jobsearch': 13, 'historical': 121},
    {'params': {'q': 'kallhäll introduktion'}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': 'kallhäll ystad'}, 'jobsearch': 17, 'historical': 54},
    {'params': {'q': 'stockholm malmö'}, 'jobsearch': 1071, 'historical': 4001},
    {'params': {'q': 'skåne'}, 'jobsearch': 656, 'historical': 951},
    {'params': {'q': '+trelleborg -stockholm ystad'}, 'jobsearch': 19, 'historical': 48},
    {'params': {'q': 'fridhemsplan'}, 'jobsearch': 1, 'historical': 9},

])
def test_freetext_query_location_extracted_or_enriched_or_freetext(test_case):
    run_test_case(test_case)


def test_too_big_offset():
    response = get_search_expect_error( params={'offset': '2001', 'limit': '0'},
                                       expected_http_code=requests.codes.bad_request)
    response_json = json.loads(response.content.decode('utf8'))
    assert response_json['errors']['offset'] == "Invalid argument: 2001. argument must be within the range 0 - 2000"
    assert 'Input payload validation failed' in str(response.text)


def test_total_hits():
    test_case = {"params": {'offset': '0', 'limit': '0'}, "jobsearch": NUMBER_OF_ADS,
                 "historical": NUMBER_OF_HISTORICAL_ADS}

    run_test_case(test_case)


@pytest.mark.skip("rewrite to get max offset since number of ads > max offset")
@pytest.mark.slow
def test_find_all_ads_check_removed_is_false():
    limit = 100
    for offset in range(0, NUMBER_OF_ADS, limit):
        json_response = get_search(params={'offset': offset, 'limit': limit})
        hits = json_response['hits']
        for hit in hits:
            assert hit['removed'] is False
        if NUMBER_OF_ADS - offset > limit:
            expected = limit
        else:
            expected = NUMBER_OF_ADS % limit
        compare(len(hits), expected)


def test_freetext_query_job_title_with_hyphen():
    json_response = get_search(params={'q': 'HR-specialister', 'limit': '1'})
    assert json_response['freetext_concepts']
    assert json_response['freetext_concepts']['occupation']
    occupation_val = json_response['freetext_concepts']['occupation'][0]
    assert occupation_val == 'hr-specialist'


def test_freetext_query_two_params():
    test_case = {"params": {'q': 'gymnasielärare lokförare'}, "jobsearch": EXPECTED_GYMNASIE_LARARE, "historical": 131}
    run_test_case(test_case)


def test_publication_range():
    date_from = "2020-12-01T00:00:00"
    date_until = "2020-12-20T00:00:00"
    params = {constants.PUBLISHED_AFTER: date_from, constants.PUBLISHED_BEFORE: date_until, "limit": 100}
    json_response = get_search(params)
    hits = json_response['hits']
    if HISTORICAL:
        assert len(hits) == 64
    else:
        assert len(hits) == 60
    for hit in hits:
        assert parser.parse(hit[fields.PUBLICATION_DATE]) >= parser.parse(date_from)
        assert parser.parse(hit[fields.PUBLICATION_DATE]) <= parser.parse(date_until)


def test_driving_license_required():
    _fetch_and_validate_result( {taxonomy.DRIVING_LICENCE_REQUIRED: 'true'}, [fields.DRIVING_LICENCE_REQUIRED],
                               [True])
    _fetch_and_validate_result( {taxonomy.DRIVING_LICENCE_REQUIRED: 'false'}, [fields.DRIVING_LICENCE_REQUIRED],
                               [False])


@pytest.mark.parametrize("query, path, expected, non_negative",
                         [({taxonomy.OCCUPATION: work.systemutvecklare_programmerare},
                           [fields.OCCUPATION + ".concept_id"], [work.systemutvecklare_programmerare], True),
                          ({taxonomy.OCCUPATION: f"-{work.systemutvecklare_programmerare}"},
                           [fields.OCCUPATION + ".concept_id"], [work.systemutvecklare_programmerare], False),
                          ({taxonomy.GROUP: group.mjukvaru__och_systemutvecklare_m_fl_},
                           [fields.OCCUPATION_GROUP + ".concept_id"],
                           [group.mjukvaru__och_systemutvecklare_m_fl_], True),
                          ({taxonomy.FIELD: field.data_it}, [fields.OCCUPATION_FIELD + ".concept_id"], [field.data_it],
                           True),
                          ({taxonomy.FIELD: f"-{field.data_it}"}, [fields.OCCUPATION_FIELD + ".concept_id"],
                           [field.data_it], False),
                          ({taxonomy.GROUP: "2512"}, [fields.OCCUPATION_GROUP + ".legacy_ams_taxonomy_id"], ["2512"],
                           True),
                          ({taxonomy.FIELD: "3"}, [fields.OCCUPATION_FIELD + ".legacy_ams_taxonomy_id"], ["3"], True),
                          ({taxonomy.FIELD: "-3"}, [fields.OCCUPATION_FIELD + ".legacy_ams_taxonomy_id"], ["3"], False),

                          # 0 results
                          # TODO check this
                          # ({taxonomy.OCCUPATION: "D7Ns_RG6_hD2"}, [fields.OCCUPATION + ".legacy_ams_taxonomy_id"],
                          # ["2419"], True)
                          ])
def test_occupation_codes( query, path, expected, non_negative):
    _fetch_and_validate_result( query, path, expected, non_negative)


def test_skill():
    params = {taxonomy.SKILL: 'DHhX_uVf_y6X', "limit": 100}
    json_response = get_search(params)
    for hit in json_response['hits']:
        must = "DHhX_uVf_y6X" in [skill['concept_id']
                                  for skill in hit["must_have"]["skills"]]
        should = "DHhX_uVf_y6X" in [skill['concept_id']
                                    for skill in hit["nice_to_have"]["skills"]]
        assert must or should


def test_negative_skill():
    params = {taxonomy.SKILL: '-DHhX_uVf_y6X', "limit": 100}
    json_response = get_search(params)
    for hit in json_response['hits']:
        assert "DHhX_uVf_y6X" not in [skill['concept_id']
                                      for skill in hit["must_have"]["skills"]]
        assert "DHhX_uVf_y6X" not in [skill['concept_id']
                                      for skill in hit["nice_to_have"]["skills"]]


# 0 hits
def test_worktime_extent():
    _fetch_and_validate_result( query={taxonomy.WORKTIME_EXTENT: '6YE1_gAC_R2G'},
                               resultfield=[fields.WORKING_HOURS_TYPE + ".concept_id"],
                               expected=['6YE1_gAC_R2G'])


def test_scope_of_work():
    params = {constants.PARTTIME_MIN: 50, constants.PARTTIME_MAX: 80, "limit": 100}
    json_response = get_search(params)
    including_max = False
    including_min = False
    for hit in json_response['hits']:
        assert hit['scope_of_work']['min'] >= 50
        assert hit['scope_of_work']['max'] <= 80
        if hit['scope_of_work']['min'] == 50:
            including_min = True
        if hit['scope_of_work']['max'] == 80:
            including_max = True

    assert including_min
    assert including_max


def test_driving_licence():
    params = {taxonomy.DRIVING_LICENCE: ['VTK8_WRx_GcM'], "limit": 100}
    json_response = get_search(params)
    for hit in json_response['hits']:
        concept_ids = [item['concept_id'] for item in hit[fields.DRIVING_LICENCE]]
        assert 'VTK8_WRx_GcM' in concept_ids


# 0 hits
def test_employment_type():
    _fetch_and_validate_result( {taxonomy.EMPLOYMENT_TYPE: other.vanlig_anstallning_v1},
                               [fields.EMPLOYMENT_TYPE + ".concept_id"], [other.vanlig_anstallning_v1])


@pytest.mark.parametrize('required', [True, False])
def test_experience( required):
    query = {constants.EXPERIENCE_REQUIRED: required}
    result_field = [fields.EXPERIENCE_REQUIRED]
    _fetch_and_validate_result( query, result_field, expected=[required])


def test_region():
    _fetch_and_validate_result( {taxonomy.REGION: '01'}, [fields.WORKPLACE_ADDRESS_REGION_CODE], ['01'])
    _fetch_and_validate_result( {taxonomy.REGION: '-01'}, [fields.WORKPLACE_ADDRESS_REGION_CODE], ['01'], False)

    # TODO: this test does not work with parametrize

    def test_freetext_query_synonym_param():
        json_response = get_search(params={'q': 'montessori', 'limit': '0'})
        assert json_response['freetext_concepts']['skill'][0] == 'montessoripedagogik'
        compare(json_response['total']['value'], expected=3)
