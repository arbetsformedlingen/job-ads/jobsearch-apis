import pytest
from requests.exceptions import HTTPError
from common.constants import START_SEASONAL_TIME, END_SEASONAL_TIME
from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_HISTORICAL_ADS, TEST_USE_STATIC_DATA

pytestmark = pytest.mark.historical


def _historical_seasonal_param_formatter(from_date: str, to_date: str) -> dict:
    if from_date and not to_date:
        params = {START_SEASONAL_TIME: from_date}
    elif to_date and not from_date:
        params = {END_SEASONAL_TIME: to_date}
    elif from_date and to_date:
        params = {START_SEASONAL_TIME: from_date, END_SEASONAL_TIME: to_date}
    else:
        params = {}
    return params


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_mm_dd, to_mm_dd, expected", [
    ('02-28', '05-31', 3119),
    ('02-28', '03-01', 81),
    ('01-01', '06-30', 5932),
    ('1-1', '12-31', 10727),
    ('2-2', '5-9', 3385)
])
def test_historical_season_filter( from_mm_dd, to_mm_dd, expected):
    params = {START_SEASONAL_TIME: from_mm_dd, END_SEASONAL_TIME: to_mm_dd}
    result = get_search(params)
    assert result['total']['value'] == expected, params


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("start_mm_dd,  expected", [
    ('03-01', NUMBER_OF_HISTORICAL_ADS),
])
def test_historical_season_filter_start_date( start_mm_dd, expected):
    params = {START_SEASONAL_TIME: start_mm_dd}
    result = get_search(params)
    assert result['total']['value'] == expected, params


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("end_mmdd, expected", [
    ('02-01', NUMBER_OF_HISTORICAL_ADS),
])
def test_historical_season_filter_end_date( end_mmdd, expected):
    params = {END_SEASONAL_TIME: end_mmdd}
    result = get_search(params)
    assert result['total']['value'] == expected, params


@pytest.mark.parametrize("from_mm_dd, to_mm_dd", [
    ('02-29', '05-31'),
    ('01-01', '02-29'),
    ('08-32', '11-29'),
])
def test_error( from_mm_dd, to_mm_dd):
    """
    February 29 only exists on leap years, causing an error when trying to use it for
    this filter that checks every year
    Dates that do not exist ever also raises an error
    """
    params = {END_SEASONAL_TIME: from_mm_dd, START_SEASONAL_TIME: to_mm_dd}
    with pytest.raises(HTTPError):
        get_search(params)


@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [
    (None, None, NUMBER_OF_HISTORICAL_ADS),
    ('03-01', '03-31', 1200),
    ('01-01', '12-31', 10727),
    ('04-01', '06-30', 2683),
    ('10-01', '12-31', 2592),
    (None, '08-31', NUMBER_OF_HISTORICAL_ADS),
    ('05-22', None, NUMBER_OF_HISTORICAL_ADS),
    ('08-31', '04-01', 0),  # reverse order
])
def test_historical_season( from_date, to_date, expected):
    """
    This test depends on test ads from all years
    """
    params = _historical_seasonal_param_formatter(from_date, to_date)
    result = get_search(params)
    assert result['total']['value'] == expected
