import pytest
from stream import load_ads as stream
from tests.integration_tests.test_resources import test_data


def test_dsl():
    offset = stream.offset
    actual_dsl = stream._stream_base_query()
    expected = {
        "query": {
            "bool": {
                "filter": [
                    {
                        "range": {
                            "publication_date": {
                                "lte": "now/m+%dH/m" % offset
                            }
                        }
                    },
                    {
                        "range": {
                            "last_publication_date": {
                                "gte": "now/m+%dH/m" % offset
                            }
                        }
                    }
                ]
            }
        },
    }
    assert actual_dsl == expected


@pytest.mark.parametrize("dsl, items, concept_ids, expected_result", test_data.data_for_test_add_filter_query)
def test_add_filter_query(dsl, items, concept_ids, expected_result):
    result = stream.add_filter_query(dsl[0], items, concept_ids)
    assert result == expected_result[0]


def test_format_removed_ad():
    ad = test_data.ad_for_test_format_removed_ads
    result = stream.format_removed_ad(ad)
    expected_id = '24025897'

    ad_id = result['id']
    assert isinstance(ad_id, str), f"id was of type: {type(ad_id)}"
    expected = {'id': expected_id,
                'removed': True,
                'removed_date': '2021-05-29T15:34:59',
                'occupation': 'bXNH_MNX_dUR',
                'occupation_group': 'Z8ci_bBE_tmx',
                'occupation_field': 'NYW6_mP6_vwf',
                'municipality': 'dJbx_FWY_tK6',
                'region': 'NvUF_SP1_1zo',
                'country': 'i46j_HmG_v64'}
    assert result == expected


def test_format_already_removed_ad():
    already_removed_ad = {
        'id': 24830868,
        'removed': True,
        'removed_date': '2021-06-08T09:36:38',
        'last_publication_date': None,
        'publication_date': None,
        'occupation': {'concept_id': ['p17k_znk_osi']},
        'occupation_group': {'concept_id': 'oXSW_fbY_XrY'},
        'occupation_field': {'concept_id': 'RPTn_bxG_ExZ'},

        'workplace_address': {'municipality_concept_id': 'kMxr_NiX_YrU', 'region_concept_id': 's93u_BEb_sx2',
                              'country_concept_id': 'i46j_HmG_v64'}
    }

    expected = {'id': '24830868', 'removed': True, 'removed_date': '2021-06-08T09:36:38',
                'occupation': {'concept_id': ['p17k_znk_osi']}, 'occupation_group': {'concept_id': 'oXSW_fbY_XrY'},
                'occupation_field': {'concept_id': 'RPTn_bxG_ExZ'}, 'municipality': 'kMxr_NiX_YrU',
                'region': 's93u_BEb_sx2', 'country': 'i46j_HmG_v64'}
    formatted_again = stream.format_removed_ad(already_removed_ad)

    assert formatted_again == expected
    for key in ['id', 'removed', 'removed_date', 'occupation', 'occupation_group', 'occupation_field', 'municipality',
                'region', 'country']:
        assert formatted_again.get(key, None) is not None

    with pytest.raises(KeyError):
        formatted_again['timestamp']
