import requests
from tests.test_resources.helper import get_stream_check_number_of_results, get_snapshot_raw
from common.constants import OCCUPATION_CONCEPT_ID


def test_use_api_key_stream():
    """
    Check that nothing breaks when using api-key in header
    """
    test_headers = {'api-key': 'test_api_key', 'accept': 'application/json'}
    params = {'date': '2020-12-15T00:00:01', OCCUPATION_CONCEPT_ID: "YfCD_kUE_kck"}
    get_stream_check_number_of_results(params=params, expected_number=8, headers=test_headers, )


def test_use_api_key_snapshot():
    """
    Check that nothing breaks when using api-key in header
    """
    test_headers = {'api-key': 'test_api_key', 'accept': 'application/json'}
    s = requests.sessions.Session()
    s.headers.update(test_headers)
    get_snapshot_raw()
