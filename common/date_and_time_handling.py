import calendar
import datetime

from dateutil import parser

from common import constants


def format_start_date_historical(start_date):
    """
    Will format date and add month and day if missing
    See examples in unit tests
    returns a timestamp as string
    """
    if not start_date:
        return None
    default_start = datetime.datetime(year=2000, month=1, day=1)
    return parser.parse(start_date, default=default_start).strftime(constants.DATE_FORMAT)


def format_end_date_historical(end_date):
    """
    If only year is passed, the date will be formatted to the last second of the year (yyyy-12-31T23:59:59)
    If year and month is passed, the last day of that month will be used (leap year check is included). (yyyy-mm-<last day of month>T23:59:59)
    if yyyy-mm-dd is passed, hh:mm:ss will be modified to 23:59:59
    See examples in unit tests
    returns a datetime object

    """
    if not end_date:
        return None
    elif len(end_date) == 4:  # YYYY
        dt = parser.parse(end_date)
        new_end_date = dt.replace(month=12, day=31, hour=23, minute=59, second=59)
    elif len(end_date) == 7:  # YYYY-MM
        dt = parser.parse(end_date)
        # check how many days the month has for a particular year (it handles leap years)
        number_of_days_in_month = calendar.monthrange(dt.year, dt.month)[1]
        new_end_date = dt.replace(day=number_of_days_in_month, hour=23, minute=59, second=59)
    else:
        # create a datetime object and modify hh:mm:ss
        dt = parser.parse(end_date)
        new_end_date = dt.replace(hour=23, minute=59, second=59)

    return new_end_date
