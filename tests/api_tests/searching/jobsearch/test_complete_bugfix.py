import pytest
import requests
from tests.test_resources.test_settings import TEST_URL, test_headers
from tests.test_resources.helper import get_complete

# mark all tests in this file as @pytest.mark.jobsearch
pytestmark = [pytest.mark.jobsearch]


@pytest.mark.skip(reason="The bug fix NAR-1178 has been temporarily reverted")
@pytest.mark.parametrize("query, query_misspelled", [("undersköterska", "undersjköterska")])
def test_error_nar_1178(query, query_misspelled):
    """
    NAR-1178: wrong occurrences in /complete, when
    contextual = false and term is misspelled
    """
    typeahead_result0 = get_complete(params={'q': query, 'contextual': True})['typeahead']
    typeahead_result1 = get_complete(params={'q': query_misspelled, 'contextual': True})['typeahead']
    typeahead_result2 = get_complete(params={'q': query, 'contextual': False})['typeahead']
    typeahead_result3 = get_complete(params={'q': query_misspelled, 'contextual': False})['typeahead']
    assert typeahead_result2 == typeahead_result0
    assert typeahead_result3 == typeahead_result1
    assert [item['occurrences'] for item in typeahead_result3] \
           == [item['occurrences'] for item in typeahead_result0]


working_queries = ['Systemutvecklare / Programmerare', 'v[rd o', 'kock) l' '"jobba på', 'Vitvaror & Malmö', 'M] l',
                   'på distans ', 'v< undersköterska < ', 'kjell ! co', '< undersköterska', ' < undersköterska',
                   'Djurtekniker ( 1-2 st ', 'Djurtekniker ( 1-2 st',
                   'Djurtekniker [1-2 st ', 'Lokalvård / Städare (med B-körkort', 'M) l', 'kjell & co']


@pytest.mark.smoke
@pytest.mark.parametrize("query", working_queries)
def test_error_nar_862_b(query):
    """
    test with queries that gave http 500 response
    using request for each query to make them independent (no connection reuse)
    """
    url = f"{TEST_URL}/complete"
    r = requests.get(url, params={'q': query}, headers=test_headers)
    r.raise_for_status()


def test_error_nar_1552():
    """
    'sverige' or 'svenska' should not show up as suggestions as second word after first word + space
    """
    result = get_complete(params={'q': 'sjuksköterska '})
    typeahead = result['typeahead']
    unwanted_words = ['sverige', 'svenska']
    for suggestion in typeahead:
        value = suggestion['value']
        for unwanted in unwanted_words:
            assert unwanted not in value, f"Error: '{unwanted}' found in '{value}'"


def test_error_nar_1552_b():
    """
    'sverige' or 'svenska' may  show up as suggestions as second word if
    """
    result = get_complete(params={'q': 'sjuksköterska s'})
    typeahead = result['typeahead']
    assert 'sverige' in typeahead[0]['value']
    assert 'svenska' in typeahead[1]['value']
