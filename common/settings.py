import os

# Elasticsearch settings
ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")
ES_INDEX = os.getenv("ES_INDEX", "platsannons-read")
ES_USE_SSL = os.getenv("ES_USE_SSL", "TRUE").upper()

ES_TAX_INDEX_ALIAS = os.getenv("ES_TAX_INDEX_ALIAS", "taxonomy")
ES_RETRIES = int(os.getenv("ES_RETRIES", 1))
ES_RETRIES_SLEEP = int(os.getenv("ES_RETRIES_SLEEP", 2))

ES_HISTORICAL_TIMEOUT = int(os.getenv("ES_HISTORICAL_TIMEOUT", 300))
ES_DEFAULT_TIMEOUT = int(os.getenv("ES_DEFAULT_TIMEOUT", 10))

DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP = os.getenv('DELAY_LOAD_SYNONYM_DICTIONARY_STARTUP', 'false').lower() == 'true'

BASE_PB_URL = os.getenv('BASE_PB_URL', 'https://arbetsformedlingen.se/platsbanken/annonser/')

COMPANY_LOGO_BASE_URL = os.getenv('COMPANY_LOGO_BASE_URL',
                                  'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/')
COMPANY_LOGO_FETCH_DISABLED = os.getenv('COMPANY_LOGO_FETCH_DISABLED', 'false').lower() == 'true'

ADS_LABELS_CACHE_EXP_SECONDS = int(os.getenv("ADS_LABELS_CACHE_EXP_SECONDS", 600))

HISTORICAL_START_YEAR = int(os.getenv("HISTORICAL_START_YEAR", 2016))

JAE_API_URL = os.getenv("JAE_API_URL", "https://jobad-enrichments-api.jobtechdev.se")

TAXONOMY_PROCESSES = int(os.getenv("TAXONOMY_PROCESSES", 8))

JOBSEARCH_HOST=os.getenv("JOBSEARCH_HOST", "https://jobsearch.api.jobtechdev.se")
