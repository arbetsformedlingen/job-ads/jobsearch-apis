import pytest
import requests

from common.constants import ABROAD, OCCUPATION_CONCEPT_ID, LOCATION_CONCEPT_ID
from tests.test_resources.concept_ids import concept_ids_geo as geo, occupation as work, occupation_group as group

from tests.test_resources.helper import get_stream_expect_error, get_stream_check_number_of_results, get_stream
from tests.test_resources.test_settings import DAWN_OF_TIME

# mark all tests in this file as @pytest.mark.jobstream
pytestmark = pytest.mark.jobstream


@pytest.mark.parametrize("wrong_date",
                         ['2020-13-25T00:00:00', '2020-034-25T00:00:00', '20-00-25T00:00:00', '0001-00-01',
                          'T11:28:00'])
def test_wrong_date_format(  wrong_date):
    get_stream_expect_error(  params={'date': wrong_date}, expected_http_code=requests.codes.bad_request)


@pytest.mark.parametrize('type, value', [
    (OCCUPATION_CONCEPT_ID, work.bartender),
    (LOCATION_CONCEPT_ID, geo.stockholm)])
def test_filter_without_date_expect_bad_request_response(  type, value):
    """
    test that a 'bad request' response (http 400) is returned when doing a request without date parameter
    """
    get_stream_expect_error(  params={type: value}, expected_http_code=requests.codes.bad_request)


@pytest.mark.parametrize('work, expected_number_of_hits', [
    (group.mjukvaru__och_systemutvecklare_m_fl_, 239),
    (group.mjukvaru__och_systemutvecklare_m_fl_.lower(), 0)])
def test_filter_with_lowercase_concept_id(  work, expected_number_of_hits):
    """
    compare correct concept_id with a lower case version
    """
    params = {'date': DAWN_OF_TIME, OCCUPATION_CONCEPT_ID: work}
    get_stream_check_number_of_results(  expected_number_of_hits, params)


@pytest.mark.parametrize("abroad", [True, False])
def test_work_abroad(  abroad):
    """
    Check that param 'arbete-utomlands' returns http 400 BAD REQUEST for stream
    """
    get_stream_expect_error(  {ABROAD: abroad}, expected_http_code=requests.codes.bad_request)


def test_response_format():
    hits = get_stream(  params={'date': '2021-03-30T00:00:00'})

    for hit in hits:
        for x in ['occupation', 'occupation_group', 'occupation_field']:
            assert isinstance(hit[x], dict), f"ERROR: expected a dict, but got {type(hit[x])}"
