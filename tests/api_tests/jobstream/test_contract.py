# coding=utf-8
import pytest
import tests.test_resources.ad_fields_data_type as type_check
from tests.test_resources.test_settings import STREAM, SNAPSHOT

"""
ads are loaded once in fixture and reused in all tests using that fixture to save time
The pytest fixture returns ads from snapshot or stream endpoint depending on the parameter
"""
pytestmark = pytest.mark.smoke

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_types_top_level(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_types_top_level(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_types_occupation(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_types_occupation(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_duration(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_duration(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_employment_type(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_employment_type(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_salary_type(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_salary_type(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_types_employer(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_types_employer(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_scope_of_work(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_scope_of_work(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_application_details(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_application_details(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_job_ad_description(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_job_ad_description(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_workplace_address(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_workplace_address(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_requirements(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_requirements(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_working_hours_type(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_working_hours_type(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_contact_persons(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_types_contact(ad)

@pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
def test_length(jobstream_fixture):
    for ad in jobstream_fixture:
        type_check.check_length(ad, 36)
