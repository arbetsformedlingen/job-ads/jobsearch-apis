import logging
import time
from flask_restx import abort
from flask_restx import Resource

from common import constants
from search.common_search.ad_search import fetch_ad_by_id
from search.common_search.querybuilder import QueryBuilderType
from search.common_search.stats.stats import get_stats
from search.rest.endpoint.common import QueryBuilderContainer, \
    search_endpoint
from common.ad_formatter import format_hits_with_only_original_values
from search.rest.model.apis import ns_historical, open_results, job_ad
from search.rest.model.queries import load_ad_query, historical_query, stats_query
from search.rest.model.swagger import swagger_doc_params, swagger_filter_doc_params

log = logging.getLogger(__name__)

querybuilder_container = QueryBuilderContainer()


@ns_historical.route('ad/<id>', endpoint='ad')
class AdById(Resource):

    @ns_historical.doc(
        description='Load a job ad by ID',
    )
    @ns_historical.response(404, 'Job ad not found')
    @ns_historical.expect(load_ad_query)
    @ns_historical.marshal_with(job_ad)
    def get(self, id, **kwargs):
        result = fetch_ad_by_id(str(id), historical=True)
        if result.get('removed'):
            abort(404, 'Ad not found')
        else:
            return format_hits_with_only_original_values([result])[0]


@ns_historical.route('search')
class Search(Resource):

    @ns_historical.doc(
        description='Search using parameters and/or freetext',
        params={
            constants.HISTORICAL_FROM: 'Search ads from this date',
            constants.HISTORICAL_TO: 'Search ad until this date',
            constants.START_SEASONAL_TIME: 'Search seasonal ads from this date, date type MM-DD',
            constants.END_SEASONAL_TIME: 'Search seasonal ads until this date, date type MM-DD',
            constants.HISTORICAL_REQUEST_TIMEOUT: 'Set the query timeout in seconds (some historical queries may take several minutes)',
            **swagger_doc_params, **swagger_filter_doc_params
        },
    )
    @ns_historical.expect(historical_query)
    @ns_historical.marshal_with(open_results)
    def get(self, **kwargs):
        start_time = int(time.time() * 1000)
        args = historical_query.parse_args()
        return search_endpoint(querybuilder_container.get(QueryBuilderType.HISTORICAL_SEARCH), args, start_time)


@ns_historical.route('stats')
class Stats(Resource):

    @ns_historical.doc(
        description='Load stats by taxonomy type',
        params={
            constants.TAXONOMY_TYPE: "One or more taxonomy type, default will return all (available fields: "
                                    "occupation-name, occupation-group, occupation-field, employment-type,"
                                    " country, region, municipality, skill, language). ",
            constants.STATS_BY: "Search taxonomy type with different fields",
            constants.LIMIT: "Maximum number of statistical rows per taxonomy field"
        },
    )
    @ns_historical.expect(stats_query)
    def get(self, **kwargs):
        args = stats_query.parse_args()
        return get_stats(args)
