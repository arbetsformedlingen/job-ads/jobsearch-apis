import pytest

from tests.test_resources.helper import get_complete, get_typeahead_values, check_complete_results

pytestmark = [pytest.mark.jobsearch]

@pytest.mark.parametrize("query, expected_typeahead", [
    ("", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
          'västra götalands län', 'skåne', 'skåne län']),
    (" ", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
           'västra götalands län', 'skåne', 'skåne län']),
    ("\t", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
            'västra götalands län', 'skåne', 'skåne län']),
    ("qwerty", []),
    ("qwerty ",
     ['qwerty stockholms län', 'qwerty körkort', 'qwerty engelska', 'qwerty stockholm', 'qwerty västra götaland',
      'qwerty västra götalands län', 'qwerty skåne', 'qwerty skåne län', 'qwerty b-körkort', 'qwerty sjuksköterska']
     ),
])
def test_complete_basic_tests( query, expected_typeahead):
    """
    Testing basic & extreme cases (including whitespaces)
    """
    json_response = get_complete(params={'q': query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected_typeahead)




@pytest.mark.jobsearch
@pytest.mark.parametrize("query, expected_suggestions", [
    ('systemutvecklare angular',
     ['systemutvecklare angularjs', 'systemutvecklare angular', 'systemutvecklare angular.js']),
    ('#coro', ['coordinator', 'crona lön', 'dorotea', 'corona', 'coo']),
    ('#coron', ['crona lön', 'corona']),
    ('c',
     ['civilingenjör', 'cloud', 'c#', 'coachning', 'c++', 'chaufför', 'cad', 'ci/cd', 'c-körkort', 'css', 'compliance', 'crm', 'chaufförer', 'customer service', 'certifikat', 'ce-körkort', 'civilingenjörsutbildning', 'configuration', 'controller', 'cnc- operatör', 'cnc-operatör', 'coordinator', 'ce-chaufför', 'cafébiträde', 'customer advisor', 'c-chaufför', 'chief information officer', 'cio', 'co', 'coach', 'cypern', 'charlottenberg']

     ),
    ('uppd', ['uppdragsledning', 'uppdragsutbildning', 'uppdragsutbildningar', 'uppdragsansvarig', 'uppdragsledare',
              'uppdragsanalyser', 'uppdragsansvar', 'uppdragsledarerfarenhet', 'uppdragsverksamhet', 'uppdukning']),
    ('underh',
     ['underhållsarbete', 'underhållsmekaniker', 'underhållning', 'underhållsarbeten', 'underhållsplaner',
      'underhållsprojekt', 'underhållsingenjör', 'underhållstekniker', 'underhåll och reparation', 'underhållsfrågor',
      'underhållssystem', 'underhållsteknik', 'underhållsavdelning', 'underhållsplanering', 'underhållsverksamhet',
      'underhållsanalytiker', 'underhållsansvarig', 'underhållschef', 'underhållsledare', 'underhållsansvar',
      'underhållsberedning', 'underhållsbesiktning', 'underhållare', 'underhållsmontör', 'underhållsplanerare']
     ),
    ('sjuks',
     ['sjuksköterska', 'sjuksköterskor', 'sjuksköterskeuppgifter', 'sjuksköterskelegitimation', 'sjuksköterskeexamen',
      'sjuksköterskan', 'sjuksköterskearbete', 'sjuksköterskemottagning', 'sjuksköterskeutbildning',
      'sjukskrivningsfrågor', 'sjuksköterska inom psykiatri', 'sjuksköterskeassistent']),
    ('arbetsl',
     ['arbetslivserfarenhet', 'arbetsledning', 'arbetsledare', 'arbetsliv', 'arbetslivspsykologi', 'arbetslivsfrågor',
      'arbetslivserfarenheter', 'arbetslagsarbete', 'arbetsledaransvar', 'arbetsledarerfarenhet', 'arbetsledarskap',
      'arbetslivsinriktad rehabilitering', 'arbetslagsledare']),

])
def test_complete_endpoint_with_spellcheck_typeahead( query, expected_suggestions):
    """
    test of /complete endpoint
    parameters: query and list of expected result(s)
    """
    json_response = get_complete(params={'q': query, 'limit': 50, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected_suggestions)


@pytest.mark.jobsearch
@pytest.mark.parametrize("query, query_2, expected_typeahead", [
    ("stor", "",
     ['storkök', 'storage', 'storhushåll', 'storstädning', 'storköksutbildning', 'stora datamängder', 'storstädningar',
      'storbritannien', 'storuman', 'storhushållsutbildning']

     ),
    ("stor", "s",
     ['stor sverige', 'stor svenska', 'stor stockholms län', 'stor stockholm', 'stor skåne', 'stor skåne län',
      'stor sjuksköterska', 'stor sjukvård', 'stor säljare', 'stor systemutvecklare']),
    ("storage", "",
     ['storage', 'storage stockholm', 'storage stockholms län', 'storage göteborg', 'storage västra götaland',
      'storage västra götalands län']),
    ("storage", "s",
     ['storage sverige', 'storage svenska', 'storage stockholms län', 'storage stockholm', 'storage skåne',
      'storage skåne län', 'storage sjuksköterska', 'storage sjukvård', 'storage säljare', 'storage systemutvecklare']
     ),
])
def test_complete_multiple_words( query, query_2, expected_typeahead):
    """
    Test typeahead with two words
    """
    if query_2 == "":
        full_query = query
    else:
        full_query = query + ' ' + query_2
    json_response = get_complete({'q': full_query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected_typeahead)


@pytest.mark.jobsearch
@pytest.mark.parametrize("query, expected", [
    ("göteborg sjuksköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'göteborg sjuksköterska inom psykiatri']),
    ("götteborg sjuksköterska",
     ['götteborg sjuksköterska', 'götteborg sjuksköterskan', 'götteborg sjuksköterska inom psykiatri']),

    ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska',
      'götteborg sjuksköterskan']),
    ("göteborg sjukssköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']),
    ("göteborg sjukssköterska läkkare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterska väktare',
      'göteborg sjuksköterskan lärare', 'göteborg sjuksköterska mäklare', 'göteborg sjuksköterska lättare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läkkare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']),
    ("göteborg sjukssköterska lääkare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjukssköterska lärare', 'göteborg sjuksköterska lääkare',
      'göteborg sjuksköterskan läkare', 'göteborg sjukssköterska läkare', 'göteborg sjuksköterskan lättare',
      'göteborg sjuksköterskan lääkare']),
    ("göteborg sjukssköterska läkare", ['göteborg sjukssköterska läkare']),
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']
     ),
    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stokholm ",
     ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
      'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
      'stokholm b-körkort', 'stokholm sjuksköterska']

     ),  # trailing space
    ("stokholm  ",
     ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
      'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
      'stokholm b-körkort', 'stokholm sjuksköterska']

     ),  # 2 trailing spaces
    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ",
     ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
      'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
      'stockhlm b-körkort', 'stockhlm sjuksköterska']

     ),  # trailing space
    ("   stockhlm   ",
     ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
      'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
      'stockhlm b-körkort', 'stockhlm sjuksköterska']

     ),  # leading + trailing spaces
    ("stokholm lärarre",
     ['stockholms lärare', 'stockholm lärare', 'stockholms läkare', 'stockholm läkare', 'stockholms lärande',
      'stockholm lärande', 'stockholms bärare', 'stockholm bärare', 'stockholms läraren', 'stockholm läraren']
     ),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']
     ),
    ("göteborg läckare sjukssköterska ",
     ['göteborg läckare sjukssköterska stockholms län', 'göteborg läckare sjukssköterska körkort',
      'göteborg läckare sjukssköterska engelska', 'göteborg läckare sjukssköterska stockholm',
      'göteborg läckare sjukssköterska västra götaland', 'göteborg läckare sjukssköterska västra götalands län',
      'göteborg läckare sjukssköterska skåne', 'göteborg läckare sjukssköterska skåne län',
      'göteborg läckare sjukssköterska b-körkort', 'göteborg läckare sjukssköterska sjuksköterska']

     ),
    ("göteborg läckare sjukssköterska",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']
     ),
    ("läckare götteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
      'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']
     ),
    ("läckare göteborg", ['läckare göteborg']),
    ("stockholm läckare göteborg", ['stockholm läckare göteborg']),
    ("stockhlm läckare göteborg", ['stockhlm läckare göteborg']),
    ("stockhlm läckare göteborg ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
                                    'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
                                    'stockhlm läckare göteborg västra götaland',
                                    'stockhlm läckare göteborg västra götalands län', 'stockhlm läckare göteborg skåne',
                                    'stockhlm läckare göteborg skåne län', 'stockhlm läckare göteborg b-körkort',
                                    'stockhlm läckare göteborg sjuksköterska']

     ),  # trailing space
    ("stockhlm läckare göteborg  ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
                                     'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
                                     'stockhlm läckare göteborg västra götaland',
                                     'stockhlm läckare göteborg västra götalands län',
                                     'stockhlm läckare göteborg skåne', 'stockhlm läckare göteborg skåne län',
                                     'stockhlm läckare göteborg b-körkort', 'stockhlm läckare göteborg sjuksköterska']

     ),  # 2 trailing space
])
def test_complete_spelling_correction_multiple_words(query, expected):
    """
    Test typeahead with multiple (misspelled) words
    """
    json_response = get_complete({'q': query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.jobsearch
@pytest.mark.parametrize("query, expected", [
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']
     ),
    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stokholm ",
     ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
      'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
      'stokholm b-körkort', 'stokholm sjuksköterska']
     ),  # trailing space
    ("stokholm  ",
     ['stokholm stockholms län', 'stokholm körkort', 'stokholm engelska', 'stokholm stockholm',
      'stokholm västra götaland', 'stokholm västra götalands län', 'stokholm skåne', 'stokholm skåne län',
      'stokholm b-körkort', 'stokholm sjuksköterska']
     ),  # 2 trailing spaces
    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ",
     ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
      'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
      'stockhlm b-körkort', 'stockhlm sjuksköterska']
     ),  # trailing space
    ("   stockhlm   ",
     ['stockhlm stockholms län', 'stockhlm körkort', 'stockhlm engelska', 'stockhlm stockholm',
      'stockhlm västra götaland', 'stockhlm västra götalands län', 'stockhlm skåne', 'stockhlm skåne län',
      'stockhlm b-körkort', 'stockhlm sjuksköterska']

     ),  # leading + trailing spaces
    ("stockholm pythan", ['stockholm python', 'stockholm python-utvecklare']),
    ("läkare götteborg", ['läkare göteborg']),
    ("läkare götteborg", ['läkare göteborg']),
])
def test_spelling_correction_with_complete_suggest( query, expected):
    """
    Test typeahead with only one (misspelled) word
    """
    json_response = get_complete({'q': query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska',
      'götteborg sjuksköterskan']),
    ("stokholm lärarre",
     ['stockholms lärare', 'stockholm lärare', 'stockholms läkare', 'stockholm läkare', 'stockholms lärande',
      'stockholm lärande', 'stockholms bärare', 'stockholm bärare', 'stockholms läraren', 'stockholm läraren']),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']),
    ("göteborg läckare sjukssköterska ",
     ['göteborg läckare sjukssköterska stockholms län', 'göteborg läckare sjukssköterska körkort',
      'göteborg läckare sjukssköterska engelska', 'göteborg läckare sjukssköterska stockholm',
      'göteborg läckare sjukssköterska västra götaland', 'göteborg läckare sjukssköterska västra götalands län',
      'göteborg läckare sjukssköterska skåne', 'göteborg läckare sjukssköterska skåne län',
      'göteborg läckare sjukssköterska b-körkort', 'göteborg läckare sjukssköterska sjuksköterska']
     ),
    ("göteborg läckare sjukssköterska",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']),
    ("läckare götteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
      'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']),
    ("läckare göteborg", ['läckare göteborg']),
    ("stockholm läckare göteborg", ['stockholm läckare göteborg']),
    ("stockhlm läckare göteborg", ['stockhlm läckare göteborg']),
    ("stockhlm läckare göteborg ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
                                    'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
                                    'stockhlm läckare göteborg västra götaland',
                                    'stockhlm läckare göteborg västra götalands län', 'stockhlm läckare göteborg skåne',
                                    'stockhlm läckare göteborg skåne län', 'stockhlm läckare göteborg b-körkort',
                                    'stockhlm läckare göteborg sjuksköterska']

     ),  # trailing space
    ("stockhlm läckare göteborg  ", ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
                                     'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
                                     'stockhlm läckare göteborg västra götaland',
                                     'stockhlm läckare göteborg västra götalands län',
                                     'stockhlm läckare göteborg skåne', 'stockhlm läckare göteborg skåne län',
                                     'stockhlm läckare göteborg b-körkort', 'stockhlm läckare göteborg sjuksköterska']

     ),  # 2 trailing spaces
    ("   stockhlm läckare göteborg   ",
     ['stockhlm läckare göteborg stockholms län', 'stockhlm läckare göteborg körkort',
      'stockhlm läckare göteborg engelska', 'stockhlm läckare göteborg stockholm',
      'stockhlm läckare göteborg västra götaland', 'stockhlm läckare göteborg västra götalands län',
      'stockhlm läckare göteborg skåne', 'stockhlm läckare göteborg skåne län', 'stockhlm läckare göteborg b-körkort',
      'stockhlm läckare göteborg sjuksköterska']

     ),  # leading + trailing spaces
])
def test_spelling_correction_with_phrase_suggest( query, expected):
    """
    Test typeahead with several (misspelled) words
    """
    json_response = get_complete({'q': query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.jobsearch
@pytest.mark.parametrize("query, expected", [
    ("python ",
     ['python stockholms län', 'python körkort', 'python engelska', 'python stockholm', 'python västra götaland',
      'python västra götalands län', 'python skåne', 'python skåne län', 'python b-körkort', 'python sjuksköterska']
     ),
    ("python  ",
     ['python stockholms län', 'python körkort', 'python engelska', 'python stockholm', 'python västra götaland',
      'python västra götalands län', 'python skåne', 'python skåne län', 'python b-körkort', 'python sjuksköterska']

     ),  # 2 trailing spaces
    ("läkare ",
     ['läkare stockholms län', 'läkare körkort', 'läkare engelska', 'läkare stockholm', 'läkare västra götaland',
      'läkare västra götalands län', 'läkare skåne', 'läkare skåne län', 'läkare b-körkort', 'läkare sjuksköterska']

     ),
    ("   läkare   ",
     ['läkare stockholms län', 'läkare körkort', 'läkare engelska', 'läkare stockholm', 'läkare västra götaland',
      'läkare västra götalands län', 'läkare skåne', 'läkare skåne län', 'läkare b-körkort', 'läkare sjuksköterska']

     ),  # leading + trailing spaces
])
# TODO: A real test case is missing here, that really deletes something
# TODO: Can there even be a real test case for this when contextual=False?
def test_remove_same_word_function( query, expected):
    """
    Test when input is word with space, the result will not show the same word
    """
    json_response = get_complete({'q': query, 'contextual': False})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)
