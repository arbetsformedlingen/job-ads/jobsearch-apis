import json

import pytest
import requests

from tests.test_resources.helper import get_complete, get_complete_expect_error, compare_two_lists, \
    get_typeahead_values, check_complete_results

pytestmark = [pytest.mark.jobsearch]


@pytest.mark.parametrize("query, expected_typeahead", [
    ("", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
          'västra götalands län', 'skåne', 'skåne län']),
    (" ", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
           'västra götalands län', 'skåne', 'skåne län']),
    ("\t", ['sverige', 'svenska', 'stockholms län', 'körkort', 'engelska', 'stockholm', 'västra götaland',
            'västra götalands län', 'skåne', 'skåne län']),
    ("qwerty", []),
    ("qwerty ", []),
    ("java      python ",
     ['java python stockholms län', 'java python stockholm', 'java python systemutvecklare',
      'java python programmerare', 'java python engelska', 'java python västra götaland',
      'java python västra götalands län', 'java python design', 'java python göteborg', 'java python data']

     ),
    ("java\tpython ",
     ['java python stockholms län', 'java python stockholm', 'java python systemutvecklare',
      'java python programmerare', 'java python engelska', 'java python västra götaland',
      'java python västra götalands län', 'java python design', 'java python göteborg', 'java python data']

     ),
    ("java\t python ",
     ['java python stockholms län', 'java python stockholm', 'java python systemutvecklare',
      'java python programmerare', 'java python engelska', 'java python västra götaland',
      'java python västra götalands län', 'java python design', 'java python göteborg', 'java python data']

     ),
])
def test_complete_basic_tests(query, expected_typeahead):
    """
    Testing basic & extreme cases (including whitespaces)
    """
    json_response = get_complete(params={'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected_typeahead)


@pytest.mark.parametrize("contextual", [True, False])
@pytest.mark.parametrize("query, expected", [('servit', ['servitris', 'servitör', 'servitriser', 'servitörer']),
                                             ('systemutvecklare angu', ['systemutvecklare angular',
                                                                        'systemutvecklare angularjs']),
                                             ('angu', ['angular', 'angularjs', 'angular.js']),
                                             ('ang', ['angularjs', 'angular', 'angular.js', 'angered']),
                                             ('pyth', ['python', 'python-utvecklare', 'pythonutvecklare'])])
def test_complete_endpoint_synonyms_typeahead(query, expected, contextual):
    """
    Test that incomplete search queries will return suggestions
    first arg is the query
    second arg is a list of expected synonyms, which all must be found in the response
    """
    json_response = get_complete(params={'q': query, 'contexual': contextual})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("contextual", [True, False])
@pytest.mark.parametrize("query, expected",
                         [('c#', ['c#', 'c#.net']), ('c+', ['c++', 'c++-utvecklare', 'c++ developer'])])
def test_synonyms_special_chars(query, expected, contextual):
    json_response = get_complete(params={'q': query, 'contexual': contextual})
    typeahead_values = [item['value'] for item in json_response['typeahead']]
    check_complete_results(query, actual=typeahead_values, expected=expected)


@pytest.mark.parametrize("query, expected_suggestions", [
    ('systemutvecklare angular', ['systemutvecklare angularjs', 'systemutvecklare angular']),
    ('#coro', ['coordinator', 'crona lön', 'dorotea', 'corona', 'coo']),
    ('#coron', ['crona lön', 'corona']),
    ('c',
     ['civilingenjör', 'cloud', 'c#', 'coachning', 'c++', 'chaufför', 'cad', 'ci/cd', 'c-körkort', 'css']),
    ('uppd', ['uppdragsledning', 'uppdragsutbildning', 'uppdragsutbildningar', 'uppdragsansvarig', 'uppdragsledare',
              'uppdragsanalyser', 'uppdragsansvar', 'uppdragsledarerfarenhet', 'uppdragsverksamhet', 'uppdukning']),
    ('underh',
     ['underhållsarbete', 'underhållsmekaniker', 'underhållning', 'underhållsarbeten', 'underhållsplaner',
      'underhållsprojekt', 'underhållssystem', 'underhållsingenjör', 'underhållstekniker', 'underhåll och reparation']
     ),
    ('sjuks',
     ['sjuksköterska', 'sjuksköterskor', 'sjuksköterskeuppgifter', 'sjuksköterskelegitimation', 'sjuksköterskeexamen',
      'sjuksköterskan', 'sjuksköterskearbete', 'sjuksköterskemottagning', 'sjuksköterskeutbildning',
      'sjukskrivningsfrågor']
     ),
    ('arbetsl',
     ['arbetslivserfarenhet', 'arbetsledning', 'arbetsledare', 'arbetsliv', 'arbetslivspsykologi', 'arbetslivsfrågor',
      'arbetslivserfarenheter', 'arbetslagsarbete', 'arbetsledaransvar', 'arbetsledarerfarenhet']
     ),

])
def test_complete_endpoint_with_spellcheck_typeahead(query, expected_suggestions):
    """
    test of /complete endpoint
    parameters: query and list of expected result(s)
    """
    json_response = get_complete(params={'q': query})
    compare_two_lists(get_typeahead_values(json_response), expected_suggestions)


def test_check_400_bad_request_when_limit_is_greater_than_allowed():
    """
    Test that a limit of 51 will give a '400 BAD REQUEST' response with a meaningful error message
    """
    response = get_complete_expect_error({'q': 'x', 'limit': 51},
                                         expected_http_code=requests.codes.bad_request)
    response_json = json.loads(response.content.decode('utf8'))
    assert response_json['errors']['limit'] == 'Invalid argument: 51. argument must be within the range 0 - 50'
    assert response_json['message'] == 'Input payload validation failed'


@pytest.mark.parametrize("query, query_2, expected_typeahead", [
    ("stor", "",
     ['storkök', 'storage', 'storhushåll', 'storstädning', 'storköksutbildning', 'stora datamängder', 'storstädningar',
      'storbritannien', 'storuman', 'storhushållsutbildning']
     ),
    ("stor", "s",
     ['stor sverige', 'stor svenska', 'stor stockholms län', 'stor stockholm', 'stor skåne', 'stor skåne län',
      'stor sjukvård', 'stor sjuksköterska', 'stor samverkan', 'stor språkkunskaper']),
    ("storage", "",
     ['storage', 'storage stockholm', 'storage stockholms län', 'storage göteborg', 'storage västra götaland',
      'storage västra götalands län']),
    ("storage", "s",
     ['storage sverige', 'storage stockholm', 'storage stockholms län', 'storage sales', 'storage svenska',
      'storage scala', 'storage sql', 'storage switch', 'storage systemutvecklare', 'storage sampling']),
])
def test_complete_multiple_words(query, query_2, expected_typeahead):
    """
    Test typeahead with two words
    """
    if query_2 == "":
        full_query = query
    else:
        full_query = query + ' ' + query_2
    json_response = get_complete(params={'q': full_query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected_typeahead)


@pytest.mark.parametrize("contextual", [True, False])
def test_complete_for_locations_with_space_and_contextual_param(contextual):
    """
    Test typeahead for location with trailing space after city name,
    and using parameter 'contextual' True or False
    """
    query = 'malmö '
    expected = ['malmö försäljare',
                'malmö säljare',
                'malmö sjuksköterska',
                'malmö innesäljare',
                'malmö lärare',
                'malmö telefonförsäljare']

    json_response = get_complete({'q': query, 'limit': 10, 'contextual': contextual})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("göteborg sjuksköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
    ("götteborg sjuksköterska", ['göteborg sjuksköterska']),

    ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska',
      'götteborg sjuksköterskan']),
    ("göteborg sjukssköterska", ['göteborg sjuksköterska', 'göteborg sjuksköterskan']),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']
     ),
    ("göteborg sjukssköterska läkkare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterska väktare',
      'göteborg sjuksköterskan lärare', 'göteborg sjuksköterska mäklare', 'göteborg sjuksköterska lättare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läkkare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']
     ),
    ("göteborg sjukssköterska lääkare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjukssköterska lärare', 'göteborg sjuksköterska lääkare',
      'göteborg sjuksköterskan läkare', 'göteborg sjukssköterska läkare', 'göteborg sjuksköterskan lättare',
      'göteborg sjuksköterskan lääkare']
     ),
    ("göteborg sjukssköterska läkare", ['göteborg sjuksköterska läkare', 'göteborg sjuksköterskan läkare']),
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']

     ),
    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stokholm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("stokholm  ", ['stockholms län', 'stockholm', 'stockholm city']),  # 2 trailing spaces
    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("   stockhlm   ", ['stockholms län', 'stockholm', 'stockholm city']),  # leading + trailing spaces

    ("stokholm lärarre",
     ['stockholms lärare', 'stockholm lärare', 'stockholms läkare', 'stockholm läkare', 'stockholms lärande',
      'stockholm lärande', 'stockholms bärare', 'stockholm bärare', 'stockholms läraren', 'stockholm läraren']
     ),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']
     ),
    ("göteborg läckare sjukssköterska ",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']
     ),
    ("göteborg läckare sjukssköterska",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']
     ),
    ("läckare götteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
      'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']
     ),
    ("läckare göteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg']),
    ("stockholm läckare göteborg",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholm packare göteborg', 'stockholm plockare göteborg']),
    ("stockhlm läckare göteborg",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),
    ("stockhlm läckare göteborg ",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # trailing space
    ("stockhlm läckare göteborg  ",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # 2 trailing space
])
def test_complete_spelling_correction_multiple_words(query, expected):
    """
    Test typeahead with multiple (misspelled) words
    """
    json_response = get_complete(params={'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("läckare", ['läkare', 'läkare stockholms län', 'läkare skåne', 'läkare skåne län', 'läkare stockholm',
                 'läkare jönköpings län']),
    ("götteborg", ['göteborg', 'göteborg systemutvecklare', 'göteborg sjuksköterska', 'göteborg civilingenjör',
                   'göteborg mjukvaruutvecklare', 'göteborg programmerare', 'göteborg personlig assistent']

     ),

    ("stokholm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stokholm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("stokholm  ", ['stockholms län', 'stockholm', 'stockholm city']),  # 2 trailing spaces
    ("stockhlm", ['stockholms län', 'stockholm', 'stockholm city']),
    ("stockhlm ", ['stockholms län', 'stockholm', 'stockholm city']),  # trailing space
    ("   stockhlm   ", ['stockholms län', 'stockholm', 'stockholm city']),  # leading + trailing spaces

    ("stockholm pythan", ['stockholm python', 'stockholm python-utvecklare']),
    ("läkare götteborg", ['läkare göteborg']),
    ("läkare götteborg", ['läkare göteborg']),
])
def test_spelling_correction_with_complete_suggest(query, expected):
    """
    Test typeahead with only one (misspelled) word
    """
    json_response = get_complete(params={'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("götteborg sjukssköterska",
     ['göteborg sjuksköterska', 'göteborg sjuksköterskan', 'götteborg sjuksköterska', 'göteborg sjukssköterska',
      'götteborg sjuksköterskan']
     ),
    ("stokholm lärarre",
     ['stockholms lärare', 'stockholm lärare', 'stockholms läkare', 'stockholm läkare', 'stockholms lärande',
      'stockholm lärande', 'stockholms bärare', 'stockholm bärare', 'stockholms läraren', 'stockholm läraren']
     ),
    ("göteborg sjukssköterska läckare",
     ['göteborg sjuksköterska lärare', 'göteborg sjuksköterska läkare', 'göteborg sjuksköterskan lärare',
      'göteborg sjuksköterska lättare', 'göteborg sjuksköterska packare', 'göteborg sjuksköterska plockare',
      'göteborg sjukssköterska lärare', 'göteborg sjuksköterska läckare', 'göteborg sjuksköterskan läkare',
      'göteborg sjukssköterska läkare']
     ),
    ("göteborg läckare sjukssköterska ",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']
     ),
    ("göteborg läckare sjukssköterska",
     ['göteborg lärare sjuksköterska', 'göteborg läkare sjuksköterska', 'göteborg lärare sjuksköterskan',
      'göteborg lättare sjuksköterska', 'göteborg packare sjuksköterska', 'göteborg plockare sjuksköterska',
      'göteborg lärare sjukssköterska', 'göteborg läckare sjuksköterska', 'göteborg läkare sjuksköterskan',
      'göteborg läkare sjukssköterska']
     ),
    ("läckare götteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg',
      'lärare götteborg', 'läckare göteborg', 'läkare götteborg', 'lättare götteborg', 'packare götteborg']
     ),
    ("läckare göteborg",
     ['lärare göteborg', 'läkare göteborg', 'lättare göteborg', 'packare göteborg', 'plockare göteborg']),
    ("stockholm läckare göteborg",
     ['stockholm lärare göteborg', 'stockholm läkare göteborg', 'stockholm lättare göteborg',
      'stockholm packare göteborg', 'stockholm plockare göteborg']
     ),
    ("stockhlm läckare göteborg",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),
    ("stockhlm läckare göteborg ",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # trailing space
    ("stockhlm läckare göteborg  ",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),  # 2 trailing spaces
    ("   stockhlm läckare göteborg   ",
     ['stockholms lärare göteborg', 'stockholm lärare göteborg', 'stockholms läkare göteborg',
      'stockholm läkare göteborg', 'stockholms lättare göteborg', 'stockholm lättare göteborg',
      'stockholms packare göteborg', 'stockholms plockare göteborg', 'stockholm packare göteborg',
      'stockholm plockare göteborg']
     ),
    # leading + trailing spaces
])
def test_spelling_correction_with_phrase_suggest(query, expected):
    """
    Test typeahead with several (misspelled) words
    """
    json_response = get_complete({'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("python ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']

     ),
    ("python  ",
     ['python stockholms län', 'python design', 'python stockholm', 'python systemutvecklare', 'python västra götaland',
      'python västra götalands län', 'python data', 'python göteborg', 'python amazon web services', 'python aws']

     ),  # 2 trailing spaces
    ("läkare ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),
    ("   läkare   ",
     ['läkare specialistläkare', 'läkare st-läkare', 'läkare stockholms län', 'läkare sjukvård', 'läkare allmänmedicin',
      'läkare skåne', 'läkare skåne län', 'läkare sjuksköterska', 'läkare språkkunskaper', 'läkare stockholm']

     ),  # leading + trailing spaces
])
# TODO: A real test case is missing here, that really deletes something
# TODO: When ad 24916354 is in the index 'djurtekniker ' is a real case
def test_remove_same_word_function(query, expected):
    """
    Test when input is word with space, the result will not show the same word
    """
    json_response = get_complete(params={'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("query, expected", [
    ("java nystartsjob", ['java nystartsjobb']),
    ("java nystar", ['java nystartsjobb']),
    ("java rekryterings",
     ['java rekryteringsutbildning', 'java rekrytering', 'java rekryteringskompetens', 'java rekryteringstjänster',
      'java rekryteringssystem', 'java rekryteringsverktyg', 'java rekryteringsföretag', 'java rekryteringsbranschen',
      'java rekryteringsaktiviteter', 'java rekryteringsarbeten']),  # Both an enriched term and a label.
    ("java räksm", ['java räksmörgås']),
])
def test_label_complete(query, expected):
    """
    Test that complete for ad labels works.
    """
    json_response = get_complete(params={'q': query})
    check_complete_results(query, actual=get_typeahead_values(json_response), expected=expected)


@pytest.mark.parametrize("qfield, expected", [
    ('occupation', ['sjuksköterska', 'lärare', 'försäljare', 'säljare', 'lärare i grundskolan', 'personlig assistent',
                    'systemutvecklare', 'sjuksköterskor', 'grundlärare', 'ämneslärare']),
    ('skill',
     ['svenska', 'körkort', 'engelska', 'b-körkort', 'arbetslivserfarenhet', 'försäljning', 'sjukvård', 'datorvana',
      'barn', 'dokumentation']),
    ('location',
     ['sverige', 'stockholms län', 'stockholm', 'västra götaland', 'västra götalands län', 'skåne', 'skåne län',
      'göteborg', 'östergötland', 'östergötlands län']),
    ('employer', ['viva bemanning', 'randstad', 'region skåne', 'västra götalandsregionen', 'annsam', 'academic work',
                  'centric care', 'studentconsulting', 'humana', 'almia']),

])
def test_q_fields(qfield, expected):
    """
     ['occupation', 'skill', 'location', 'employer']
    """
    json_response = get_complete(params={'qfields': qfield})
    check_complete_results(qfield, actual=get_typeahead_values(json_response), expected=expected)
