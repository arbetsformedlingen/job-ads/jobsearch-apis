occupation_queries = [
    (['occ1'], {'bool': {'should': [{'term': {'occupation.concept_id.keyword': {'value': 'occ1', 'boost': 2.0}}},
                                    {'term': {
                                        'occupation.legacy_ams_taxonomy_id': {'value': 'occ1', 'boost': 2.0}}}]}}),
    (['occ1', 'occ2'], {'bool': {
        'should': [{'term': {'occupation.concept_id.keyword': {'value': 'occ1', 'boost': 2.0}}},
                   {'term': {'occupation.concept_id.keyword': {'value': 'occ2', 'boost': 2.0}}},
                   {'term': {'occupation.legacy_ams_taxonomy_id': {'value': 'occ1', 'boost': 2.0}}},
                   {'term': {'occupation.legacy_ams_taxonomy_id': {'value': 'occ2', 'boost': 2.0}}}]}}),
    (['occ1', 'occ2', None], {'bool': {
        'should': [{'term': {'occupation.concept_id.keyword': {'value': 'occ1', 'boost': 2.0}}},
                   {'term': {'occupation.concept_id.keyword': {'value': 'occ2', 'boost': 2.0}}},
                   {'term': {'occupation.legacy_ams_taxonomy_id': {'value': 'occ1', 'boost': 2.0}}},
                   {'term': {'occupation.legacy_ams_taxonomy_id': {'value': 'occ2', 'boost': 2.0}}}]}}),
    (None, None),
    # must_not
    (['-startswithminus'],
     {'bool': {'must_not': [{'term': {'occupation.concept_id.keyword': {'value': 'startswithminus'}}},
                            {'term': {'occupation.legacy_ams_taxonomy_id': {'value': 'startswithminus'}}}]}})

]

occupation_group_queries = [
    (["group1", "group2"], {'bool': {'should': [
        {'term': {'occupation_group.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'group1'}}},
        {'term': {'occupation_group.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'group2'}}},
        {'term': {'occupation_group.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                              'value': 'group1'}}},
        {'term': {'occupation_group.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                              'value': 'group2'}}}]}}),
    (["group1", "group2", None], {'bool': {'should': [
        {'term': {'occupation_group.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'group1'}}},
        {'term': {'occupation_group.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'group2'}}},
        {'term': {
            'occupation_group.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                        'value': 'group1'}}},
        {'term': {
            'occupation_group.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                        'value': 'group2'}}}]}}),
    ([None], None),
    (['-group_starts_with_minus'],
     {'bool': {'must_not': [
         {'term': {'occupation_group.concept_id.keyword': {'value': 'group_starts_with_minus'}}},
         {'term': {
             'occupation_group.legacy_ams_taxonomy_id': {'value': 'group_starts_with_minus'}}}]}}),
]

occupation_field_queries = [

    (['field1'], {'bool': {'should': [
        {'term': {'occupation_field.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'field1'}}},
        {'term': {'occupation_field.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                              'value': 'field1'}}}]}}),
    (['field1', 'field2'], {'bool': {'should': [
        {'term': {'occupation_field.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'field1'}}},
        {'term': {'occupation_field.concept_id.keyword': {'boost': 1.0,
                                                          'value': 'field2'}}},
        {'term': {'occupation_field.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                              'value': 'field1'}}},
        {'term': {'occupation_field.legacy_ams_taxonomy_id': {'boost': 1.0,
                                                              'value': 'field2'}}}]}}),
    ([None], None),
    (['-starts_with_minus'], {'bool': {'must_not': [
        {'term': {'occupation_field.concept_id.keyword': {'value': 'starts_with_minus'}}},
        {'term': {'occupation_field.legacy_ams_taxonomy_id': {'value': 'starts_with_minus'}}}]}}
     )
]

occupation_collections_queries = [

    (['field1'], {'bool': {'should': [
        {'term': {'collections.concept_id.keyword': {'boost': 2.0,
                                                          'value': 'field1'}}},
        {'term': {'collections.legacy_ams_taxonomy_id': {'boost': 2.0,
                                                              'value': 'field1'}}}]}}),
    (['field1', 'field2'], {'bool': {'should': [
        {'term': {'collections.concept_id.keyword': {'boost': 2.0,
                                                          'value': 'field1'}}},
        {'term': {'collections.concept_id.keyword': {'boost': 2.0,
                                                          'value': 'field2'}}},
        {'term': {'collections.legacy_ams_taxonomy_id': {'boost': 2.0,
                                                              'value': 'field1'}}},
        {'term': {'collections.legacy_ams_taxonomy_id': {'boost': 2.0,
                                                              'value': 'field2'}}}]}}),
    ([None], None),
    (['-starts_with_minus'], {'bool': {'must_not': [
        {'term': {'collections.concept_id.keyword': {'value': 'starts_with_minus'}}}, 
        {'term': {'collections.legacy_ams_taxonomy_id': {'value': 'starts_with_minus'}}}]}}
     )
]

combined_queries = [
    (['occ1'], ['group1'], ['field1'],
     {'bool': {'should': [{'term': {'occupation.concept_id.keyword': {'boost': 2.0,
                                                                      'value': 'occ1'}}},
                          {'term': {
                              'occupation.legacy_ams_taxonomy_id': {'boost': 2.0,
                                                                    'value': 'occ1'}}},
                          {'term': {
                              'occupation_group.concept_id.keyword': {'boost': 1.0,
                                                                      'value': 'group1'}}},
                          {'term': {'occupation_group.legacy_ams_taxonomy_id': {
                              'boost': 1.0,
                              'value': 'group1'}}},
                          {'term': {
                              'occupation_field.concept_id.keyword': {'boost': 1.0,
                                                                      'value': 'field1'}}},
                          {'term': {'occupation_field.legacy_ams_taxonomy_id': {
                              'boost': 1.0,
                              'value': 'field1'}}}]}}
     ),
    ([None], [None], [None], None),
    ([False], [False], [False], None),
    ([0], [0], [0], None),
    ([set()], [[]], [{}], None),
    ([''], [], [None], None),

]
