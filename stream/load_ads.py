import datetime
import logging
import json
import time
from datetime import date, timedelta

from elasticsearch.helpers import scan

from common import constants, settings
from common.main_elastic_client import elastic_client
from common.helpers import calculate_utc_offset
from common.result_model import job_ad
from common.ad_formatter import format_one_ad
from stream.api import ns_stream
from stream.atom_feed import AtomFeed

log = logging.getLogger(__name__)

offset = calculate_utc_offset()


def _stream_base_query():
    return {
        "query": {
            "bool": {
                "filter": [
                    {
                        "range": {
                            "publication_date": {
                                "lte": "now/m+%dH/m" % offset
                            }
                        }
                    },
                    {
                        "range": {
                            "last_publication_date": {
                                "gte": "now/m+%dH/m" % offset
                            }
                        }
                    }
                ]
            }
        },
    }


def _index_exists(idx_name):
    return elastic_client().indices.exists_alias(name=[idx_name]) or elastic_client().indices.exists(index=[idx_name])


def build_dsl_for_stream(args):
    """
    Builds a query with the parameters that can be used for JobStream
    """
    since = args.get(constants.DATE)
    # time span, default is None
    if args.get(constants.UPDATED_BEFORE_DATE, None):
        before = args.get(constants.UPDATED_BEFORE_DATE)
    else:
        before = datetime.datetime.strptime(constants.MAX_DATE, '%Y-%m-%dT%H:%M:%S')

    # input is not allowed by type=inputs.datetime_from_iso8601
    if since == 'yesterday':
        since = (date.today() - timedelta(1)).strftime('%Y-%m-%d')

    # Convert timestamp to epoch time which is used in the 'timestamp' field of the ads
    # all "after" and "before" searches are based on the 'timestamp' field which also is when the ad was last updated
    ts_from = int(time.mktime(since.timetuple())) * 1000
    # add 1 sec to find ad (ms truncation problem)
    ts_to = int(time.mktime(before.timetuple()) + 1) * 1000

    index = _get_index()
    log.info(f"(load_all)Elastic stream index is set to: {index}")

    dsl = _stream_base_query()
    dsl['query']['bool']['must'] = [{
        "range": {
            "timestamp": {
                "gte": ts_from,
                "lte": ts_to
            }
        }
    }]

    occupation_concept_ids = args.get(constants.OCCUPATION_CONCEPT_ID)
    if occupation_concept_ids:
        occupation_list = [occupation + '.' + 'concept_id.keyword' for occupation in constants.OCCUPATION_LIST]
        add_filter_query(dsl, occupation_list, occupation_concept_ids)

    location_concept_ids = args.get(constants.LOCATION_CONCEPT_ID)
    if location_concept_ids:
        location_list = ['workplace_address.' + location + '_concept_id' for location in constants.LOCATION_LIST]
        add_filter_query(dsl, location_list, location_concept_ids)
    return dsl


def load_ads_for_stream(args, atom=False):
    dsl = build_dsl_for_stream(args)
    if atom:
        return _load_ads_atom_generator(dsl)
    
    return _load_ads_generator(dsl)


def _get_index():
    index = settings.ES_INDEX
    log.info(f"(load_all)Elastic stream index is set to: {index}")
    return index


def _load_ads_atom_generator(dsl):
    log.debug(f"(load_ads_generator) Query: {json.dumps(dsl)}")
    scan_result = scan(elastic_client(), dsl, index=_get_index())
    feed = AtomFeed()
    return feed.generate(scan_result)


def _load_ads_generator(dsl, snapshot=False):
    """
    Used for both stream and snapshot
    Creates a list with formatted ads from the scan result;  [ad1, ad2, ad3]
    """
    log.debug(f"(load_ads_generator) Query: {json.dumps(dsl)}")
    scan_result = scan(elastic_client(), dsl, index=_get_index())
    counter = 0
    yield '['
    for ad in scan_result:
        if counter > 0:
            yield ','
        source = ad['_source']
        if snapshot:
            # The query to get ads to use in snapshot excludes removed ads.
            # Checking a boolean parameter is faster than checking the value from the ad.
            yield json.dumps(format_ad(source))
        else:  # stream must check if ad is removed or not
            if source.get('removed', False):
                yield json.dumps(format_removed_ad(source))
            else:
                yield json.dumps(format_ad(source))
        counter += 1
    log.debug(f"(load_ads_generator) Delivered: {counter} ads as stream")
    yield ']'


def add_filter_query(dsl, items, concept_ids):
    # add occupation or location filter query
    should_query = []
    for concept_id in concept_ids:
        if concept_id:
            for item in items:
                should_query.append({"term": {
                    item: concept_id
                }})
    dsl['query']['bool']['filter'].append({"bool": {"should": should_query}})
    return dsl


@ns_stream.marshal_with(job_ad)
def format_ad(ad_data):
    return format_one_ad(ad_data)


# @marshaller.marshal_with(removed_job_ad)
def format_removed_ad(ad_data):
    return {
        'id': str(ad_data.get('id')),
        'removed': ad_data.get('removed'),
        'removed_date': ad_data.get('removed_date'),
        'occupation': ad_data.get('occupation', None),
        'occupation_group': ad_data.get('occupation_group', None),
        'occupation_field': ad_data.get('occupation_field', None),
        'municipality': ad_data.get('workplace_address', {}).get('municipality_concept_id', None),
        'region': ad_data.get('workplace_address', {}).get('region_concept_id', None),
        'country': ad_data.get('workplace_address', {}).get('country_concept_id', None)
    }


def load_snapshot():
    dsl = _stream_base_query()
    dsl['query']['bool']['filter'].append({"term": {"removed": False}})
    return _load_ads_generator(dsl, snapshot=True)
