import pytest
from requests.exceptions import HTTPError
from tests.test_resources.helper import get_logo

pytestmark = [pytest.mark.jobsearch]


def test_logo():
    """
    Try to get a logo from an ad that is in the test data set
    the logo png file comes from an external system
    """
    try:
        logo_response = get_logo(ad_id='24681267')
    except HTTPError as e:
        # The logo service often returns http 503
        assert e.response.status_code == 503
        assert e.response.reason == 'SERVICE UNAVAILABLE'
        print("Ignoring http 503 Service unavailable")
    else:
        assert "PNG" in logo_response.text
        assert logo_response.headers['Content-Type'] == 'image/png'
        assert 'logo.png' in str(logo_response.headers)


def test_logo_ad_not_found():
    response = get_logo(ad_id=112233, verify_response_ok=False)
    assert response.status_code == 404
    assert '"message": "Ad not found. You have requested this URI [/ad/112233/logo] but did you mean /ad/<id>/logo ?"' in response.text
