import pytest
import requests
from search.common_search.logo import fetch_ad_logo, get_correct_logo_url, get_logo_url_if_available
from unittest.mock import Mock, patch
from werkzeug.exceptions import ServiceUnavailable

@patch('search.common_search.logo.fetch_ad_by_id')
def test_get_correct_logo_url_for_invalid_ad_id(mock_fetch_ad_by_id):
    """
    Test that get_correct_logo_url function returns correct logo url with a valid ad_id
    """
    mock_fetch_ad_by_id.return_value = {}
    ad_id = '24373453'

    result = get_correct_logo_url(ad_id)

    mock_fetch_ad_by_id.assert_called_with(ad_id)
    assert result is None


@patch('search.common_search.logo.get_logo_url_if_available')
@patch('search.common_search.logo.fetch_ad_by_id')
def test_get_correct_logo_url_for_workplace_id(mock_fetch_ad_by_id, mock_get_logo_url_if_available):
    """
    Test that get_correct_logo_url function returns correct logo url with a valid ad_id
    """
    mock_fetch_ad_by_id.return_value = {'employer': {'workplace_id': 123456}}
    mock_get_logo_url_if_available.return_value = "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png"
    ad_id = '24373453'

    result = get_correct_logo_url(ad_id)

    mock_fetch_ad_by_id.assert_called_with(ad_id)
    mock_get_logo_url_if_available.assert_called_with("https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png")
    assert result == "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/arbetsplatser/123456/logotyper/logo.png"


@patch('search.common_search.logo.get_logo_url_if_available')
@patch('search.common_search.logo.fetch_ad_by_id')
def test_get_correct_logo_url_for_organization_number(mock_fetch_ad_by_id, mock_get_logo_url_if_available):
    """
    Test that get_correct_logo_url function returns correct logo url with a valid ad_id
    """
    mock_fetch_ad_by_id.return_value = {'employer': {'organization_number': 123456}}
    mock_get_logo_url_if_available.return_value = "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/123456/logotyper/logo.png"
    ad_id = '24373453'

    result = get_correct_logo_url(ad_id)

    mock_fetch_ad_by_id.assert_called_with(ad_id)
    mock_get_logo_url_if_available.assert_called_with("https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/123456/logotyper/logo.png")
    assert result == "https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/organisation/123456/logotyper/logo.png"


def test_get_logo_url_if_available_for_valid_url(monkeypatch):
    class MockResponse:
        status_code = 200

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    result = get_logo_url_if_available("https://fakeurl")

    assert result == "https://fakeurl"


def test_get_logo_url_if_available_for_invalid_url(monkeypatch):
    class MockResponse:
        status_code = 400

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    result = get_logo_url_if_available("https://fakeurl")
    assert result is None


def test_get_logo_url_if_available_when_throwing_exception(monkeypatch):
    class MockResponse:
        def __init__(self, *args, **kwargs):
            raise requests.exceptions.ConnectionError

    def mock_head(*args, **kwargs):
        return MockResponse()

    # apply the monkeypatch for requests.head to mock_head
    monkeypatch.setattr(requests, "head", mock_head)

    with pytest.raises(ServiceUnavailable) as excinfo:
        get_logo_url_if_available("https://fakeurl")
    assert "Service Unavailable" in str(excinfo.value)


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', True)
@patch('search.common_search.logo.file_formatter')
@patch('search.common_search.logo.get_local_file')
def test_fetch_ad_logo_from_url_when_company_logo_fetch_disabled(mock_get_local_file, mock_file_formatter):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is True in settings.
    """
    mock_get_local_file.return_value = "logo bytes"
    mock_file_formatter.return_value = "some string"

    result = fetch_ad_logo("24373453")

    mock_get_local_file.assert_called_with("../resources/1x1-00000000.png")
    mock_file_formatter.assert_called_with("logo bytes")
    assert result == "some string"


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', False)
@patch('search.common_search.logo.get_correct_logo_url')
@patch('search.common_search.logo.file_formatter')
@patch('requests.get')
def test_fetch_ad_logo_from_url_when_company_logo_fetch_enabled(mock_requests_get, mock_file_formatter, mock_get_correct_logo_url):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is False in settings. This is the happy path
    that will return a valid URL for logo without calling the exception.
    """
    mock_file_formatter.return_value = "some string"
    mock_get_correct_logo_url.return_value = "https://fakeurl"
    mock_response = Mock()
    mock_response.status_code = 200
    mock_response.raw.read.return_value = "logo bytes"
    mock_requests_get.return_value = mock_response

    result = fetch_ad_logo("24373453")

    mock_get_correct_logo_url.assert_called_with("24373453")
    mock_requests_get.assert_called_with("https://fakeurl", stream=True, timeout=5)
    mock_file_formatter.assert_called_with("logo bytes")
    assert result == "some string"
    """
    Check that the raise_for_status did not get called
    """
    assert not mock_requests_get.raise_for_status.called


@patch('search.common_search.logo.settings.COMPANY_LOGO_FETCH_DISABLED', False)
@patch('search.common_search.logo.get_correct_logo_url')
@patch('search.common_search.logo.file_formatter')
@patch('requests.get')
def test_fetch_ad_logo_from_url_for_missing_logo(mock_requests_get, mock_file_formatter, mock_get_correct_logo_url):
    """
    Test that fetch_ad_logo function returns correct logo url with a valid ad_id
    when COMPANY_LOGO_FETCH_DISABLED is False in settings. This is the unhappy path
    that will throw raise_for_status() exception.
    """
    def raise_error(ex):
        raise ex
    mock_file_formatter.return_value = "some string"
    mock_get_correct_logo_url.return_value = "https://fakeurl"
    mock_response = Mock()
    mock_response.status_code = 404
    mock_response.raise_for_status = lambda: raise_error(requests.exceptions.HTTPError)
    mock_requests_get.return_value = mock_response

    with pytest.raises(requests.exceptions.HTTPError):
        fetch_ad_logo("24373453")

    mock_get_correct_logo_url.assert_called_with("24373453")
    mock_requests_get.assert_called_with("https://fakeurl", stream=True, timeout=5)
