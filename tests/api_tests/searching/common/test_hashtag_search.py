import pytest

from tests.test_resources.api_test_runner import run_test_case


@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    {'params': {'q': '#corona'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'corona'}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': "#jobbjustnu'"}, 'jobsearch': 308, 'historical': 155},
    {'params': {'q': "jobbjustnu'"}, 'jobsearch': 5, 'historical': 9},
    {'params': {'q': "#metoo'"}, 'jobsearch': 1, 'historical': 2},
    {'params': {'q': "metoo'"}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': "#wedo'"}, 'jobsearch': 1, 'historical': 3},
    {'params': {'q': "wedo'"}, 'jobsearch': 0, 'historical': 1}
])
def test_hashtag_search(test_case):
    run_test_case(test_case)
