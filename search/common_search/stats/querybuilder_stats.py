from common import constants, fields as f


def create_historical_stats_aggs_query(stat, stats_by, limit):
    if stats_by == constants.LEGACY_ID:
        return {
            "terms": {
                "field": f.stats_v2_options[stat],
                "size": limit
            }
        }
    elif stats_by == constants.CONCEPT_ID:
        return {
            "terms": {
                "field": f.stats_v2_concept_id_options[stat],
                "size": limit
            }
        }
    elif stats_by == constants.LABEL:
        return {
            "terms": {
                "field": f.stats_v2_concept_name_options[stat],
                "size": limit
            }
        }
