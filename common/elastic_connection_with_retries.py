import os
import sys
import time
import logging
import platform
import certifi
from ssl import create_default_context
from elasticsearch import Elasticsearch, ElasticsearchException
from elasticsearch.helpers import scan
from common import settings
import requests

log = logging.getLogger(__name__)


def elastic_search_with_retry(client, query, index, hard_failure=False, es_request_timeout=settings.ES_DEFAULT_TIMEOUT):
    fail_count = 0
    max_retries = settings.ES_RETRIES
    for i in range(max_retries):
        try:
            return client.search(body=query, index=index, request_timeout=es_request_timeout)
        except (ElasticsearchException, Exception) as e:
            fail_count += 1
            time.sleep(settings.ES_RETRIES_SLEEP)
            log.warning(f"Failed to search Elastic, failed attempt {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to search Elastic after: {fail_count} attempts.")
                if hard_failure:
                    log.error(f"Failed to search Elastic after: {fail_count} attempts. Exit!.")
                    hard_kill()
                else:
                    return None


def elastic_scan_with_retry(client, query, index, size, source):
    fail_count = 0
    max_retries = settings.ES_RETRIES
    for i in range(max_retries):
        try:
            return scan(client, query, index=index, size=size, _source=source)
        except (ElasticsearchException, Exception) as e:
            fail_count += 1
            time.sleep(settings.ES_RETRIES_SLEEP)
            log.warning(f"Failed to search Elastic, failed attempt {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to search Elastic after: {fail_count} attempts. Exit!")
                hard_kill()


def create_elastic_client_with_retry(host, port=settings.ES_PORT, user=settings.ES_USER, password=settings.ES_PWD,
                                     es_timeout=settings.ES_DEFAULT_TIMEOUT):
    start_time = int(time.time() * 1000)
    log.info(f"Creating Elastic client, host: {host}, port: {port} user: {user} ")
    fail_count = 0
    max_retries = settings.ES_RETRIES

    for i in range(max_retries):
        try:
            if user and password:
                use_ssl = False
                scheme = None
                ssl_context = None
                if settings.ES_USE_SSL == 'TRUE':
                    use_ssl = True
                    scheme='https'
                    ssl_context=create_default_context(cafile=certifi.where())
                elastic = Elasticsearch([host],
                                        port=port,
                                        use_ssl=use_ssl,
                                        scheme=scheme,
                                        ssl_context=ssl_context,
                                        http_auth=(user, password),
                                        timeout=es_timeout)
            else:
                elastic = Elasticsearch([{'host': host, 'port': port}])
            # check connection
            info = elastic.info()
            log.info(f"Successfully connected to Elasticsearch node at {settings.ES_HOST}:{settings.ES_PORT}")
            log.debug(f"Elastic info: {info}")
            log.debug(
                f"Elasticsearch node created and connected after: {int(time.time() * 1000 - start_time)} milliseconds")
            return elastic

        except (ElasticsearchException, Exception, requests.exceptions.BaseHTTPError) as e:
            fail_count += 1
            time.sleep(settings.ES_RETRIES_SLEEP)
            log.warning(f"Failed to connect to Elastic, failed attempt: {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                log.error(f"Failed to connect to Elastic after attempts: {fail_count}. Exit!")
                hard_kill()


def hard_kill():
    """
    to shut down uwsgi and exit
    for this to work, it needs to be configured not to restart:
    - die-on-term = true in uwsgi.ini
    - autorestart=false in supervisord.conf
    """
    log.error("(hard_kill)Shutting down...")
    if platform.system() != 'Windows':
        os.system('killall -15 uwsgi')  # to send SIGTERM
    sys.exit(1)
