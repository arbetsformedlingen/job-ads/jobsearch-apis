occupation_name_stats = {
    "aggregations": {
        "occupation-name": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 43716,
            "buckets": [
                {
                    "key": "7296",
                    "doc_count": 3727,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 3727,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24438158",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation": [
                                            {
                                                "concept_id": "bXNH_MNX_dUR",
                                                "label": "Sjuksköterska, grundutbildad"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "5798",
                    "doc_count": 2003,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2003,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24457873",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation": [
                                            {
                                                "concept_id": "eU1q_zvL_9Rf",
                                                "label": "Personlig assistent"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "2419",
                    "doc_count": 1035,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 1035,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24452579",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation": [
                                            {
                                                "concept_id": "fg7B_yov_smw",
                                                "label": "Systemutvecklare/Programmerare"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "7117",
                    "doc_count": 875,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 875,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24484925",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation": [
                                            {
                                                "concept_id": "p17k_znk_osi",
                                                "label": "Utesäljare"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "2291",
                    "doc_count": 739,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 739,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24487903",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "occupation": [
                                            {
                                                "concept_id": "8Uhp_XYo_z5f",
                                                "label": "Kundtjänstmedarbetare"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

occupation_name_stats_result = [
    {
        "type": "occupation-name",
        "values": [
            {
                "term": "Sjuksköterska, grundutbildad",
                "concept_id": "bXNH_MNX_dUR",
                "code": "7296",
                "count": 3727
            },
            {
                "term": "Personlig assistent",
                "concept_id": "eU1q_zvL_9Rf",
                "code": "5798",
                "count": 2003
            },
            {
                "term": "Systemutvecklare/Programmerare",
                "concept_id": "fg7B_yov_smw",
                "code": "2419",
                "count": 1035
            },
            {
                "term": "Utesäljare",
                "concept_id": "p17k_znk_osi",
                "code": "7117",
                "count": 875
            },
            {
                "term": "Kundtjänstmedarbetare",
                "concept_id": "8Uhp_XYo_z5f",
                "code": "2291",
                "count": 739
            }
        ]
    }
]

occupation_group_stats = {
    "aggregations": {
        "occupation-group": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 39322,
            "buckets": [
                {
                    "key": "2221",
                    "doc_count": 3817,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 3817,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24438158",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_group": [
                                            {
                                                "concept_id": "Z8ci_bBE_tmx",
                                                "label": "Grundutbildade sjuksköterskor"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "2512",
                    "doc_count": 2911,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2911,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24451520",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_group": [
                                            {
                                                "concept_id": "DJh5_yyF_hEM",
                                                "label": "Mjukvaru- och systemutvecklare m.fl."
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "3322",
                    "doc_count": 2110,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2110,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24472114",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_group": [
                                            {
                                                "concept_id": "oXSW_fbY_XrY",
                                                "label": "Företagssäljare"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "5343",
                    "doc_count": 2069,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2069,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24445601",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_group": [
                                            {
                                                "concept_id": "sq3e_WVv_Fjd",
                                                "label": "Personliga assistenter"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "2341",
                    "doc_count": 1685,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 1685,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24541089",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_group": [
                                            {
                                                "concept_id": "oQUQ_D11_HPx",
                                                "label": "Grundskollärare"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

occupation_group_stats_result = [
    {
        "type": "occupation-group",
        "values": [
            {
                "term": "Grundutbildade sjuksköterskor",
                "concept_id": "Z8ci_bBE_tmx",
                "code": "2221",
                "count": 3817
            },
            {
                "term": "Mjukvaru- och systemutvecklare m.fl.",
                "concept_id": "DJh5_yyF_hEM",
                "code": "2512",
                "count": 2911
            },
            {
                "term": "Företagssäljare",
                "concept_id": "oXSW_fbY_XrY",
                "code": "3322",
                "count": 2110
            },
            {
                "term": "Personliga assistenter",
                "concept_id": "sq3e_WVv_Fjd",
                "code": "5343",
                "count": 2069
            },
            {
                "term": "Grundskollärare",
                "concept_id": "oQUQ_D11_HPx",
                "code": "2341",
                "count": 1685
            }
        ]
    }
]

occupation_field_stats = {
    "aggregations": {
        "occupation-field": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 21941,
            "buckets": [
                {
                    "key": "8",
                    "doc_count": 8385,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 8385,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24435456",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_field": [
                                            {
                                                "concept_id": "NYW6_mP6_vwf",
                                                "label": "Hälso- och sjukvård"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "5",
                    "doc_count": 6956,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 6956,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24456631",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_field": [
                                            {
                                                "concept_id": "RPTn_bxG_ExZ",
                                                "label": "Försäljning, inköp, marknadsföring"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "15",
                    "doc_count": 5385,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 5385,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24460175",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_field": [
                                            {
                                                "concept_id": "MVqp_eS8_kDZ",
                                                "label": "Pedagogik"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "3",
                    "doc_count": 5103,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 5103,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24451520",
                                    "_score": 0.0,
                                    "_source": {
                                        "occupation_field": [
                                            {
                                                "concept_id": "apaJ_2ja_LuF",
                                                "label": "Data/IT"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "1",
                    "doc_count": 4218,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 4218,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24454477",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "occupation_field": [
                                            {
                                                "concept_id": "X82t_awd_Qyc",
                                                "label": "Administration, ekonomi, juridik"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

occupation_field_stats_result = [
        {
            "type": "occupation-field",
            "values": [
                {
                    "term": "Hälso- och sjukvård",
                    "concept_id": "NYW6_mP6_vwf",
                    "code": "8",
                    "count": 8385
                },
                {
                    "term": "Försäljning, inköp, marknadsföring",
                    "concept_id": "RPTn_bxG_ExZ",
                    "code": "5",
                    "count": 6956
                },
                {
                    "term": "Pedagogik",
                    "concept_id": "MVqp_eS8_kDZ",
                    "code": "15",
                    "count": 5385
                },
                {
                    "term": "Data/IT",
                    "concept_id": "apaJ_2ja_LuF",
                    "code": "3",
                    "count": 5103
                },
                {
                    "term": "Administration, ekonomi, juridik",
                    "concept_id": "X82t_awd_Qyc",
                    "code": "1",
                    "count": 4218
                }
            ]
        }
    ]

country_stats = {
    "aggregations": {
        "country": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 70,
            "buckets": [
                {
                    "key": "199",
                    "doc_count": 51476,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 51476,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24435456",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "country": "Sverige",
                                            "country_concept_id": "i46j_HmG_v64"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "155",
                    "doc_count": 229,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 229,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24459348",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "workplace_address": {
                                            "country": "Norge",
                                            "country_concept_id": "QJgN_Zge_BzJ"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "193",
                    "doc_count": 148,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 148,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24600119",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "workplace_address": {
                                            "country": "Spanien",
                                            "country_concept_id": "bN7k_4ka_YGQ"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "44",
                    "doc_count": 60,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 60,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24697458",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "workplace_address": {
                                            "country": "Danmark",
                                            "country_concept_id": "Co3h_1xq_Cwb"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "130",
                    "doc_count": 14,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 14,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24726103",
                                    "_score": 0.0,
                                    "_ignored": [
                                        "workplace_address.coordinates"
                                    ],
                                    "_source": {
                                        "workplace_address": {
                                            "country": "Malta",
                                            "country_concept_id": "u8qF_qpq_R5W"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

country_stats_result = [
    {
        "type": "country",
        "values": [
            {
                "term": "Sverige",
                "concept_id": "i46j_HmG_v64",
                "code": "199",
                "count": 51476
            },
            {
                "term": "Norge",
                "concept_id": "QJgN_Zge_BzJ",
                "code": "155",
                "count": 229
            },
            {
                "term": "Spanien",
                "concept_id": "bN7k_4ka_YGQ",
                "code": "193",
                "count": 148
            },
            {
                "term": "Danmark",
                "concept_id": "Co3h_1xq_Cwb",
                "code": "44",
                "count": 60
            },
            {
                "term": "Malta",
                "concept_id": "u8qF_qpq_R5W",
                "code": "130",
                "count": 14
            }
        ]
    }
]

municipality_stats = {
    "aggregations": {
        "municipality": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 31435,
            "buckets": [
                {
                    "key": "0180",
                    "doc_count": 9797,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 9797,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24451520",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "municipality": "Stockholm",
                                            "municipality_concept_id": "AvNB_uwa_6n6"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "1480",
                    "doc_count": 4569,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 4569,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24457238",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "municipality": "Göteborg",
                                            "municipality_concept_id": "PVZL_BQT_XtL"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "1280",
                    "doc_count": 2322,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2322,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24452579",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "municipality": "Malmö",
                                            "municipality_concept_id": "oYPt_yRA_Smm"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "0380",
                    "doc_count": 1257,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 1257,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24473502",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "municipality": "Uppsala",
                                            "municipality_concept_id": "otaF_bQY_4ZD"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "0580",
                    "doc_count": 1067,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 1067,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24481278",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "municipality": "Linköping",
                                            "municipality_concept_id": "bm2x_1mr_Qhx"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

municipality_stats_result = [
    {
        "type": "municipality",
        "values": [
            {
                "term": "Stockholm",
                "concept_id": "AvNB_uwa_6n6",
                "code": "0180",
                "count": 9797
            },
            {
                "term": "Göteborg",
                "concept_id": "PVZL_BQT_XtL",
                "code": "1480",
                "count": 4569
            },
            {
                "term": "Malmö",
                "concept_id": "oYPt_yRA_Smm",
                "code": "1280",
                "count": 2322
            },
            {
                "term": "Uppsala",
                "concept_id": "otaF_bQY_4ZD",
                "code": "0380",
                "count": 1257
            },
            {
                "term": "Linköping",
                "concept_id": "bm2x_1mr_Qhx",
                "code": "0580",
                "count": 1067
            }
        ]
    }
]

region_stats = {
    "aggregations": {
        "region": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 16544,
            "buckets": [
                {
                    "key": "01",
                    "doc_count": 14775,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 14775,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24451520",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "region_concept_id": "CifL_Rzy_Mku",
                                            "region": "Stockholms län"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "14",
                    "doc_count": 8557,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 8557,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24435483",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "region_concept_id": "zdoY_6u5_Krt",
                                            "region": "Västra Götalands län"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "12",
                    "doc_count": 6538,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 6538,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24452844",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "region_concept_id": "CaRE_1nn_cSU",
                                            "region": "Skåne län"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "05",
                    "doc_count": 2185,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 2185,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24468406",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "region_concept_id": "oLT3_Q9p_3nn",
                                            "region": "Östergötlands län"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                },
                {
                    "key": "06",
                    "doc_count": 1804,
                    "id_and_name": {
                        "hits": {
                            "total": {
                                "value": 1804,
                                "relation": "eq"
                            },
                            "max_score": 0.0,
                            "hits": [
                                {
                                    "_index": "platsannons-20210616-0600",
                                    "_type": "_doc",
                                    "_id": "24464092",
                                    "_score": 0.0,
                                    "_source": {
                                        "workplace_address": {
                                            "region_concept_id": "MtbE_xWT_eMi",
                                            "region": "Jönköpings län"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }
}

region_stats_result = [
    {
        "type": "region",
        "values": [
            {
                "term": "Stockholms län",
                "concept_id": "CifL_Rzy_Mku",
                "code": "01",
                "count": 14775
            },
            {
                "term": "Västra Götalands län",
                "concept_id": "zdoY_6u5_Krt",
                "code": "14",
                "count": 8557
            },
            {
                "term": "Skåne län",
                "concept_id": "CaRE_1nn_cSU",
                "code": "12",
                "count": 6538
            },
            {
                "term": "Östergötlands län",
                "concept_id": "oLT3_Q9p_3nn",
                "code": "05",
                "count": 2185
            },
            {
                "term": "Jönköpings län",
                "concept_id": "MtbE_xWT_eMi",
                "code": "06",
                "count": 1804
            }
        ]
    }
]
