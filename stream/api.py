from datetime import datetime, timedelta
from flask_restx import Api, Namespace, reqparse, inputs

import common.constants
from common import constants
from common.result_model import root_api

stream_api = Api(version=common.constants.API_VERSION,
                 title='JobStream',
                 description='## An API for retrieving all currently published job ads from the Swedish Public Employment Service\n'
                             '**Useful links:**\n'
                             '- [JobStream getting started](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamEN.md)\n'
                             '- [JobStream Code example](https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/jobstream-example)\n'
                             '- [Taxonomy Getting Started](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md)\n'
                             '- [Taxonomy Atlas](https://atlas.jobtechdev.se)\n',
                 default_label="An API for retrieving job ads",
                 contact_url='https://forum.jobtechdev.se/c/vara-api-er-dataset/job-stream/25',
                 contact='Contact: JobTech Forum',
                 license='Ads are licensed under CC0',
                 license_url='https://creativecommons.org/publicdomain/zero/1.0/deed.sv'
                 )

ns_stream = Namespace('JobStream ads', description='Start with a snapshot and keep it up to date with stream')

stream_api.add_namespace(ns_stream, '/')

for name, definition in root_api.models.items():
    ns_stream.add_model(name, definition)

default_time = (datetime.now() - timedelta(minutes=10)).strftime(constants.DATE_FORMAT)

stream_query = reqparse.RequestParser()
stream_query.add_argument(constants.DATE, type=inputs.datetime_from_iso8601, required=True, default=default_time)
stream_query.add_argument(constants.UPDATED_BEFORE_DATE, type=inputs.datetime_from_iso8601, required=False)
stream_query.add_argument(constants.OCCUPATION_CONCEPT_ID, action='append', required=False)
stream_query.add_argument(constants.LOCATION_CONCEPT_ID, action='append', required=False)

# /snapshot endpoint
snapshot_query = reqparse.RequestParser()
