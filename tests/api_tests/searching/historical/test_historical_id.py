import json

import pytest
from tests.test_resources.helper import get_by_id, get_by_id_raw, get_search
from tests.test_resources.historical import random_non_duplicates, random_ids_from_2017, random_duplicates, \
    get_new_ids_from_409_response, _check_ad_from_search_result
from tests.test_resources.api_test_runner import HISTORICAL

pytestmark = pytest.mark.historical

CONFLICT = 409

@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.smoke
@pytest.mark.parametrize("original_id, new_id_list", random_duplicates())
def test_historical_duplicates_original_id_get_by_id( original_id, new_id_list):
    response = get_by_id_raw( original_id)
    assert response.status_code == CONFLICT, f"Expected http status code 409/Conflict but got {response.status_code}"
    conflict_response = json.loads(response.content.decode('utf8'))

    assert conflict_response["message"]["info"] == f'Multiple matches found for id {original_id}'
    assert conflict_response['message']['requested_id'] == original_id
    assert original_id in conflict_response['message']['info']
    new_ids_from_response = get_new_ids_from_409_response(conflict_response)
    assert len(new_ids_from_response) == len(new_id_list)
    assert sorted(new_ids_from_response) == sorted(new_id_list)
    for new_ad_id in new_ids_from_response:
        ad = get_by_id( new_ad_id)
        assert ad["id"] == new_ad_id


@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.parametrize("original_id, new_id_list", random_duplicates())
def test_search_original_id( original_id, new_id_list):
    """
    search q for original id
    Check that an ad is in the response
    check that its id (hashed) is in the list of expected ids
    """
    result_original = get_search(params={"q": original_id, "limit": 100})
    hits = result_original["hits"]
    assert len(hits) == len(new_id_list)
    ad_ids_from_result = []
    for hit in hits:
        ad_ids_from_result.append(hit["id"])
    assert sorted(ad_ids_from_result) == sorted(new_id_list)


@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.parametrize("original_id, new_id_list", random_duplicates())
def test_search_new_id( original_id, new_id_list):
    for new_id in new_id_list:
        result = get_search(params={"q": new_id, "limit": 100})
        checked_ad = _check_ad_from_search_result(result)
        assert checked_ad["id"] == new_id


@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.parametrize("original_id, new_id", random_non_duplicates())
def test_non_duplicates_new_id( original_id, new_id):
    """
    Search for new (hashed) id and verify that it is found
    """
    result = get_search(params={"q": new_id, "limit": 100})
    checked_ad = _check_ad_from_search_result(result)
    assert checked_ad["id"] == new_id


@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.parametrize("original_id, new_id", random_non_duplicates())
def test_non_duplicates_original_id( original_id, new_id):
    """
    1:1 original_id - new_id
    """
    result = get_search(params={"q": original_id, "limit": 100})
    checked_ad = _check_ad_from_search_result(result)
    assert checked_ad["id"] == new_id


@pytest.mark.skipif(HISTORICAL, reason="needs new test data")
@pytest.mark.parametrize("original_id, new_id", random_ids_from_2017())
def test_non_duplicates_original_id_2017( original_id, new_id):
    """
    1:1 original_id - new_id
    ids from 2017 have a different format which makes searching for them hard
    Check that the expected ad is in the search result
    """
    result = get_search(params={"q": original_id, "limit": 100})
    success = False
    for hit in result["hits"]:
        if hit["id"] == new_id:
            success = True
    assert success
