import logging
import time
from flask import request
from common.ad_formatter import format_hits_with_only_original_values
from search.common_search.text_to_concept import TextToConcept
from search.common_search.querybuilder import QueryBuilder, QueryBuilderType
from search.common_search.ad_search import search_for_ads
from search.common_search.text_to_label import TextToLabel

log = logging.getLogger(__name__)


# Endpoint functions common between Jobsearch and Historical ads

class QueryBuilderContainer(object):
    """
    Holds full blown QueryBuilder object with ontology
    Use launch() to create, and get() to get the object
    """

    def __init__(self):
        self.querybuilder = None

    def get(self, qb_type):
        if not isinstance(qb_type, QueryBuilderType):
            return None
        self.querybuilder.set_qb_type(qb_type)
        return self.querybuilder

    def launch(self):
        ttc = TextToConcept()
        ttl = TextToLabel()
        self.querybuilder = QueryBuilder(ttc, ttl)


def search_endpoint(querybuilder, args, start_time):
    log.debug(f"Query parsed after: {int(time.time() * 1000 - start_time)} milliseconds.")
    result = search_for_ads(args,
                            querybuilder,
                            start_time,
                            request.headers.get('X-Fields'))

    log.debug(f"Query results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    max_score = result.get('max_score', 1.0)
    hits = [dict(hit['_source'],
                 **{'relevance': (hit['_score'] / max_score)
                 if max_score > 0 else 0.0})
            for hit in result.get('hits', [])]
    hits = format_hits_with_only_original_values(hits)
    result['hits'] = hits
    return _format_api_response(result, start_time)


def _format_api_response(esresult, start_time):
    total_results = {'value': esresult.get('total', {}).get('value')}
    result_time = int(time.time() * 1000 - start_time)
    result = {
        "total": total_results,
        "positions": esresult.get('positions', 0),
        "query_time_in_millis": esresult.get('took', 0),
        "result_time_in_millis": result_time,
        "stats": esresult.get('stats', []),
        "freetext_concepts": esresult.get('concepts', {}),
        "hits": esresult.get('hits', {})
    }
    log.debug(f"Sending results after: {result_time} milliseconds.")
    return result
