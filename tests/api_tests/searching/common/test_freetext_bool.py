import pytest
import copy

from tests.test_resources.helper import get_search, get_search_with_headers
from tests.test_resources.api_test_runner import HISTORICAL
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA
from common.constants import X_FEATURE_FREETEXT_BOOL_METHOD
from tests.test_resources.test_settings import test_headers


@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("test_case", [
    {"query": "", "jobsearch": 5029, "historical": 17750, "bool_method": "and"},
    {"query": "Sirius crew", "jobsearch": 0, "historical": 0, "bool_method": "and"},
    {"query": "Sirius crew", "jobsearch": 4, "historical": 11, "bool_method": "or"},
    {"query": "Sirius crew", "jobsearch": 4, "historical": 11, "bool_method": None},
    {"query": "TechBuddy uppdrag", "jobsearch": 1, "historical": 1, "bool_method": "and"},
    {"query": "TechBuddy uppdrag", "jobsearch": 1403, "historical": 3810, "bool_method": "or"},
    {"query": "TechBuddy uppdrag", "jobsearch": 1403, "historical": 3810, "bool_method": None}
])
def test_freetext_bool_method(test_case):
    """
    Test with 'or' & 'and' values for X_FEATURE_FREETEXT_BOOL_METHOD header flag
    Default value is 'OR' (used in test cases with None as param)
    Searches with 'or' returns more hits
    """
    params = {'q': test_case["query"], 'limit': 0}
    # use default setting for X_FEATURE_FREETEXT_BOOL_METHOD == 'OR'
    bool_method = test_case["bool_method"]
    if not bool_method:
        response_json = get_search(params)
    else:
        tmp_headers = copy.deepcopy(test_headers)
        tmp_headers[X_FEATURE_FREETEXT_BOOL_METHOD] = bool_method
        response_json = get_search_with_headers(params, tmp_headers)

    if HISTORICAL:
        expected = test_case["historical"]
    else:
        expected = test_case["jobsearch"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected
