import json
import requests

import common.constants
from tests.test_resources.test_settings import TEST_URL


def test_swagger():
    """
    Test Swagger info
    """
    response = requests.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    response_json = json.loads(response.content)
    assert response_json['info']['version'] == common.constants.API_VERSION
    assert response_json['paths']['/stream']
    for path in ['/stream', '/snapshot']:
        assert len(response_json['paths'][path]['get']['responses']) == 2
