import random

import requests
import pytest
from tests.test_resources.helper import get_by_id

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.smoke
def test_get_by_id(random_ads):
    ad_ids = []
    for ad in random_ads:
        ad_ids.append(ad['id'])

    subset_of_ids = random.sample(ad_ids, 5)
    for ad_id in subset_of_ids:
        ad = get_by_id(ad_id)
        assert ad['id'] == ad_id


def test_fetch_not_found_ad_by_id():
    with pytest.raises(requests.exceptions.HTTPError):
        get_by_id(ad_id='823069282306928230692823069282306928230692')
