import logging
from common import settings
from jobtech.common.customlogging import configure_logging
from common.elastic_connection_with_retries import create_elastic_client_with_retry

configure_logging([__name__.split('.')[0]])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')

log.info(f"Using Elasticsearch node at {settings.ES_HOST}:{settings.ES_PORT}")
log.info(f"Using Elasticsearch index {settings.ES_INDEX}")

class ElasticClient(object):
    """
    Main elastic client
    Created the first time a query search is made
    Can also be created beforehand by using launch()
    """

    def __init__(self):
        self.elastic = None

    def launch(self, es_timeout=10):
        if self.elastic is None:
            log.info("Creating main elastic client")
            self.elastic = create_elastic_client_with_retry(settings.ES_HOST, settings.ES_PORT, settings.ES_USER,
                                                            settings.ES_PWD, es_timeout)

    def __call__(self):
        if self.elastic is None:
            self.launch()
        return self.elastic


elastic_client = ElasticClient()
