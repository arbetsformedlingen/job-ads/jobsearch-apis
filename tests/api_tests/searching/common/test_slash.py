import pytest

from tests.test_resources.api_test_runner import run_test_case

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'försäljning/marknad'}, 'jobsearch': 3, 'historical': 35},
    {'params': {'q': 'försäljning marknad'}, 'jobsearch': 12, 'historical': 62},
    pytest.param({'params': {'q': 'försäljning / marknad'}, 'jobsearch': 28, 'historical': 108},
                 marks=pytest.mark.smoke),
    {'params': {'q': 'lager/logistik'}, 'jobsearch': 19, 'historical': 50},
    {'params': {'q': 'lager / logistik'}, 'jobsearch': 7, 'historical': 10},
    {'params': {'q': 'lager logistik'}, 'jobsearch': 138, 'historical': 472},
    {'params': {'q': 'psykolog/beteendevetare'}, 'jobsearch': 20, 'historical': 53},
    {'params': {'q': 'psykolog / beteendevetare'}, 'jobsearch': 0, 'historical': 1},
    {'params': {'q': 'psykolog beteendevetare'}, 'jobsearch': 30, 'historical': 71},
    {'params': {'q': 'Affärsutvecklare/exploateringsingenjör'}, 'jobsearch': 3, 'historical': 8},
    {'params': {'q': 'Affärsutvecklare / exploateringsingenjör'}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': 'Affärsutvecklare exploateringsingenjör'}, 'jobsearch': 11, 'historical': 16},
    {'params': {'q': 'Affärsutvecklare/exploateringsingenjörer'}, 'jobsearch': 2, 'historical': 5},
    {'params': {'q': 'Affärsutvecklare / exploateringsingenjörer'}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': 'Affärsutvecklare exploateringsingenjörer'}, 'jobsearch': 11, 'historical': 16},
    {'params': {'q': 'barnpsykiatri/habilitering'}, 'jobsearch': 1, 'historical': 6},
    {'params': {'q': 'barnpsykiatri / habilitering'}, 'jobsearch': 0, 'historical': 1},
    pytest.param({'params': {'q': 'barnpsykiatri habilitering'}, 'jobsearch': 21, 'historical': 39},
                 marks=pytest.mark.smoke),
    {'params': {'q': 'mentor/kontaktlärare'}, 'jobsearch': 0, 'historical': 1},
    {'params': {'q': 'mentor / kontaktlärare'}, 'jobsearch': 0, 'historical': 7},
    {'params': {'q': 'mentor kontaktlärare'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'Verktygsmakare/Montör'}, 'jobsearch': 17, 'historical': 86},
    {'params': {'q': 'Verktygsmakare / Montör'}, 'jobsearch': 4, 'historical': 4},
    {'params': {'q': 'Verktygsmakare Montör'}, 'jobsearch': 38, 'historical': 156},
    {'params': {'q': 'Kolloledare/specialpedagog'}, 'jobsearch': 18, 'historical': 78},
    {'params': {'q': 'Kolloledare / specialpedagog'}, 'jobsearch': 0, 'historical': 1},
    {'params': {'q': 'Kolloledare specialpedagog'}, 'jobsearch': 24, 'historical': 90},
    {'params': {'q': 'fritidshem/fritidspedagog'}, 'jobsearch': 14, 'historical': 63},
    {'params': {'q': 'fritidshem / fritidspedagog'}, 'jobsearch': 3, 'historical': 0},
    pytest.param({'params': {'q': 'fritidshem fritidspedagog'}, 'jobsearch': 16, 'historical': 59},
                 marks=pytest.mark.smoke),
    {'params': {'q': 'UX/UI Designer'}, 'jobsearch': 1, 'historical': 2},
    {'params': {'q': 'UX / UI Designer'}, 'jobsearch': 1, 'historical': 1},
    {'params': {'q': 'UX UI Designer'}, 'jobsearch': 1, 'historical': 2},

])
def test_freetext_search_slash(test_case):
    """

    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    pytest.param({'params': {'q': '.NET/C#'}, 'jobsearch': 14, 'historical': 38}, marks=pytest.mark.smoke),
    {'params': {'q': '.NET / C#'}, 'jobsearch': 7, 'historical': 15},
    {'params': {'q': '.NET C#'}, 'jobsearch': 67, 'historical': 122},
    {'params': {'q': '.NET /C#'}, 'jobsearch': 2, 'historical': 11},
    {'params': {'q': '.NET/ C#'}, 'jobsearch': 10, 'historical': 30},
    {'params': {'q': '.NET'}, 'jobsearch': 40, 'historical': 82},
    {'params': {'q': 'C#/.net'}, 'jobsearch': 13, 'historical': 40},
    pytest.param({'params': {'q': 'C# .net'}, 'jobsearch': 67, 'historical': 122}, marks=pytest.mark.smoke),
    {'params': {'q': 'C# /.net'}, 'jobsearch': 10, 'historical': 30},
    {'params': {'q': 'C# / .net'}, 'jobsearch': 7, 'historical': 15},
    {'params': {'q': 'C#'}, 'jobsearch': 52, 'historical': 94},
    {'params': {'q': 'dotnet'}, 'jobsearch': 40, 'historical': 79},

])
def test_freetext_search_dot_hash_slash(test_case):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    for words that have . or # (e.g. '.net', 'c#')
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'programmerare'}, 'jobsearch': 157, 'historical': 234},
    {'params': {'q': 'Systemutvecklare'}, 'jobsearch': 157, 'historical': 234},
    {'params': {'q': 'Systemutvecklare/Programmerare'}, 'jobsearch': 14, 'historical': 61},
    {'params': {'q': 'Systemutvecklare Programmerare'}, 'jobsearch': 157, 'historical': 234},
    pytest.param({'params': {'q': 'Systemutvecklare / Programmerare'}, 'jobsearch': 20, 'historical': 18},
                 marks=pytest.mark.smoke),
])
def test_freetext_search_slash_short(test_case):
    run_test_case(test_case)
