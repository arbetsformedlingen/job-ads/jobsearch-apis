import json
import pytest
import requests

import common.constants
from tests.test_resources.test_settings import TEST_URL

pytestmark = [pytest.mark.jobsearch]


def test_search_swagger():
    """
    Test Swagger info for search
    """
    response = requests.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    response_json = json.loads(response.content)
    assert response_json['info']['version'] == common.constants.API_VERSION

    expected = [('/ad/{id}', 2), ('/ad/{id}/logo', 1), ('/complete', 1), ('/search', 1), ('/taxonomy/search', 1)]
    for expected_path, expected_number in expected:
        assert response_json['paths'][expected_path]
        assert len(response_json['paths'][expected_path]['get']['responses']) == expected_number
