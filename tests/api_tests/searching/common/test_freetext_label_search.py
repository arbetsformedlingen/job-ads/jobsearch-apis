import pytest

from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA


LABEL_NYSTARTSJOBB = 'NYSTARTSJOBB'.lower()
LABEL_REKRYTERINGSUTBILDNING = 'REKRYTERINGSUTBILDNING'.lower()
LABEL_DUMMY = 'DUMMY_LABEL'.lower()
LABEL_SWEDISH_CHARS = 'RÄKSMÖRGÅS'.lower()
LABEL_ANYCASE = 'wEirD_CaSE_labEL'.lower()


@pytest.mark.jobsearch
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("test_case", [
    {"query": LABEL_NYSTARTSJOBB, "jobsearch": 251},
    {"query": LABEL_REKRYTERINGSUTBILDNING, "jobsearch": 248},
    {"query": f"{LABEL_NYSTARTSJOBB} {LABEL_DUMMY}", "jobsearch": 299},
    {"query": f"{LABEL_REKRYTERINGSUTBILDNING} kalleankaordutanträff", "jobsearch": 0}, # zero hits since 'rekryteringsutbildning' is both enriched and a label, together with a non enriched term the result is zero hits
    {"query": f"{LABEL_NYSTARTSJOBB} kalleankaordutanträff", "jobsearch": 251},
    {"query": f"python {LABEL_NYSTARTSJOBB}", "jobsearch": 5},
])
def test_freetext_label_search(test_case):
    params = {'q': test_case["query"], 'limit': 100}
    response_json = get_search(params)
    expected = test_case["jobsearch"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected

@pytest.mark.jobsearch
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("test_case", [
    {"query": LABEL_SWEDISH_CHARS, "jobsearch": 10},
    {"query": f"{LABEL_REKRYTERINGSUTBILDNING} {LABEL_SWEDISH_CHARS}", "jobsearch": 2},
    {"query": f"{LABEL_NYSTARTSJOBB} {LABEL_SWEDISH_CHARS}", "jobsearch": 260},
])
def test_freetext_label_search_swedish_chars(test_case):
    params = {'q': test_case["query"], 'limit': 100}
    response_json = get_search(params)
    expected = test_case["jobsearch"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected


@pytest.mark.jobsearch
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("test_case", [
    {"query": LABEL_ANYCASE, "jobsearch": 10},
])
def test_freetext_label_search_anycase(test_case):
    params = {'q': test_case["query"], 'limit': 100}
    response_json = get_search(params)
    expected = test_case["jobsearch"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected


@pytest.mark.jobsearch
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("test_case", [
    {"query": "-nystartsjobb", "jobsearch": 4778},
    {"query": "-rekryteringsutbildning", "jobsearch": 4781},
])
def test_negative_freetext_label_search(test_case):
    params = {'q': test_case["query"], 'limit': 100}
    response_json = get_search(params)
    expected = test_case["jobsearch"]

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected
