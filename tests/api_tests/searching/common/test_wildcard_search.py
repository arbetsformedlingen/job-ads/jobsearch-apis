import pytest

from tests.test_resources.api_test_runner import run_test_case

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


# TODO: adapt for historical ads test data


@pytest.mark.smoke
@pytest.mark.parametrize("test_case", [
    {'params': {'q': 'murar*'}, 'jobsearch': 1, 'historical': 14},
    {'params': {'q': '*utvecklare'}, 'jobsearch': 183, 'historical': 423},
    {'params': {'q': 'utvecklare*'}, 'jobsearch': 93, 'historical': 164},
    {'params': {'q': '*utvecklare*'}, 'jobsearch': 0, 'historical': 0, 'comment': 'multiple wildcards doe not work'},
    {'params': {'q': 'Anläggningsarbetar*'}, 'jobsearch': 6, 'historical': 18},
    {'params': {'q': 'Arbetsmiljöingenjö*'}, 'jobsearch': 1, 'historical': 10},
    {'params': {'q': 'Behandlingsassisten*'}, 'jobsearch': 10, 'historical': 54},
    {'params': {'q': 'Bilrekonditionerar*'}, 'jobsearch': 3, 'historical': 9},
    {'params': {'q': 'Eventkoordinato*'}, 'jobsearch': 0, 'historical': 2},
    {'params': {'q': 'Fastighetsskötar*'}, 'jobsearch': 16, 'historical': 68},
    {'params': {'q': 'Fastighet*'}, 'jobsearch': 244, 'historical': 692},
    {'params': {'q': 'Kundtjänstmedarbetar*'}, 'jobsearch': 26, 'historical': 139},
    {'params': {'q': 'Kundtjänst*'}, 'jobsearch': 107, 'historical': 429},
    {'params': {'q': 'sjukskö*'}, 'jobsearch': 651, 'historical': 1283},
    {'params': {'q': 'sköterska*'}, 'jobsearch': 7, 'historical': 7},
    {'params': {'q': 'skötersk*'}, 'jobsearch': 11, 'historical': 30},
    {'params': {'q': 'sjukvårds*tion'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'sj'}, 'jobsearch': 0, 'historical': 7, 'comment': 'min 3 characters'},
    {'params': {'q': 'sj*'}, 'jobsearch': 0, 'historical': 0, 'comment': 'min 3 characters'},
    {'params': {'q': 'sju*'}, 'jobsearch': 1095, 'historical': 2746},
])
def test_wildcard_search_exact_match(test_case):
    """
    Test different wildcard queries
    check that the number of results is exactly as expected
    """
    run_test_case(test_case)
