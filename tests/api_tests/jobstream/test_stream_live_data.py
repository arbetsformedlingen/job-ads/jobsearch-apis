import pytest
from tests.test_resources.helper import get_stream
from common.constants import UPDATED_BEFORE_DATE
from tests.test_resources.test_settings import NUMBER_OF_ADS, DAWN_OF_TIME, current_time_stamp
# mark all tests in this file as @pytest.mark.jobstream
pytestmark = pytest.mark.jobstream

def test_stream_filter_on_date_interval():
    params = {'date': DAWN_OF_TIME, UPDATED_BEFORE_DATE: current_time_stamp}
    list_of_ads = get_stream( params)
    assert len(list_of_ads) >= NUMBER_OF_ADS
