import pytest

pytestmark = [pytest.mark.jobsearch]


def test_education_fields_exists( random_ads):
    """
    Value should be a list (that could be empty)
    """
    for ad in random_ads:
        assert isinstance(ad['must_have']['education'], list)
        assert isinstance(ad['must_have']['education_level'], list)
        assert isinstance(ad['nice_to_have']['education'], list)
        assert isinstance(ad['nice_to_have']['education_level'], list)
