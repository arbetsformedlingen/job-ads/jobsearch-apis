import random
import pytest

from tests.test_resources import test_settings
from tests.test_resources.helper import get_snapshot, get_search, get_historical_stats, get_stream


@pytest.fixture(scope='session')
def jobstream_fixture(request):
    """
    get ads from stream or snapshot based on parameter
    usage:

    from tests.test_resources.test_settings import SNAPSHOT, STREAM

    @pytest.mark.parametrize("jobstream_fixture", [STREAM, SNAPSHOT], indirect=True)
    def test(jobstream_fixture):
        for ad in jobstream_fixture:
            pass

    """
    if request.param == test_settings.SNAPSHOT:
        return get_snapshot()
    else:
        return get_stream({'date': '2020-10-01T00:00:01'})


@pytest.fixture
def random_ads():
    offset = random.randint(0, 1900)
    params = {'limit': 100, 'offset': offset}
    return get_search(params)['hits']


@pytest.fixture(scope='session')
def historical_stats():
    """
    get stats once to save time and resources
    """
    stats = get_historical_stats(params={})
    return stats
