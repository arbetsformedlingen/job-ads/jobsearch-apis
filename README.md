# Search ads API
This repository contains code for JobSearch, JobStream and Historical APIs. 
    
* ___JobSearch___ is an search engine API intended for job boards, such as Platsbanken. 
* ___JobStream___ is meant to simplify keeping a local copy of currently published ads in Platsbanken by subscribing to a stream of changes.  
* ___Historical___ is a repository for ads that is not longer published or has expired. 


## Requirements
* Python 3.9.4 or later
* Access to a host or server running Elasticsearch version 7.11.2 or later, but earlier than 8

## Environment variables
The application is entirely configured using environment variables. 
Default values are provided with the listing.


### Application configuration

| Variable Name       | Default Value    | Description   |
|---                  |---               |---            |
| ES_HOST             | 127.0.0.1        | Specifies which elastic search host to use for searching. | 
| ES_PORT             | 9200             | Port number to use for elastic search |
| ES_INDEX            | platsannons-read | Specifies which index to search ads from (JobSearch and JobStream) | 
| ES_USER             |                  | Sets username for elastic search (no default value) |
| ES_PWD              |                  | Sets password for elastic search (no default value) |
| ES_TAX_INDEX        | taxonomy         | Specifies which index contains taxonomy information. |


### Flask configuration
| Variable Name       | Default Value    | Description   |
|---                  |---               |---            |
| FLASK_APP           |                  | The name of the application. Set to "jobsearch", "jobstream" or "historical". |
| FLASK_ENV           |                  | Set to "development" for development. |


## Running the service

To start up the application, set the appropriate environment variables as described above. Then run the following
commands.

    $ pip install -r requirements.txt
    $ export FLASK_APP=jobsearch
    $ export FLASK_ENV=development
    $ flask run

Go to http://127.0.0.1:5000 to access the swagger API.  
To run Jobstream or historical, run one these commands instead of jobsearch in the example above

    $ export FLASK_APP=jobstream 
    $ export FLASK_APP=historical

## Tests

### Run unit tests

    $ pytest tests/unit_tests

### Run integration tests

When running integration tests, an actual application is started,
so you need to specify environment variables for elastic search in order for it to run properly.

    $ pytest tests/integration_tests

### Run API tests

When running api tests, the application must be started. Most of the tests are dependent on a specific set of ads in the Elastic index
| Application       | Test group name |
|---                |---              |
| Jobsearch         | jobsearch       |
| Jobstream         | jobstream       |
| Historical ads    | historical      |
     

    In addition, there is a test group name 'complete' that can be run when the application is started as jobsearch

    $ pytest -m <test group name> tests/api_tests


### Test coverage
    
https://pytest-cov.readthedocs.io/en/latest/

    $ pytest -m <test group name> --cov=sokannonser tests/


## Further documentation
More documentation is found in the /docs package.