# coding=utf-8
import pytest
from common.ad_formatter import format_hits_with_only_original_values


def test_contact_persons():
    hits_contacts = [
        {'id': 123, 'application_contacts': [
            {'name': 'Testy Testsson', 'description': 'blabla', 'email': 'test@jobtechdev.se',
             'telephone': '+01011122233', 'contactType': None}]},
        {'id': 456, 'application_contacts': [
            {'name': 'Testy Testsson', 'description': 'blabla', 'email': 'test@jobtechdev.se',
             'telephone': '+01011122233', 'contactType': None}]}
    ]
    result_contacts = format_hits_with_only_original_values(hits_contacts)
    expected = [{'id': 123, 'application_contacts': [
        {'name': 'Testy Testsson', 'description': 'blabla', 'email': 'test@jobtechdev.se', 'telephone': '+01011122233',
         'contactType': None}]}, {'id': 456, 'application_contacts': [
        {'name': 'Testy Testsson', 'description': 'blabla', 'email': 'test@jobtechdev.se', 'telephone': '+01011122233',
         'contactType': None}]}]
    assert result_contacts == expected


@pytest.mark.parametrize("value", [
    [{'something': 123}, {'something_else': 'abc'}],
    [{'id': 123, 'application_contacts': [None]}]
])
def test_no_change(value):
    assert format_hits_with_only_original_values(value) == value


@pytest.mark.parametrize("value", [None, False, 0])
def test_type_error(value):
    with pytest.raises(TypeError):
        format_hits_with_only_original_values(value)


@pytest.mark.parametrize("value", [None, False, 0, 'some_string'])
def test_attribute_error(value):
    value_as_list = [value]
    with pytest.raises(AttributeError):
        format_hits_with_only_original_values(value_as_list)


@pytest.mark.parametrize("field", ['occupation_group', 'occupation_field'])
@pytest.mark.parametrize("data, expected", [
    (['some group'], 'some group'),
    (['some group', 'some_other_group'], 'some group'),
    ([False], False),
    (False, False),
    ([True, False], True),
    ('not_a_list', 'not_a_list'),
    ({'my_key': 'value'}, {'my_key': 'value'}),
])
def test_occupation_group_field(data, expected, field):
    hits = [{field: data}]
    expected = [{field: expected}]
    result = format_hits_with_only_original_values(hits)
    assert result == expected


@pytest.mark.parametrize('skill_type', ['must_have', 'nice_to_have'])
@pytest.mark.parametrize('data', [
    {'skill': ['you must have this']},
    {'skill': ['another must', 'and this']},
    {'skill': ['nice', 'nicer', 'nicest']},
    {'skill': []},
])
def test_skills_unchanged(skill_type, data):
    hits = [{skill_type: data}]
    assert format_hits_with_only_original_values(hits) == hits


def test_employment_type_original_value():
    hits = [
        {'ad': '1', 'employment_type': [
            {'concept_id': 'Jh8f_q9J_pbJ', 'label': 'Sommarjobb / feriejobb', 'legacy_ams_taxonomy_id': '2',
             'original_value': True},
            {'concept_id': 'EBhX_Qm2_8eX', 'label': 'Säsongsanställning', 'legacy_ams_taxonomy_id': None}]}]
    expected = [
        {'ad': '1', 'employment_type':
            {'concept_id': 'Jh8f_q9J_pbJ', 'label': 'Sommarjobb / feriejobb', 'legacy_ams_taxonomy_id': '2'}}]
    assert format_hits_with_only_original_values(hits) == expected


def test_employment_type_original_value_false():
    hits = [
        {'ad': '1', 'employment_type': [
            {'concept_id': 'Jh8f_q9J_pbJ', 'label': 'Sommarjobb / feriejobb', 'legacy_ams_taxonomy_id': '2',
             'original_value': False},
            {'concept_id': 'EBhX_Qm2_8eX', 'label': 'Säsongsanställning', 'legacy_ams_taxonomy_id': None}]}]
    expected = [{'ad': '1', 'employment_type': None}]
    assert format_hits_with_only_original_values(hits) == expected
