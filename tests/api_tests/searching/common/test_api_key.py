import pytest
import requests
from tests.test_resources.helper import get_search

pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


def test_use_api_key():
    """
    Check that nothing breaks when using api-key in header
    """
    test_headers = {'api-key': 'test_api_key', 'accept': 'application/json'}
    s = requests.sessions.Session()
    s.headers.update(test_headers)
    params = {'q': 'nyckel', 'limit': '5'}
    response = get_search(params)
    for hit in response["hits"]:
        assert hit["headline"]
