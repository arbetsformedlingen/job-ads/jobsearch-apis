import pytest

from tests.unit_tests.test_resources.mock_for_querybuilder_tests import mock_querybuilder_jobsearch, \
    mock_querybuilder_historical
from tests.test_resources.helper import is_dst


@pytest.mark.parametrize('jobsearch', [True, False])
@pytest.mark.parametrize('value', [True, False, 'true', 'false', 'True', 'False'])
def test_experience_values_jobsearch(value, jobsearch):
    args = {'experience': value}
    if value in [True, 'true', 'True']:
        value_in_expected = True
    else:
        value_in_expected = False

    expected_time = 'now+2H/m' if is_dst() else 'now+1H/m'  # f"{expected_time}"

    if jobsearch:
        mock_querybuilder = mock_querybuilder_jobsearch
        filter = [{'range': {'publication_date': {'lte': f"{expected_time}"}}},
                  {'range': {'last_publication_date': {'gte': f"{expected_time}"}}},
                  {'term': {'removed': False}}]
    else:
        mock_querybuilder = mock_querybuilder_historical
        filter = []

    expected_query_dsl = {'from': 0, 'size': 10, 'track_total_hits': True, 'track_scores': True, 'query': {
        'bool': {'must': [{'term': {'experience_required': value_in_expected}}],
                 'filter': filter}},
                          'aggs': {'positions': {'sum': {'field': 'number_of_vacancies'}}},
                          'sort': ['_score', {'publication_date': 'desc'}]}

    result = mock_querybuilder.parse_args(args)
    assert result == expected_query_dsl
