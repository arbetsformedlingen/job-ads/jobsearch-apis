# -*- coding: utf-8 -*-
import pytest
from tests.test_resources.api_test_runner import run_test_case
from tests.test_resources.helper import get_search

# marks all tests as jobsearch & smoke
pytestmark = [pytest.mark.jobsearch, pytest.mark.smoke]

NYSTARTSJOBB = "nystartsjobb"
REKRYTERINGSUTBILDNING = "rekryteringsutbildning"

NUMBER_NYSTARTSJOBB = 249
NUMBER_REKRYTERINGSUTBILDNING = 247
NUMBER_BOTH = 15

"""
Tests for parameter "label" which has values that are generally known but can change when new labels ("nyckelord") are added in Ledigt Arbete.
Label can be used to include ads which has a a specific label.
If multiple labels are used, the relation between them is AND

["label-1", "label-2"] == label-1 AND label-2

Test data LA mock api 2024-01-23:
  [["nystartsjobb"], 232],
  [["rekryteringsutbildning"], 226],
  [["dummy_label"], 44],
  [["nystartsjobb", "rekryteringsutbildning"], 14],
  [["weird_case_label"], 10],
  [["räksmörgås"], 7],
  [["rekryteringsutbildning", "dummy_label"], 4],
  [["rekryteringsutbildning", "räksmörgås"], 2],
  [["nystartsjobb", "räksmörgås"], 1],
  [["nystartsjobb", "dummy_label"], 1],
  [["nystartsjobb", "rekryteringsutbildning", "dummy_label"], 1]
"""


@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"label": NYSTARTSJOBB}, "jobsearch": NUMBER_NYSTARTSJOBB},
        {
            "params": {"label": REKRYTERINGSUTBILDNING},
            "jobsearch": NUMBER_REKRYTERINGSUTBILDNING,
        },
        {"params": {"label": "räksmörgås"}, "jobsearch": 10},
        {"params": {"label": "weird_case_label"}, "jobsearch": 10},
    ],
)
def test_single_label(test_case):
    """
    Returns ads that fulfills any of these conditions:
    - has only this label
    - has this label in combination with other labels
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"label": [REKRYTERINGSUTBILDNING, NYSTARTSJOBB]},
            "jobsearch": NUMBER_BOTH,
        },
        {
            "params": {"label": [NYSTARTSJOBB, REKRYTERINGSUTBILDNING]},
            "jobsearch": NUMBER_BOTH,
        },
    ],
)
def test_multiple_labels(test_case):
    """
    Test that label includes ads
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"label": NYSTARTSJOBB}, "jobsearch": NUMBER_NYSTARTSJOBB},
        {"params": {"label": [NYSTARTSJOBB]}, "jobsearch": NUMBER_NYSTARTSJOBB},
    ],
)
def test_label_list_string(test_case):
    """
    Test that label includes ads
    Result should be the same for both string and [string]
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"label": "unknown"}, "jobsearch": 0},
    ],
)
def test_label_unknown(test_case):
    """
    Test that label includes or excludes unknown labels
    the negative filtering should have no effect
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"label": [REKRYTERINGSUTBILDNING, "unknown"]}, "jobsearch": 0},
    ],
)
def test_label_with_unknown(test_case):
    """
    Combine known and unknown labels
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {"label": [NYSTARTSJOBB, NYSTARTSJOBB]}, "jobsearch": NUMBER_NYSTARTSJOBB},
    ],
)
def test_repeat_label(test_case):
    """
    Repeating labels should not change result
    """
    run_test_case(test_case)


@pytest.mark.parametrize(
    "test_case",
    [
        {
            "params": {"label": [REKRYTERINGSUTBILDNING]},
        }
    ],
)
def test_label_field_in_result_model(test_case):
    """
    Test that the field label is in the result model.
    """
    params = test_case["params"]
    response = get_search(params)
    number_of_hits = response["total"]["value"]

    assert number_of_hits > 0

    for hit in response["hits"]:
        assert REKRYTERINGSUTBILDNING in hit["label"]
