import pytest

from common.date_and_time_handling import format_start_date_historical, format_end_date_historical
from common.constants import DATE_FORMAT


@pytest.mark.parametrize('start_date, expected', [
    ('2020', '2020-01-01'),
    ('2020-01', '2020-01-01'),
    ('2020-09', '2020-09-01'),
    ('2020-12', '2020-12-01'),
    ('2020-02', '2020-02-01'),
    ('2020-02-29', '2020-02-29'),
    ('2020-02-28', '2020-02-28'),
    ('20', '2000-01-20')
])
def test_start_date(start_date, expected):
    """
    Check that incomplete dates are returned as full timestamps incl hh:mm:ss
    'T00:00:00' is omitted from the 'expected' part of the test cases for readability
    """
    result = format_start_date_historical(start_date)
    assert result == f"{expected}T00:00:00"


@pytest.mark.parametrize('end_date, expected', [
    ('2020', '2020-12-31'),
    ('2020-01', '2020-01-31'),
    ('2020-09', '2020-09-30'),
    ('2020-12', '2020-12-31'),
    ('2020-02', '2020-02-29'),
    ('2021-02', '2021-02-28'),
    ('2021-02-27', '2021-02-27'),
])
def test_end_date(end_date, expected):
    """
    Check that incomplete dates are returned as datetime objects
    'T23:59:59' is omitted from the 'expected' part of the test cases for readability
    """
    result = format_end_date_historical(end_date).strftime(DATE_FORMAT)

    assert result == f"{expected}T23:59:59"


def test_end_date_none():
    assert format_end_date_historical(None) is None


def test_start_date_none():
    assert format_start_date_historical(None) is None


# no idea why '2013-13' could not pass
@pytest.mark.parametrize('wrong_date', [
    '20201',
    '2021-09-31',
    '20-20'
])
def test_wrong_start_date(wrong_date):
    """
    Check that wrong dates raises a ValueError
    """
    with pytest.raises(ValueError):
        format_start_date_historical(wrong_date)
