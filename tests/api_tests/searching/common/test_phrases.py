# -*- coding: utf-8 -*-

import pytest

from tests.test_resources.helper import get_search, get_total
from tests.test_resources.api_test_runner import run_test_case, HISTORICAL
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA, OPEN_FOR_ALL_PHRASE, REMOTE_MATCH_PHRASES, \
    TRAINEE_PHRASES, LARLING_PHRASES, FRANCHISE_PHRASES, HIRE_WORKPLACE_PHRASES
from common.constants import REMOTE, TRAINEE, LARLING, FRANCHISE, HIRE_WORK_PLACE, OPEN_FOR_ALL

# marks all tests in this file as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on specific data set")
@pytest.mark.parametrize('test_case', [
    {'params': {'open_for_all': True, 'limit': 0}, 'jobsearch': 152, 'historical': 11},
    {'params': {'open_for_all': False, 'limit': 0}, 'jobsearch': 4877, 'historical': 17739},
    {'params': {'remote': True, 'limit': 0}, 'jobsearch': 36, 'historical': 34},
    {'params': {'remote': False, 'limit': 0}, 'jobsearch': 4993, 'historical': 17716},
    {'params': {'trainee': True, 'limit': 0}, 'jobsearch': 4, 'historical': 2},
    {'params': {'trainee': False, 'limit': 0}, 'jobsearch': 5025, 'historical': 17748},
    {'params': {'larling': True, 'limit': 0}, 'jobsearch': 4, 'historical': 2},
    {'params': {'larling': False, 'limit': 0}, 'jobsearch': 5025, 'historical': 17748},
    {'params': {'franchise': True, 'limit': 0}, 'jobsearch': 7, 'historical': 4},
    {'params': {'franchise': False, 'limit': 0}, 'jobsearch': 5022, 'historical': 17746},
])
def test_phrase_params_number_of_ads(test_case):
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    {'params': {'q': 'utvecklare', 'open_for_all': True, 'limit': 0}, 'jobsearch': 2, 'historical': 0},
    {'params': {'q': 'mötesbokare', 'open_for_all': True, 'limit': 0}, 'jobsearch': 2, 'historical': 0},
    {'params': {'q': 'mötesbokare', 'open_for_all': False, 'limit': 0}, 'jobsearch': 7, 'historical': 29},
    {'params': {'q': 'tekniker', 'open_for_all': False, 'limit': 0}, 'jobsearch': 25, 'historical': 105},
    {'params': {'q': 'säljare', 'open_for_all': True, 'limit': 0}, 'jobsearch': 8, 'historical': 0},
    {'params': {'q': 'säljare', 'open_for_all': False, 'limit': 0}, 'jobsearch': 217, 'historical': 1279},
    {'params': {'q': 'utvecklare', 'remote': True, 'limit': 0}, 'jobsearch': 2, 'historical': 1},
    {'params': {'q': 'mötesbokare', 'remote': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'mötesbokare', 'remote': False, 'limit': 0}, 'jobsearch': 9, 'historical': 29},
    {'params': {'q': 'tekniker', 'remote': False, 'limit': 0}, 'jobsearch': 26, 'historical': 105},
    {'params': {'q': 'säljare', 'remote': True, 'limit': 0}, 'jobsearch': 1, 'historical': 2},
    {'params': {'q': 'säljare', 'remote': False, 'limit': 0}, 'jobsearch': 224, 'historical': 1277},
    {'params': {'q': 'traineeprogram', 'trainee': False, 'limit': 0}, 'jobsearch': 4, 'historical': 4},
    {'params': {'q': 'frisör', 'trainee': True, 'limit': 0}, 'jobsearch': 1, 'historical': 1},
    {'params': {'q': 'frisör', 'trainee': False, 'limit': 0}, 'jobsearch': 12, 'historical': 48},
    {'params': {'q': 'tekniker', 'trainee': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'säljare', 'trainee': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'säljare', 'trainee': False, 'limit': 0}, 'jobsearch': 225, 'historical': 1279},
    {'params': {'q': 'lärling', 'larling': True, 'limit': 0}, 'jobsearch': 2, 'historical': 1},
    {'params': {'q': 'frisör', 'larling': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'frisör', 'larling': False, 'limit': 0}, 'jobsearch': 13, 'historical': 49},
    {'params': {'q': 'snickare', 'larling': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'snickare', 'larling': False, 'limit': 0}, 'jobsearch': 32, 'historical': 92},
    {'params': {'q': 'rörmokare', 'larling': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'lärling', 'franchise': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'lärling', 'franchise': False, 'limit': 0}, 'jobsearch': 4, 'historical': 14},
    {'params': {'q': 'frisör', 'franchise': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'frisör', 'franchise': False, 'limit': 0}, 'jobsearch': 13, 'historical': 49},
    {'params': {'q': 'pressbyrån', 'franchise': True, 'limit': 0}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': 'företagare', 'franchise': True, 'limit': 0}, 'jobsearch': 1, 'historical': 0},
    {'params': {'q': 'företagare', 'franchise': False, 'limit': 0}, 'jobsearch': 44, 'historical': 100},
    {'params': {'q': 'frisör', 'hire-work-place': True, 'limit': 0}, 'jobsearch': 2, 'historical': 0},
    {'params': {'q': 'frisör', 'hire-work-place': False, 'limit': 0}, 'jobsearch': 11, 'historical': 49},
    {'params': {'q': 'mötesbokare', 'hire-work-place': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'mötesbokare', 'hire-work-place': False, 'limit': 0}, 'jobsearch': 9, 'historical': 29},
    {'params': {'q': 'tekniker', 'hire-work-place': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'frisörstol', 'hire-work-place': True, 'limit': 0}, 'jobsearch': 0, 'historical': 0},
    {'params': {'q': 'frisörstol', 'hire-work-place': False, 'limit': 0}, 'jobsearch': 0, 'historical': 0},

])
def test_phrase_query(test_case):
    run_test_case(test_case)


@pytest.mark.parametrize('phrase_params, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('query', ['hemifrån', 'säljare', 'java', 'lärare', 'tekniker', 'öppen för alla', 'trainee'])
def test_query_content( phrase_params, phrases, query):
    """
    Check that ads have at least one phrase associated with "öppen för alla"
    in description or title when using 'open_for_all': True
    """
    successes = []
    result = get_search({'q': query, phrase_params: True, 'limit': 100})
    hits = result['hits']
    for hit in hits:
        for phrase in phrases:
            if phrase in hit['description']['text'].lower() or phrase in hit['headline'].lower():
                successes.append(True)
                break
    assert len(successes) >= len(hits)


@pytest.mark.parametrize('phrase_params, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('bool_value', [True, False])
def test_phrases_binary( phrase_params, phrases, bool_value):
    paramss = {phrase_params: bool_value}
    response = get_search(paramss)
    for hit in response['hits']:
        text_to_check = f"{hit['description']['text']} {hit['headline']}".lower()
        assert bool_value == any(x in text_to_check for x in phrases)


def _binary_query(query, phrase_params, phrases, bool_value):
    params = {'q': query, phrase_params: bool_value}
    response = get_search(params)
    for hit in response['hits']:
        text_to_check = f"{hit['description']['text']} {hit['headline']}".lower()
        assert bool_value == any(x in text_to_check for x in phrases)

@pytest.mark.parametrize("query", ['säljare', 'java', 'sjuksköterska', 'frisör'])
@pytest.mark.parametrize('phrase_params, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('bool_value', [True, False])
def test_phrases_binary_query(query, phrase_params, phrases, bool_value):
    _binary_query(query, phrase_params, phrases, bool_value)


@pytest.mark.skipif(HISTORICAL, reason="'open for all' does not work on historical ads")
@pytest.mark.parametrize("query", ['säljare', 'java', 'sjuksköterska', 'frisör'])
@pytest.mark.parametrize('phrase_params, phrases', [
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
])
@pytest.mark.parametrize('bool_value', [True, False])
def test_phrases_binary_query_open_for_all( query, phrase_params, phrases, bool_value):
    _binary_query(query, phrase_params, phrases, bool_value)


@pytest.mark.parametrize('phrase_params', [REMOTE,
                                           OPEN_FOR_ALL,
                                           TRAINEE,
                                           LARLING,
                                           FRANCHISE,
                                           HIRE_WORK_PLACE])
def test_sum_phrases( phrase_params):
    open_none = get_total(params={'limit': 0})
    open_true = get_total(params={phrase_params: True, 'limit': 0})
    open_false = get_total(params={phrase_params: False, 'limit': 0})
    assert open_false + open_true == open_none
