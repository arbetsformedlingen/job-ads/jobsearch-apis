# Testing of JobSearch-APIS

# Testing

This document describes

- How to load test data
- What different kinds of tests there are in the project
- Configuration
- How to run tests

## But first

Read `README.md`
Commands and paths are for Windows.

## Test data for api tests

The api tests are dependent on a specific set of ads.
Pro:

- easy to detect changes in search results
  Con:
- If a change in the search result happens, and it's expected, the tests needs updating with the new number of expected
  ads.

# Test data for JobSearch and JobStream

This is loaded from an api that has the same endpoints as where "live ads" are collected, but it has ads that the
api tests are matched against.

Importing ads into an OpenSearch index is done
with [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers)
The ad source
is [JobSearch Ad Moch API](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/jobsearch-ad-mock-api) which
is deployed in OpenShift testing cluster, but can also be run locally.

| env var               | Value                                                                                                                          |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------|
| LA_FEED_URL           | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/andradeannonser/              |
| LA_DETAILS_URL        | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/annonser/                     |
| LA_BOOTSTRAP_FEED_URL | https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/sokningar/publiceradeannonser |

or with JobSearch Ad Mock API running locally:

| env var               | Value                                               |
|-----------------------|-----------------------------------------------------|
| LA_FEED_URL           | http://127.0.0.1:5000/andradeannonser/              |
| LA_DETAILS_URL        | http://127.0.0.1:5000/annonser/                     |
| LA_BOOTSTRAP_FEED_URL | http://127.0.0.1:5000/sokningar/publiceradeannonser |

The aliases created by Jobsearch-importers are used when starting the apis

# Test data for Historical

This is loaded from a file which is stored in Team Jobbdata's Minio bucket.
Use jobsearch-importers to load data with the command `historical opensearch testdata.jsonl`

# Run tests

The different kinds of tests have different dependencies.

To run the API tests, the api must be started in the same mode as you try to test.
i.e API started as jobsearch allows you to run api tests with the pytest.mark `jobsearch`. Trying to run jobsearch tests
against Historical api will fail.
Make sure you shut down the api before continuing with the next.
Some tests are relevant for both jobsearch and historical. The files with such tests are found in
`tests\api_tests\searching\common` and have a line `pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]
`so that Pytest can collect the relevant tests with the `-m <MARK>`parameter.

| command                                          | Type of tests                      | Dependencies / Configuration          |
|--------------------------------------------------|------------------------------------|---------------------------------------|
| `pytest tests\unit_tests`                        | unit tests                         | None                                  |
| `pytest tests\integration_tests`                 | tests that need the taxonomy index | OpenSearch                            |
| `pytest -m jobsearch tests\api_tests\searching`  | api tests for JobSearch api        | OpenSearch, JobSearch API, test data  |
| `pytest -m historical tests\api_tests\searching` | api tests for Historical ads api   | OpenSearch, Historical API, test data |
| `pytest tests\api_tests\jobstream`               | api tests for JobStream API        | OpenSearch, JobStream API, test data  |

To test everything you need to start an api, run tests, shut down the api and continue with the next api.

## Automatic test execution

The only tests that are executed automatically are unit tests when a Docker image is built.

## Test coverage

Run `pytest-cov`as part of a pytest command, e.g.
`pytest --cov search --cov common tests\unit_tests`

## Pytest fixtures

File: `tests\api_tests\conftest.py`
`session`: so that tests reuse a http session. To be removed.
`jobstream_fixture`: Loads all ads once to speed up tests, the ads are used in various tests.
`random_ads`: Returns 100 ads, randomized by using different offset. Only random from the "first part" of all ads since
the maximum value for offset is too low to load all ads to be loaded.
`historical_stats`: This is a slow operation, the fixture is there to run it once and reuse the result in different
tests.

## Configuration

`tests/test_resources/test_settings.py`
`TEST_URL = os.getenv('TEST_URL', 'http://127.0.0.1:5000')`
Change if you are running the api on another port than Flask default (5000)

`common/settings.py`
Set OpenSearch connection details if you are using anything other than default values

## Test cases
Test cases generally have an expected value (i.e. number of ads returned by the query), the test cases have entries for
both Jobsearch and Historical. The tests are made so that they check if it's Jobsearch or Historical api that's
running and compare the actual result to the expected result for the api..
