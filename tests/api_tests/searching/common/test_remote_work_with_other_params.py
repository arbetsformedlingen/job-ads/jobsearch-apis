import pytest

from tests.test_resources.concept_ids import occupation, occupation_group, occupation_field, concept_ids_geo as geo
from tests.test_resources.helper import get_total
from tests.test_resources.test_settings import NUMBER_OF_ADS, NUMBER_OF_HISTORICAL_ADS
from common.constants import UNSPECIFIED_SWEDEN_WORKPLACE, ABROAD, REMOTE, PUBLISHED_BEFORE, PUBLISHED_AFTER, \
    EXPERIENCE_REQUIRED, OCCUPATION, OCCUPATION_GROUP, OCCUPATION_FIELD, MUNICIPALITY
from tests.test_resources.api_test_runner import run_test_case

TEST_DATE = "2020-12-10T23:59:59"
NUMBER_OF_REMOTE_ADS = 36
# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {'params': {REMOTE: True, 'q': 'utvecklare'}, 'jobsearch': 2, 'historical': 1},
    {'params': {REMOTE: False, 'q': 'utvecklare'}, 'jobsearch': 156, 'historical': 238},
    {'params': {REMOTE: None, 'q': 'utvecklare'}, 'jobsearch': 158, 'historical': 239},
    {'params': {REMOTE: False, 'q': 'säljare'}, 'jobsearch': 224, 'historical': 1277},
    {'params': {REMOTE: None, 'q': 'säljare'}, 'jobsearch': 225, 'historical': 1279},
    {'params': {REMOTE: True, OCCUPATION: occupation.saljkonsulent}, 'jobsearch': 0, 'historical': 0},
    {'params': {REMOTE: None, OCCUPATION: occupation.saljkonsulent}, 'jobsearch': 4, 'historical': 3},
    {'params': {OCCUPATION: occupation.saljkonsulent}, 'jobsearch': 4, 'historical': 3},
    {'params': {OCCUPATION: occupation.mjukvaruutvecklare}, 'jobsearch': 50, 'historical': 26},
    {'params': {REMOTE: True, OCCUPATION: occupation.mjukvaruutvecklare}, 'jobsearch': 1, 'historical': 0},

])
def test_remote_occupation(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [

    {'params': {REMOTE: None, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
     'jobsearch': 239, 'historical': 145},
    {'params': {REMOTE: False, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_},
     'jobsearch': 235, 'historical': 144},
    {'params': {REMOTE: None, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_}, 'jobsearch': 75,
     'historical': 116},
    {'params': {REMOTE: True, OCCUPATION_GROUP: occupation_group.foretagssaljare}, 'jobsearch': 2, 'historical': 0},
])
def test_remote_occupation_group(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: True, OCCUPATION_FIELD: occupation_field.data_it}, "jobsearch": 10, "historical": 4},
    {"params": {REMOTE: True, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, "jobsearch": 5,
     "historical": 15},
    {"params": {REMOTE: False, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, "jobsearch": 573,
     "historical": 905},

])
def test_remote_occupation_field(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {'q': 'Stockholm'}, "jobsearch": 844, "historical": 3348},
    pytest.param({"params": {REMOTE: True, 'q': 'Stockholm'}, "jobsearch": 13, "historical": 9},
                 marks=pytest.mark.smoke),
    {"params": {MUNICIPALITY: geo.stockholm}, "jobsearch": 775, "historical": 861},
    {"params": {REMOTE: True, MUNICIPALITY: geo.stockholm}, "jobsearch": 13, "historical": 9},
    {"params": {REMOTE: False, MUNICIPALITY: geo.stockholm}, "jobsearch": 762, "historical": 852},
])
def test_remote_municipality(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {'q': 'Stockholms Län'}, "jobsearch": 1228, "historical": 1475},
    pytest.param({"params": {REMOTE: True, 'q': 'Stockholms Län'}, "jobsearch": 14, "historical": 9},
                 marks=pytest.mark.smoke),
    pytest.param({"params": {REMOTE: False, 'q': 'Stockholms Län'}, "jobsearch": 1214, "historical": 1466},
                 marks=pytest.mark.smoke),
    {"params": {'region': geo.stockholms_lan}, "jobsearch": 1227, "historical": 1396},
    {"params": {REMOTE: True, 'region': geo.stockholms_lan}, "jobsearch": 14, "historical": 9},
    {"params": {REMOTE: None, 'region': geo.vastra_gotalands_lan}, "jobsearch": 779, "historical": 719},
    {"params": {REMOTE: True, 'region': geo.vastra_gotalands_lan}, "jobsearch": 3, "historical": 5},
    {"params": {'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, "jobsearch": 26, "historical": 46},
    {"params": {REMOTE: True, 'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, "jobsearch": 0, "historical": 0},
])
def test_remote_region(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {PUBLISHED_AFTER: TEST_DATE}, "jobsearch": 4935, "historical": 4247},
    {"params": {PUBLISHED_BEFORE: TEST_DATE}, "jobsearch": 94, "historical": 13503},
    pytest.param({"params": {REMOTE: True, PUBLISHED_BEFORE: TEST_DATE}, "jobsearch": 4, "historical": 3},
                 marks=pytest.mark.smoke),
])
def test_remote_publish_date(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {ABROAD: True}, "jobsearch": 30, "historical": 23},
    {"params": {ABROAD: False}, "jobsearch": NUMBER_OF_ADS, "historical": NUMBER_OF_HISTORICAL_ADS},
    pytest.param({"params": {REMOTE: True, ABROAD: False}, "jobsearch": NUMBER_OF_REMOTE_ADS, "historical": 34},
                 marks=pytest.mark.smoke),
    # abroad False does nothing
])
def test_abroad(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {UNSPECIFIED_SWEDEN_WORKPLACE: True}, "jobsearch": 135, "historical": 12852},
    pytest.param({"params": {REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: True}, "jobsearch": 2, "historical": 5},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: False}, "jobsearch": NUMBER_OF_REMOTE_ADS, "historical": 34}

])
def test_unspecified_workplace(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    {"params": {EXPERIENCE_REQUIRED: True}, "jobsearch": 3938, "historical": 3725},
    {"params": {REMOTE: True, EXPERIENCE_REQUIRED: True}, "jobsearch": 31, "historical": 17},
    pytest.param({"params": {REMOTE: False, EXPERIENCE_REQUIRED: True}, "jobsearch": 3907, "historical": 3708},
                 marks=pytest.mark.smoke),
    {"params": {REMOTE: False, EXPERIENCE_REQUIRED: False}, "jobsearch": 1086, "historical": 1228}
])
def test_experience(test_case):
    """
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: True, 'q': '-Stockholm'}, "jobsearch": 23, "historical": 25},
    {"params": {REMOTE: True, MUNICIPALITY: f"-{geo.stockholm}"}, "jobsearch": 23, "historical": 25},
    pytest.param(
        {"params": {REMOTE: True, 'region': f"-{geo.vastra_gotalands_lan}"}, "jobsearch": 33, "historical": 29},
        marks=pytest.mark.smoke),
    {"params": {REMOTE: True, 'region': f"-{geo.skane_lan}"}, "jobsearch": 31, "historical": 32},
    {"params": {REMOTE: True, 'region': f"-{geo.norrbottens_lan}"}, "jobsearch": 36, "historical": 34},
])
def test_remote_negative_geography(test_case):
    """
    Negative geographical parameters
    AND condition between REMOTE and other params
    """
    run_test_case(test_case)


def test_combination_municipality():
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total({'municipality': geo.stockholm})
    number_remote = get_total({REMOTE: True, 'municipality': geo.stockholm})
    number_not_remote = get_total({REMOTE: False, 'municipality': geo.stockholm})
    assert number_remote + number_not_remote == number_region


def test_combination_region():
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total({'region': geo.stockholms_lan})
    number_remote = get_total({REMOTE: True, 'region': geo.stockholms_lan})
    number_not_remote = get_total({REMOTE: False, 'region': geo.stockholms_lan})
    assert number_remote + number_not_remote == number_region


# noinspection PyDictDuplicateKeys
# (duplicate dictionary keys are intentional for testing)

@pytest.mark.parametrize("test_case", [
    {"params": {REMOTE: False, REMOTE: True}, "jobsearch": NUMBER_OF_REMOTE_ADS, "historical": 34},  # noqa F602
    {"params": {REMOTE: True, REMOTE: False}, "jobsearch": (NUMBER_OF_ADS - NUMBER_OF_REMOTE_ADS),  # noqa F602
     "historical": 17716},
    {"params": {REMOTE: True, REMOTE: False, REMOTE: True}, "jobsearch": NUMBER_OF_REMOTE_ADS, "historical": 34} # noqa F602


])
def test_duplicate_remote_param(test_case):
    """
    with duplicated params, value of last param is used
     # noqa F602  disables check for repeated dictionary keys
    """
    run_test_case(test_case)
