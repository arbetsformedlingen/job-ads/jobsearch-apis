import logging
from datetime import datetime, timedelta
from flask import Response
from flask_restx import Resource

from common import constants
from stream.load_ads import load_snapshot, load_ads_for_stream
from stream.api import ns_stream, stream_query, snapshot_query

log = logging.getLogger(__name__)


@ns_stream.route('feed')
class StreamLoad(Resource):
    example_date = (datetime.now() - timedelta(minutes=10)).strftime(constants.DATE_FORMAT)

    @ns_stream.doc(
        description="Download all the ads that has been changed (e.g. added, updated, unpublished) during requested time interval as an Atom feed."
                    "if you try it out in a web browser, use a short date interval, otherwise the volume of data might cause your browser to hang.",
        params={
            constants.DATE: "Stream ads changed since datetime. "
                            "Accepts datetime as YYYY-MM-DDTHH:MM:SS, "
                            "for example %s." % example_date,
            constants.UPDATED_BEFORE_DATE: "Stream ads changed before datetime. "
                                           "Accepts datetime as YYYY-MM-DDTHH:MM:SS. "
                                           "Optional if you want to set a custom time span. "
                                           "Defaults to 'now' if not set.",
            constants.OCCUPATION_CONCEPT_ID: "Filter stream ads by one or more occupation concept ids "
                                             "(from occupation, occupation_group, occupation_field).",
            constants.LOCATION_CONCEPT_ID: "Filter stream ads by one or more location concept ids "
                                           "(from municipality_concept_id, region_concept_id, country_concept_id)."
        },
        responses={
            200: 'OK',
            500: 'Technical error'
        },
        content_type="application/xml"
    )
    @ns_stream.expect(stream_query)
    def get(self, **kwargs):
        args = stream_query.parse_args()
        log.info(f"ARGS: {args}")
        return Response(response=load_ads_for_stream(args, atom=True), status=200, mimetype='application/atom+xml')


@ns_stream.route('stream')
class StreamLoad(Resource):
    example_date = (datetime.now() - timedelta(minutes=10)).strftime(constants.DATE_FORMAT)

    @ns_stream.doc(
        description="Download all the ads that has been changed (e.g. added, updated, unpublished) during requested time interval."
                    "if you try it out in a web browser, use a short date interval, otherwise the volume of data might cause your browser to hang.",
        params={
            constants.DATE: "Stream ads changed since datetime. "
                            "Accepts datetime as YYYY-MM-DDTHH:MM:SS, "
                            "for example %s." % example_date,
            constants.UPDATED_BEFORE_DATE: "Stream ads changed before datetime. "
                                           "Accepts datetime as YYYY-MM-DDTHH:MM:SS. "
                                           "Optional if you want to set a custom time span. "
                                           "Defaults to 'now' if not set.",
            constants.OCCUPATION_CONCEPT_ID: "Filter stream ads by one or more occupation concept ids "
                                             "(from occupation, occupation_group, occupation_field).",
            constants.LOCATION_CONCEPT_ID: "Filter stream ads by one or more location concept ids "
                                           "(from municipality_concept_id, region_concept_id, country_concept_id)."
        },
        responses={
            200: 'OK',
            500: 'Technical error'
        }
    )
    @ns_stream.expect(stream_query)
    def get(self, **kwargs):
        args = stream_query.parse_args()
        log.info(f"ARGS: {args}")
        return Response(load_ads_for_stream(args), mimetype='application/json')


@ns_stream.route('snapshot')
class SnapshotLoad(Resource):
    @ns_stream.doc(
        description="Download all the ads currently published in Platsbanken. "
                    "The intended usage for this endpoint is to make an initial copy of the job ad dataset "
                    "and then use the stream endpoint to keep it up to date. "
                    "Avoid using in Swagger, your browser will probably hang",
        responses={
            200: 'OK',
            500: 'Technical error'
        }
    )
    @ns_stream.expect(snapshot_query)
    def get(self, **kwargs):
        args = snapshot_query.parse_args()
        log.info(f"ARGS: {args}")
        return Response(load_snapshot(), mimetype='application/json')
