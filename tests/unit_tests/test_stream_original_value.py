import pytest

from common.ad_formatter import get_original_value_from_list

none_response = {'concept_id': None, 'label': None, 'legacy_ams_taxonomy_id': None}


def test_none():
    empty_list = []
    assert get_original_value_from_list(empty_list) == none_response


@pytest.mark.parametrize("value", ['ABC123', ['ABC123']])
def test_dict(value):
    data = {'concept_id': value}
    assert get_original_value_from_list(data) == {'concept_id': value, 'label': None, 'legacy_ams_taxonomy_id': None}


def test_list_no_original():
    value = 'ABC123'
    data = [{'concept_id': value,
             'label': 'my_label',
             'legacy_ams_taxonomy_id': 'my_legacy_id'}]
    assert get_original_value_from_list(data) is None


def test_list_original():
    data = [{'concept_id': 'ABC123',
             'label': 'my_label',
             'legacy_ams_taxonomy_id': 'my_legacy_id'}]

    data[0]['original_value'] = True
    result = get_original_value_from_list(data)
    del data[0]['original_value']
    assert result == data[0]


def test_list_no_original_only_concept_id():
    data = [{'original_value': False,
             'concept_id': 'ABC123',
             'label': 'my_label',
             'legacy_ams_taxonomy_id': 'my_legacy_id'}]
    assert get_original_value_from_list(data) is None


def test_list_multiple_entries_original():
    data = {'original_value': True,
            'concept_id': 'XYZ987',
            'label': 'my_label2',
            'legacy_ams_taxonomy_id': 'my_legacy_id2'}

    data_no_original = {'original_value': False,
                        'concept_id': 'ABC123',
                        'label': 'my_label',
                        'legacy_ams_taxonomy_id': 'my_legacy_id'}

    data_list = [data, data_no_original]

    result = get_original_value_from_list(data_list)
    del data['original_value']
    assert result == data


def test_list_multiple_entries_original_2():
    data = {'original_value': True,
            'concept_id': 'XYZ987',
            'label': 'my_label2',
            'legacy_ams_taxonomy_id': 'my_legacy_id2'}

    data_no_original_1 = {'original_value': False,
                          'concept_id': 'ABC123',
                          'label': 'my_label',
                          'legacy_ams_taxonomy_id': 'my_legacy_id'}

    data_no_original_2 = {'original_value': False,
                          'concept_id': 'QWERTY',
                          'label': 'my_label',
                          'legacy_ams_taxonomy_id': 'my_legacy_id'}

    data_list = [data_no_original_1, data_no_original_2, data]

    result = get_original_value_from_list(data_list)
    del data['original_value']
    assert result == data


def test_string():
    with pytest.raises(AttributeError):
        get_original_value_from_list("hello")
