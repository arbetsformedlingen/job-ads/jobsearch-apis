from copy import deepcopy
from stream.load_ads import format_ad

none_response = {'concept_id': None, 'label': None, 'legacy_ams_taxonomy_id': None}


def test_format_ad():
    original_ad = {'occupation': [
        {'concept_id': 'something', 'label': 'Personlig assistent', 'legacy_ams_taxonomy_id': '0000',
         'original_value': False},
        {'concept_id': 'eU1q_zvL_9Rf', 'label': 'Personlig assistent', 'legacy_ams_taxonomy_id': '5798',
         'original_value': True}

    ], 'occupation_group': [
        {'concept_id': 'sq3e_WVv_Fjd', 'label': 'Personliga assistenter', 'legacy_ams_taxonomy_id': '5343'}],
        'occupation_field': [
            {'concept_id': 'GazW_2TU_kJw', 'label': 'Yrken med social inriktning',
             'legacy_ams_taxonomy_id': '16'}],
    }
    ad_to_format = deepcopy(original_ad)

    result = format_ad(ad_to_format)
    del original_ad['occupation'][1]['original_value']
    assert result['occupation'] == original_ad['occupation'][1]
    assert result['occupation_group'] == original_ad['occupation_group'][0]
    assert result['occupation_group'] == original_ad['occupation_group'][0]


def test_format_ads_none_group_field():
    original_ad = {'occupation': [
        {'concept_id': 'something', 'label': 'Personlig assistent', 'legacy_ams_taxonomy_id': '0000',
         'original_value': False},
        {'concept_id': 'eU1q_zvL_9Rf', 'label': 'Personlig assistent', 'legacy_ams_taxonomy_id': '5798',
         'original_value': True}
    ]
    }
    ad_to_format = deepcopy(original_ad)

    result = format_ad(ad_to_format)
    del original_ad['occupation'][1]['original_value']

    assert result['occupation'] == original_ad['occupation'][1]
    assert result['occupation_group'] == none_response
    assert result['occupation_group'] == none_response


def test_format_ads_none_occupation():
    result = format_ad({'occupation': None})
    assert result['occupation'] == none_response
    assert result['occupation_group'] == none_response
    assert result['occupation_group'] == none_response
