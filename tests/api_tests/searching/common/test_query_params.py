import pytest

from tests.test_resources.api_test_runner import run_test_case

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize("test_case", [
    {'params': {'published-before': '2015-05-25T00:00:01'}, 'jobsearch': 0, 'historical': 6307},
    {'params': {'published-before': '2016-06-25T00:00:01'}, 'jobsearch': 0, 'historical': 7768},
    {'params': {'published-before': '2017-07-25T00:00:01'}, 'jobsearch': 0, 'historical': 9314},
    {'params': {'published-before': '2018-08-25T00:00:01'}, 'jobsearch': 0, 'historical': 10806},
    {'params': {'published-before': '2019-09-25T00:00:01'}, 'jobsearch': 0, 'historical': 12235},
    {'params': {'published-before': '2020-10-25T00:00:01'}, 'jobsearch': 18, 'historical': 13370},
    {'params': {'published-before': '2021-04-25T00:00:01'}, 'jobsearch': 5029, 'historical': 13947},
    {'params': {'published-before': '2023-02-20T15:08:27'}, 'jobsearch': 5029, 'historical': 17481},
    {'params': {'published-before': '1971-01-01T00:00:01'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'published-before': '2021-01-01T00:00:01'}, 'jobsearch': 158, 'historical': 13552},
    {'params': {'published-before': '2021-01-25T07:29:41'}, 'jobsearch': 304, 'historical': 13603},
    {'params': {'published-after': '2020-11-01T00:00:01'}, 'jobsearch': 5007, 'historical': 4366},
    {'params': {'published-after': '2020-12-01T00:00:01'}, 'jobsearch': 4964, 'historical': 4278},
    {'params': {'published-after': '2021-03-10T00:00:01'}, 'jobsearch': 2810, 'historical': 3995},
    {'params': {'published-after': '2021-03-22T00:00:01'}, 'jobsearch': 892, 'historical': 3951},
    {'params': {'published-after': '1971-01-01T00:00:01'}, 'jobsearch': 5029, 'historical': 17750},
    {'params': {'published-after': '2023-02-20T15:08:27'}, 'jobsearch': 0, 'historical': 269},
    {'params': {'published-after': '2020-12-15T00:00:01', 'published-before': '2020-12-20T00:00:01'}, 'jobsearch': 20, 'historical': 24},
    {'params': {'published-after': '2020-12-01T00:00:01', 'published-before': '2020-12-10T00:00:01'}, 'jobsearch': 26, 'historical': 27},
    {'params': {'published-after': '2020-12-11T00:00:01', 'published-before': '2020-12-15T00:00:01'}, 'jobsearch': 8, 'historical': 9},
    {'params': {'published-after': '2023-02-20T15:08:27', 'published-before': '1971-01-01T00:00:01'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'published-after': '2016-02-20T15:08:27', 'published-before': '2017-01-01T00:00:01'}, 'jobsearch': 0, 'historical': 1249},
    {'params': {'published-after': '2018-02-20T15:08:27', 'published-before': '2020-08-01T00:00:01'}, 'jobsearch': 0, 'historical': 3058},
    {'params': {'published-after': '2017-02-20T15:08:27', 'published-before': '2017-05-01T00:00:01'}, 'jobsearch': 0, 'historical': 312},
    {'params': {'published-before': '2023-05-16T12:02:11'}, 'jobsearch': 5029, 'historical': 17750},

])
def test_publication_date(test_case):
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    {'params': {'parttime.min': 80}, 'jobsearch': 3914, 'historical': 3521},
    {'params': {'parttime.min': 20}, 'jobsearch': 4208, 'historical': 3718},
    {'params': {'parttime.max': 80}, 'jobsearch': 103, 'historical': 78},
    {'params': {'parttime.max': 20}, 'jobsearch': 4, 'historical': 15},
])
def test_query_params_part_time(test_case):
    """
    Test 'parttime.min' and 'parttime.max' query parameters
    """
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    {'params': {'position': '59.3,18.0'}, 'jobsearch': 65, 'historical': 87},
    {'params': {'position': '59.3,18.0', 'position.radius': 6}, 'jobsearch': 718, 'historical': 778},
    {'params': {'position': '59.3,18.0', 'position.radius': 50}, 'jobsearch': 1176, 'historical': 1319},
    {'params': {'position': '59.3,18.0', 'position.radius': 100}, 'jobsearch': 1500, 'historical': 1677},
    {'params': {'position': '56.9,12.5', 'position.radius': 50}, 'jobsearch': 104, 'historical': 97},
    {'params': {'position': '18.0,59.3'}, 'jobsearch': 0, 'historical': 0}, # lat long reversed
])
def test_query_params_geo_position(test_case):
    """
    Test 'position' query parameter along with 'position-radius'
    With larger radius, more hits are returned
    """
    run_test_case(test_case)


@pytest.mark.parametrize('test_case', [
    {'params': {'employer': 'västra götalandsregionen'}, 'jobsearch': 56, 'historical': 135},
    {'params': {'employer': 'Jobtech'}, 'jobsearch': 0, 'historical': 0},
    {'params': {'employer': 'Region Stockholm'}, 'jobsearch': 372, 'historical': 798},
    {'params': {'employer': 'City Dental i Stockholm AB'}, 'jobsearch': 3681, 'historical': 10378},
    {'params': {'employer': 'Premier Service Sverige AB'}, 'jobsearch': 3633, 'historical': 10149},
    {'params': {'employer': 'Smartbear Sweden AB'}, 'jobsearch': 3613, 'historical': 10062},
    {'params': {'employer': 'Göteborgs Universitet'}, 'jobsearch': 89, 'historical': 311},
    {'params': {'employer': 'Blekinge Läns Landsting'}, 'jobsearch': 13, 'historical': 317},
    {'params': {'employer': '"Skåne Läns Landsting"'}, 'jobsearch': 72, 'historical': 431},  # quoted string
])
def test_query_params_employer(test_case):
    """
    This test return too many hits
    it will return hits where company name has one of the words in the employer name (e.g. 'Sverige')
    keeping it to document current behavior
    """
    run_test_case(test_case)
