import json
import pytest
import requests
import common.constants
from tests.test_resources.test_settings import TEST_URL


@pytest.mark.historical
def test_search_swagger_historical():
    """
    Test Swagger info for historical ads
    """
    response = requests.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    response_json = json.loads(response.content)
    assert response_json['info']['version'] == common.constants.API_VERSION

    expected = [('/ad/{id}', 2), ('/search', 1), ('/stats', 1)]
    assert len(response_json['paths']) == len(expected)
    for expected_path, expected_number in expected:
        assert response_json['paths'][expected_path]
        assert len(response_json['paths'][expected_path]['get']['responses']) == expected_number
