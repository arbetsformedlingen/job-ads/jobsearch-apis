import json
import logging
import time

import jmespath
import pytest

from common import constants
from search.common_search.complete import _check_search_word_type
from search.common_search.complete import suggest
from search.common_search.querybuilder import QueryBuilder, QueryBuilderType
from search.rest.endpoint.common import QueryBuilderContainer
from search.rest.endpoint.jobsearch import Complete

log = logging.getLogger(__name__)

@pytest.fixture(scope="module")
def querybuilder_jobsearch():
    querybuilder_container = QueryBuilderContainer()
    querybuilder_container.launch()
    return querybuilder_container.get(QueryBuilderType.JOBSEARCH_COMPLETE)


@pytest.mark.parametrize("search_text, expected_result", [
    ('', None),
    ('stockholm', 'location'),
    ('"stockholm', None),
    ('städare &', None),  # could be improved to return 'occupation', but this is what we get now (and no 400)
    ('rekryteringsutbildning', 'skill')
])
def test_check_search_word_type_nar_862_and_1107(caplog, search_text, expected_result):
    """
    Tests for search word type
    """
    result = _check_search_word_type(search_text, QueryBuilder())
    log_levels = [tup[1] for tup in caplog.record_tuples]
    assert logging.ERROR not in log_levels, "Logging error from elastic call"
    assert logging.WARNING not in log_levels, "Logging warning from elastic call"
    assert result == expected_result



@pytest.mark.parametrize("search_text, expected_result", [
    ('java pytho', 'java python'),
])
@pytest.mark.integration
def test_complete_for_enriched_terms(querybuilder_jobsearch, search_text, expected_result):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    result = suggest(args, querybuilder_jobsearch)
    marshalled_result = Complete.marshal_results(result, 100, start_time)

    assert jmespath.search('typeahead[0].value', marshalled_result) == expected_result

LABEL_NYSTARTSJOBB = 'NYSTARTSJOBB'.lower()
LABEL_REKRYTERINGSUTBILDNING = 'REKRYTERINGSUTBILDNING'.lower()
LABEL_DUMMY = 'DUMMY_LABEL'.lower()
LABEL_SWEDISH_CHARS = 'RÄKSMÖRGÅS'.lower()
LABEL_ANYCASE = 'wEirD_CaSE_labEL'.lower()

@pytest.mark.parametrize("search_text, expected_result", [
    ('java nystartsjob', f'java {LABEL_NYSTARTSJOBB}'),
    ('java nysta', f'java {LABEL_NYSTARTSJOBB}'),
    ('java nystarts', f'java {LABEL_NYSTARTSJOBB}'),
    ('java rekryterings', f'java {LABEL_REKRYTERINGSUTBILDNING}'),
    ('java dummy_la', f'java {LABEL_DUMMY}'),
    ('java räksm', f'java {LABEL_SWEDISH_CHARS}'),
    ('java weird_', f'java {LABEL_ANYCASE}'),
    ('nystartsjob', f'{LABEL_NYSTARTSJOBB}'),

])
@pytest.mark.integration
def test_complete_for_labels(querybuilder_jobsearch, search_text, expected_result):
    start_time = int(time.time() * 1000)
    args = _create_args_according_to_endpoint(search_text)
    result = suggest(args, querybuilder_jobsearch)

    assert result
    assert 'aggs' in result

    marshalled_result = Complete.marshal_results(result, 100, start_time)
    log.info(f"\n\nmarshalled_result: {json.dumps(marshalled_result, indent=4)}")

    typeahead = jmespath.search('typeahead', marshalled_result)

    expected_value_found = False

    for typeahead_item in typeahead:
        if typeahead_item['value'] == expected_result:
            expected_value_found = True
            break

    assert expected_value_found, f"Expected value {expected_result} not found in typeahead result: {typeahead}"


def _create_args_according_to_endpoint(input_terms):
    args = {}
    args[constants.FREETEXT_QUERY] = input_terms
    word_list, space_last = Complete.typeahead_query_analyzer(args.get(constants.FREETEXT_QUERY))
    prefix_words = word_list if space_last else word_list[:-1]
    args[constants.TYPEAHEAD_QUERY] = ' '.join(word_list) + (' ' if space_last else '')  # used for aggs queries
    args[constants.FREETEXT_QUERY] = ' '.join(prefix_words)  # used for subset selections (if contextual==True)
    return args