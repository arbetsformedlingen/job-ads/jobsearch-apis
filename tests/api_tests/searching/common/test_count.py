import pytest
from tests.test_resources.api_test_runner import run_test_case

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.smoke
@pytest.mark.parametrize(
    "test_case",
    [
        {"params": {}, "jobsearch": 5029, "historical": 17750},
        {"params": {"q": "gymnasielärare"}, "jobsearch": 62, "historical": 132},
        {"params": {"q": "lärare"}, "jobsearch": 250, "historical": 879},
        {"params": {"q": "C#"}, "jobsearch": 52, "historical": 94},
        {"params": {"q": "c-körkort"}, "jobsearch": 30, "historical": 63},
        {"params": {"q": ".net"}, "jobsearch": 40, "historical": 82},
        {"params": {"q": "ci/cd"}, "jobsearch": 32, "historical": 12},
        {"params": {"q": "erp-system"}, "jobsearch": 7, "historical": 11},
        {"params": {"q": "tcp/ip"}, "jobsearch": 8, "historical": 17},
        {"params": {"q": "cad-verktyg"}, "jobsearch": 1, "historical": 5},
        {"params": {"q": "backend-utvecklare"}, "jobsearch": 19, "historical": 13},
        {"params": {"q": "it-tekniker"}, "jobsearch": 24, "historical": 53},
        pytest.param({"params": {"q": "sjuksköterska"}, "jobsearch": 494, "historical": 869}, marks=pytest.mark.smoke),
        {"params": {"q": "sjuksköterskor"}, "jobsearch": 494, "historical": 864},
        {"params": {"q": "körkort"}, "jobsearch": 1070, "historical": 3032},
        {"params": {"q": "livsmedel"}, "jobsearch": 22, "historical": 61},
        {"params": {"q": "sköterska"}, "jobsearch": 1, "historical": 6},
        {"params": {"q": "undersköterska"}, "jobsearch": 126, "historical": 367},
        {"params": {"q": "försäkring"}, "jobsearch": 65, "historical": 215},
        {"params": {"q": "kock"}, "jobsearch": 83, "historical": 288},
        {"params": {"q": "uppsala"}, "jobsearch": 123, "historical": 417},
        {"params": {"q": "pizzabagare"}, "jobsearch": 19, "historical": 57},
        {"params": {"q": "personlig assistent"}, "jobsearch": 196, "historical": 626},
        {"params": {"q": "förskollärare"}, "jobsearch": 56, "historical": 376},
        {"params": {"q": "python"}, "jobsearch": 97, "historical": 73},
        {"params": {"q": "barnmorska tandsköterska"}, "jobsearch": 37, "historical": 82},
        pytest.param(
            {"params": {"q": "barnmorska fysioterapeut"}, "jobsearch": 49, "historical": 95}, marks=pytest.mark.smoke
        ),
        {"params": {"q": "chefsbarnmorska barnmorska"}, "jobsearch": 17, "historical": 36},
        {"params": {"q": "bartender"}, "jobsearch": 4, "historical": 32},
        {"params": {"q": "personlig assistent +göteborg"}, "jobsearch": 11, "historical": 37},
        {"params": {"q": "förskollärare"}, "jobsearch": 56, "historical": 376},
        {"params": {"q": "sjuksköterska -stockholm"}, "jobsearch": 431, "historical": 735},
        pytest.param(
            {"params": {"municipality": ["AvNB_uwa_6n6", "oYPt_yRA_Smm"]}, "jobsearch": 983, "historical": 1058},
            marks=pytest.mark.smoke,
        ),
        {"params": {"municipality": "QiGt_BLu_amP"}, "jobsearch": 61, "historical": 62},
        {"params": {"municipality": "PVZL_BQT_XtL", "region": "CifL_Rzy_Mku"}, "jobsearch": 1593, "historical": 1756},
        {
            "params": {"municipality": "PVZL_BQT_XtL", "region": "CifL_Rzy_Mku", "country": "i46j_HmG_v64"},
            "jobsearch": 4999,
            "historical": 17728,
        },
        {"params": {"region": "CifL_Rzy_Mku"}, "jobsearch": 1227, "historical": 1396},
        {"params": {"country": "QJgN_Zge_BzJ"}, "jobsearch": 10, "historical": 11},
        {"params": {"occupation-group": "Z8ci_bBE_tmx"}, "jobsearch": 396, "historical": 363},
        pytest.param(
            {"params": {"occupation-field": "ASGV_zcE_bWf"}, "jobsearch": 175, "historical": 235},
            marks=pytest.mark.smoke,
        ),
        {"params": {"q": "lärare", "published-after": "2019-01-16T07:29:52"}, "jobsearch": 250, "historical": 213},
        {"params": {"q": "Kundtjänstmedarbetare", "sort": "pubdate-asc"}, "jobsearch": 66, "historical": 224},
        {
            "params": {
                "q": "lärare",
                "published-after": "2019-01-16T07:29:52",
                "country": "i46j_HmG_v64",
                "sort": "pubdate-asc",
            },
            "jobsearch": 250,
            "historical": 213,
        },
        {"params": {"q": "lärare", "region": ["CifL_Rzy_Mku", "CaRE_1nn_cSU"]}, "jobsearch": 65, "historical": 52},
        {"params": {"q": "lärare", "region": "CifL_Rzy_Mku"}, "jobsearch": 36, "historical": 37},
        {"params": {"q": "lärare", "municipality": "8deT_FRF_2SP"}, "jobsearch": 1, "historical": 4},
        pytest.param(
            {"params": {"employment-type": "PFZr_Syz_cUq"}, "jobsearch": 4389, "historical": 4595},
            marks=pytest.mark.smoke,
        ),
        {"params": {"employment-type": "kpPX_CNN_gDU"}, "jobsearch": 4389, "historical": 0},
        {"params": {"employment-type": "EBhX_Qm2_8eX"}, "jobsearch": 360, "historical": 0},
        {"params": {"experience": True}, "jobsearch": 3938, "historical": 3725},
        {"params": {"experience": False}, "jobsearch": 1091, "historical": 1243},
        {"params": {"remote": True}, "jobsearch": 36, "historical": 34},
        {"params": {"remote": True, "municipality": "AvNB_uwa_6n6"}, "jobsearch": 13, "historical": 9},
        {"params": {"remote": True, "region": "CifL_Rzy_Mku"}, "jobsearch": 14, "historical": 9},
        {"params": {"remote": True, "abroad": True}, "jobsearch": 0, "historical": 0},
        {"params": {"remote": True, "experience": True}, "jobsearch": 31, "historical": 17},
        {"params": {"remote": True, "occupation-group": "DJh5_yyF_hEM"}, "jobsearch": 4, "historical": 1},
        {"params": {"remote": True, "occupation-field": "RPTn_bxG_ExZ"}, "jobsearch": 5, "historical": 15},
        {"params": {"remote": True, "q": "Stockholms Län"}, "jobsearch": 14, "historical": 9},
        pytest.param(
            {"params": {"remote": True, "q": "Stockholm"}, "jobsearch": 13, "historical": 9}, marks=pytest.mark.smoke
        ),
        {"params": {"remote": True, "q": "utvecklare"}, "jobsearch": 2, "historical": 1},
        {"params": {"region": "90"}, "jobsearch": 0, "historical": 17},
        {"params": {"region": "09"}, "jobsearch": 32, "historical": 18},
        {"params": {"unspecified-sweden-workplace": True}, "jobsearch": 135, "historical": 12852},
        {"params": {"region": "90", "unspecified-sweden-workplace": True}, "jobsearch": 135, "historical": 12858},
        {"params": {"country": "219"}, "jobsearch": 0, "historical": 0},
        {"params": {"region": "90", "country": "219"}, "jobsearch": 0, "historical": 17},
        {"params": {"unspecified-sweden-workplace": True, "country": "-199"}, "jobsearch": 0, "historical": 0},
    ],
)
def test_count(test_case):
    run_test_case(test_case)
