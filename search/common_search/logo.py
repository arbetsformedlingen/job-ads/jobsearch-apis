import logging
import io
import os
import requests
from flask import send_file
from werkzeug.exceptions import ServiceUnavailable
from common import settings
from search.common_search.ad_search import fetch_ad_by_id

log = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logo_file = None


def get_correct_logo_url(ad_id):
    ad = fetch_ad_by_id(ad_id)

    logo_url = None

    if ad and 'employer' in ad:
        if 'workplace_id' in ad['employer'] and ad['employer']['workplace_id'] and int(
                ad['employer']['workplace_id']) > 0:
            '''
            Special logo for workplace_id for ads with source_type
            VIA_AF_FORMULAR or VIA_PLATSBANKEN_AD or VIA_ANNONSERA
            (workplace_id > 0)
            '''
            workplace_id = ad['employer']['workplace_id']
            eventual_logo_url = f'{settings.COMPANY_LOGO_BASE_URL}arbetsplatser/{workplace_id}/logotyper/logo.png'
            logo_url = get_logo_url_if_available(eventual_logo_url)
        elif 'organization_number' in ad['employer'] and ad['employer']['organization_number']:
            org_number = ad['employer']['organization_number']
            eventual_logo_url = f'{settings.COMPANY_LOGO_BASE_URL}organisation/{org_number}/logotyper/logo.png'
            logo_url = get_logo_url_if_available(eventual_logo_url)
    return logo_url


def get_logo_url_if_available(eventual_logo_url):
    """
    This function makes a call to the logo endpoint to check response
    Download (if response OK to request) is made later
    """
    try:
        r = requests.head(eventual_logo_url, timeout=10)
    except requests.RequestException as e:
        """
        If there is an exception (requests.RequestException is base for several different exceptions),
        log it and raise http 503 "Service Unavailable"
        """
        log.error(f"Error for eventual logo url {eventual_logo_url}: {e}")
        raise ServiceUnavailable("Error getting logo")
    else:
        if r.status_code == 200:
            log.info(f"Eventual logo url {eventual_logo_url}")
            return eventual_logo_url
        else:  # Status code is not an error, but not 200/OK
            log.warning(f"Http status code for {eventual_logo_url} was {r.status_code} ")
            return None


def get_local_file(file_path):
    global logo_file
    if logo_file is None:
        logo_filepath = current_dir + file_path
        log.debug(f'Opening global file: {logo_filepath}')
        try:
            logo_file = open(logo_filepath, 'rb')
            logo_file = logo_file.read()
        except FileNotFoundError:
            """
            If there is an exception (FileNotFoundError), log it and raise FileNotFoundError.
            """
            log.warning(f"Error getting default logo from: {logo_filepath}")
            raise FileNotFoundError(f'{"Error getting default logo"}')
    return logo_file


def fetch_ad_logo(ad_id):
    if settings.COMPANY_LOGO_FETCH_DISABLED: 
        logo_url = None
    else:
        logo_url = get_correct_logo_url(ad_id)

    if logo_url is None: 
        log.info("Logo url not found, sending empty image")
        file_path = "../resources/1x1-00000000.png"
        return file_formatter(get_local_file(file_path))
    else:
        try:
            r = requests.get(logo_url, stream=True, timeout=5)
        except requests.RequestException as e:
            """
            If there is an exception (requests.RequestException is base for several different exceptions),
            log it and raise http 503 "Service Unavailable"
            """
            log.error(f"Error for logo url {logo_url}: {e}")
            raise ServiceUnavailable(f'Error getting logo')
        if r.status_code != 200:
            log.warning(f"Status code {r.status_code} for logo url: {logo_url}")
            r.raise_for_status()
        log.info(f"Logo found on: {logo_url}")
        return file_formatter(r.raw.read(decode_content=False))


def file_formatter(file_object):
    return send_file(
        io.BytesIO(file_object),
        download_name='logo.png',
        mimetype='image/png'
    )
