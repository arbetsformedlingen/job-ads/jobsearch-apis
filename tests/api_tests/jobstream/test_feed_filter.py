import pytest

from common.constants import UPDATED_BEFORE_DATE, OCCUPATION_CONCEPT_ID, LOCATION_CONCEPT_ID
from tests.test_resources.test_settings import NUMBER_OF_ADS, DAWN_OF_TIME, current_time_stamp

import tests.test_resources.concept_ids.concept_ids_geo as geo
import tests.test_resources.concept_ids.occupation as work
import tests.test_resources.concept_ids.occupation_group as group
import tests.test_resources.concept_ids.occupation_field as field
from tests.test_resources.helper import get_feed, validate_feed, validate_feed_header, validate_feed_single_entry

# mark all tests in this file as @pytest.mark.jobstream
pytestmark = pytest.mark.jobstream


@pytest.mark.parametrize('date, work, expected_number_of_hits', [
    (DAWN_OF_TIME, group.arbetsformedlare, 22),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, 239),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, 50),
    (DAWN_OF_TIME, work.arbetsterapeut, 28)])
def test_filter_only_on_occupation(date, work, expected_number_of_hits):
    """
    Returns number of hits in the db. Temporary to verify results in other tests
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work}
    xml_str = get_feed(params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.norge, 10),
    (DAWN_OF_TIME, geo.kalmar_lan, 140),
    (DAWN_OF_TIME, geo.botkyrka, 20),
    (DAWN_OF_TIME, geo.sverige, 4999)])
def test_filter_only_on_location(date, geo, expected_number_of_hits):
    """
    Returns number of hits in the db. Temporary to verify results in other tests
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo}
    xml_str = get_feed(params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, work.larare_i_fritidshem_fritidspedagog, geo.vastra_gotalands_lan, 5),
    ('2020-12-15T00:00:01', work.larare_i_fritidshem_fritidspedagog, geo.vastra_gotalands_lan, 5),
    ('2020-12-01T00:00:01', work.bussforare_busschauffor, geo.sverige, 3),
    ('2020-12-01T00:00:01', work.larare_i_grundskolan__arskurs_7_9, geo.stockholms_lan, 10),
    (DAWN_OF_TIME, group.grundutbildade_sjukskoterskor, geo.sverige, 393),
    ('2020-11-01T00:00:01', group.mjukvaru__och_systemutvecklare_m_fl_, geo.sverige, 236),
    (DAWN_OF_TIME, field.militart_arbete, geo.sverige, 5),
    (DAWN_OF_TIME, field.halso__och_sjukvard, geo.stockholms_lan, 195),
    ('2020-11-25T00:00:01', field.halso__och_sjukvard, geo.sverige, 958),
    ('2020-12-15T00:00:01', field.halso__och_sjukvard, geo.stockholms_lan, 192),
])
def test_filter_with_date_and_occupation_and_location(date, work, geo,
                                                      expected_number_of_hits):
    """
    should return results based on date AND occupation type AND location
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work, LOCATION_CONCEPT_ID: geo}
    xml_str = get_feed(params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, concept_id, expected_number_of_hits', [
    (DAWN_OF_TIME, work.personlig_assistent, 181),
    ('2020-11-01T00:00:01', work.personlig_assistent, 181),
    ('2021-01-01T00:00:01', work.personlig_assistent, 177),
    ('2021-03-01T00:00:01', work.personlig_assistent, 122),
    (DAWN_OF_TIME, work.kassapersonal, 7),
    ('2020-12-01T00:00:01', work.kassapersonal, 7),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, 50),
    ('2020-10-25T00:00:00', work.mjukvaruutvecklare, 49),
    (DAWN_OF_TIME, group.arbetsformedlare, 22),
    ('2021-02-25T00:00:00', group.arbetsformedlare, 19),
    (DAWN_OF_TIME, field.militart_arbete, 5),
    (DAWN_OF_TIME, field.socialt_arbete, 347),
    (DAWN_OF_TIME, work.administrativ_chef, 4),
    (DAWN_OF_TIME, work.account_manager, 42),
    (DAWN_OF_TIME, work.cykelbud, 2),
])
def test_filter_with_date_and_one_occupation(date, concept_id, expected_number_of_hits):
    """
    test of filtering in /stream: should return results based on date AND occupation-related concept_id
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: concept_id}
    xml_str = get_feed(params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.falun, 33),
    (DAWN_OF_TIME, geo.stockholms_lan, 1227),
    ('2021-03-05T00:00:01', geo.stockholms_lan, 832),
    (DAWN_OF_TIME, geo.norge, 10),
    ('2021-01-01T00:00:01', geo.linkoping, 103),
    (DAWN_OF_TIME, geo.sverige, 4999),
    ('2020-11-01T00:00:01', geo.sverige, 4981),
    ('2020-12-01T00:00:01', geo.stockholm, 762),
    ('2021-02-01T00:00:01', geo.norge, 8),
    ('2021-02-15T00:00:01', geo.dalarnas_lan, 99),
])
def test_filter_with_date_and_location( date, geo, expected_number_of_hits):
    """
    should return results based on date AND occupation type AND location_1
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


#   multiple params of same type

@pytest.mark.parametrize('date, work_1, work_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.account_manager, work.cykelbud, 44),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, group.arbetsformedlare, 72),
    (DAWN_OF_TIME, work.cykelbud, work.account_manager, 44),
    ('2020-12-01T00:00:01', work.mjukvaruutvecklare, group.arbetsformedlare, 69),
    (DAWN_OF_TIME, work.administrativ_chef, field.militart_arbete, 9),
    (DAWN_OF_TIME, field.militart_arbete, work.administrativ_chef, 9),
    ('2020-11-01T00:00:01', group.bagare_och_konditorer, group.bartendrar, 6),
    (DAWN_OF_TIME, group.bagare_och_konditorer, group.bartendrar, 6),
    ('2020-12-12T00:00:01', group.apotekare, field.data_it, 412),
    ('2021-01-25T00:00:00', group.frisorer, work.akupunktor, 11),
    ('2020-11-25T00:00:00', field.pedagogiskt_arbete, field.halso__och_sjukvard, 1488),
    ('2020-12-15T00:00:00', field.hantverksyrken, group.hudterapeuter, 21),
    ('2020-11-25T00:00:00', field.socialt_arbete, work.databasutvecklare, 349)])
def test_filter_with_date_and_two_occupations( date, work_1, work_2,
                                              expected_number_of_hits):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on both date AND (work_1 OR work_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2]}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work_1, work_2, work_3, expected_number_of_hits', [
    (DAWN_OF_TIME, work.account_manager, work.cykelbud, work.databasutvecklare, 46),
    ('2020-11-01T00:00:01', work.mjukvaruutvecklare, work.databasutvecklare, group.arbetsformedlare, 73),
    ('2020-12-01T00:00:01', work.administrativ_chef, work.apotekschef, field.militart_arbete, 10),
    ('2020-11-01T00:00:01', group.bagare_och_konditorer, group.bartendrar, group.hovmastare_och_servitorer, 36),
    ('2020-11-25T00:00:00', group.frisorer, group.hudterapeuter, work.akupunktor, 16),
    (DAWN_OF_TIME, field.pedagogiskt_arbete, field.halso__och_sjukvard, field.kropps__och_skonhetsvard, 1522),
    ('2020-11-25T00:00:00', field.pedagogiskt_arbete, field.halso__och_sjukvard, field.kropps__och_skonhetsvard, 1512),
    (DAWN_OF_TIME, field.hantverksyrken, field.data_it, group.hudterapeuter, 443),
    (DAWN_OF_TIME, field.socialt_arbete, field.bygg_och_anlaggning, work.databasutvecklare, 540),
    ('2020-11-25T00:00:00', field.socialt_arbete, field.bygg_och_anlaggning, work.databasutvecklare, 538)
])
def test_filter_with_date_and_three_occupations( date, work_1, work_2, work_3,
                                                expected_number_of_hits):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on date AND (work_1 OR work_2 OR work 3)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2, work_3]}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.smoke
@pytest.mark.parametrize('date, work_1, work_2, geo_1, expected_number_of_hits', [
    (DAWN_OF_TIME, work.arbetsterapeut, group.apotekare, geo.vastra_gotalands_lan, 7),
    (DAWN_OF_TIME, work.arbetsterapeut, field.kropps__och_skonhetsvard, geo.sverige, 53),
    (DAWN_OF_TIME, group.grundutbildade_sjukskoterskor, group.grundutbildade_sjukskoterskor, geo.sverige, 393),
    (DAWN_OF_TIME, group.apotekare, group.hovmastare_och_servitorer, geo.stockholms_lan, 5),
    (DAWN_OF_TIME, group.apotekare, group.arbetsformedlare, geo.sverige, 30),
    ('2020-12-01T00:00:01', field.militart_arbete, group.ambulanssjukskoterskor_m_fl_, geo.sverige, 15),
])
def test_filter_with_date_and_two_occupations_and_location( date, work_1, work_2, geo_1,
                                                           expected_number_of_hits):
    """
    should return results based on date AND location AND (work_1 OR work_2)
    results = work_1 + work_2 that matches location
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2], LOCATION_CONCEPT_ID: geo_1}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.arboga, geo.falun, 39),
    (DAWN_OF_TIME, geo.arboga, geo.stockholms_lan, 1233),
    (DAWN_OF_TIME, geo.arboga, geo.norge, 16),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.linkoping, 230),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.sverige, 4981),
    ('2020-12-01T00:00:01', geo.schweiz, geo.stockholm, 763),
    ('2020-12-01T00:00:01', geo.schweiz, geo.jonkopings_lan, 195),
    ('2020-12-01T00:00:01', geo.schweiz, geo.norge, 11)])
def test_filter_with_date_and_two_locations( date, geo_1, geo_2, expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, geo_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [geo.stockholms_lan], 1227),
    (DAWN_OF_TIME, [geo.stockholms_lan, geo.solna], 1227),
    (DAWN_OF_TIME, [geo.stockholms_lan, geo.stockholm, geo.botkyrka, geo.solna, geo.nacka], 1227)])
def test_filter_with_date_and_multiple_locations_in_same_region( date, geo_list, expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo_list}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [field.halso__och_sjukvard], 971),
    (DAWN_OF_TIME, [field.halso__och_sjukvard, work.sjukskoterska__grundutbildad], 971),
    (DAWN_OF_TIME, [field.halso__och_sjukvard, group.grundutbildade_sjukskoterskor], 971),
    (DAWN_OF_TIME,
     [field.halso__och_sjukvard, group.grundutbildade_sjukskoterskor, group.ambulanssjukskoterskor_m_fl_,
      work.sjukskoterska__medicin_och_kirurgi], 971)])
def test_filter_with_date_and_multiple_occupations_within_same_field( date, work_list,
                                                                     expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work_list}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [field.halso__och_sjukvard], 971),
    (DAWN_OF_TIME, [group.grundutbildade_sjukskoterskor], 396),
    (DAWN_OF_TIME, [work.sjukskoterska__grundutbildad], 392),
])
def test_filter_narrowing_down_occupations_within_same_field( date, work_list,
                                                             expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work_list}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.arbetsterapeut, geo.vastra_gotalands_lan, geo.ostergotlands_lan, 4),
    (DAWN_OF_TIME, group.civilingenjorsyrken_inom_bygg_och_anlaggning, geo.malta, geo.sverige, 19),
    ('2020-12-01T00:00:01', field.kultur__media__design, geo.schweiz, geo.stockholm, 10),
    ('2020-12-01T00:00:01', field.naturvetenskapligt_arbete, geo.schweiz, geo.stockholms_lan, 13),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, geo.vastra_gotalands_lan, geo.schweiz, 61),
])
def test_filter_with_date_and_occupation_and_two_locations( date, work, geo_1, geo_2,
                                                           expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work, LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('date, work_1, work_2, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.databasutvecklare, work.arbetsterapeut, geo.goteborg, geo.falun, 2),
    (DAWN_OF_TIME, work.databasutvecklare, work.sjukskoterska__grundutbildad, geo.arboga, geo.falun, 3),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, work.arbetsterapeut, geo.arboga,
     geo.stockholms_lan, 114),
    (DAWN_OF_TIME, work.sjukskoterska__grundutbildad, group.apotekare, geo.dalarnas_lan, geo.linkoping, 18),
    (DAWN_OF_TIME, work.barnsjukskoterska, group.apotekare, geo.malta, geo.sverige, 17),
    ('2020-12-01T00:00:01', work.eltekniker, field.kultur__media__design, geo.schweiz, geo.stockholm, 10),
    ('2020-12-01T00:00:01', group.grundutbildade_sjukskoterskor, field.bygg_och_anlaggning, geo.schweiz, geo.norge, 4),
    ('2020-12-01T00:00:01', field.halso__och_sjukvard, work.bussforare_busschauffor, geo.schweiz, geo.norge, 5)])
def test_filter_with_date_and_two_occupations_and_two_locations( date, work_1, work_2, geo_1, geo_2,
                                                                expected_number_of_hits):
    """
    should return results based on date AND (occupation 1 OR occupation 2) AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2], LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


# test below for comparison of number of hits for different dates
@pytest.mark.parametrize('date, expected_number_of_hits', [
    (DAWN_OF_TIME, NUMBER_OF_ADS),
    ('2021-01-01T00:00:01', 4904),
    ('2021-03-22T12:00:00', 844),
    ('2021-03-23T12:00:00', 542),
    ('2021-03-25T12:30:40', 6),
])
def test_filter_only_on_date( date, expected_number_of_hits):
    """
    Test basic stream with filtering on date (update after this date)
    """
    xml_str = get_feed( params={'date': date})
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize('from_date, to_date, expected_number_of_hits', [
    # 1 verify that results are the same as when only using a single date
    (DAWN_OF_TIME, current_time_stamp, NUMBER_OF_ADS),
    ('2020-12-14T00:00:00', '2021-03-30T00:00:00', 4941),
    ('2020-11-25T00:00:00', '2020-11-30T00:00:00', 11),
    ('2020-11-26T00:00:00', '2020-11-30T00:00:00', 6),
    ('2020-12-22T00:00:00', '2020-12-23T10:00:00', 4),
    (DAWN_OF_TIME, '2021-04-30T10:00:00', NUMBER_OF_ADS),
    (DAWN_OF_TIME, current_time_stamp, NUMBER_OF_ADS),
])
def test_filter_on_date_interval( from_date, to_date, expected_number_of_hits):
    """
    Test stream with filtering on date interval.
    """
    params = {'date': from_date, UPDATED_BEFORE_DATE: to_date}
    xml_str = get_feed( params)
    validate_feed(xml_str, expected_number_of_hits)


@pytest.mark.parametrize("params", [
    {'date': '1971-01-01T00:00:01', 'location-concept-id': 'bUNp_6HW_Vbn'},
    {'date': '1971-01-01T00:00:01', 'occupation-concept-id': 'DJh5_yyF_hEM', 'location-concept-id': 'Q78b_oCw_Yq2'},
    {'date': '2020-12-01T00:00:01', 'occupation-concept-id': 'bH5L_uXD_ZAX', 'location-concept-id': 'Q78b_oCw_Yq2'},
    {'date': '1971-01-01T00:00:01', 'occupation-concept-id': 'XeBP_nMe_pXx',
     'location-concept-id': ['oDpK_oZ2_WYt', 'wjee_qH2_yb6']}
    , {'date': '1971-01-01T00:00:01', 'occupation-concept-id': 'XeBP_nMe_pXx',
       'location-concept-id': ['oDpK_oZ2_WYt', 'bm2x_1mr_Qhx']}
    , {'date': '2020-12-01T00:00:01', 'occupation-concept-id': 'j7Cq_ZJe_GkT',
       'location-concept-id': ['Q78b_oCw_Yq2', 'QJgN_Zge_BzJ']}
    , {'date': '2020-02-01T00:00:01', 'occupation-concept-id': 'qSXj_aXc_EGp',
       'location-concept-id': ['Q78b_oCw_Yq2', 'QJgN_Zge_BzJ']}
    , {'date': '1971-01-01T00:00:01', 'occupation-concept-id': ['pAdE_uVY_JrR', 'WNVp_RYe_zLX'],
       'location-concept-id': ['oDpK_oZ2_WYt', 'wjee_qH2_yb6']}
    , {'date': '2020-12-01T00:00:01', 'occupation-concept-id': ['4KhP_FxL_uZ5', 'j7Cq_ZJe_GkT'],
       'location-concept-id': ['Q78b_oCw_Yq2', 'QJgN_Zge_BzJ']}
    , {'date': '1971-01-01T00:00:01', 'occupation-concept-id': ['xGr7_SGS_U3F', 'DJh5_yyF_hEM'],
       'location-concept-id': ['oDpK_oZ2_WYt', 'Q78b_oCw_Yq2']}
    , {'date': '2020-12-15T00:00:00', 'updated-before-date': '2020-12-16T00:00:00'},
    {'date': '2022-05-09T09:47:13', 'updated-before-date': '1971-01-01T00:00:01'},
    {'date': '2020-12-01T00:00:01', 'occupation-concept-id': ['bH5L_uXD_ZAX', 'qSXj_aXc_EGp'],
     'location-concept-id': 'QJgN_Zge_BzJ'},

])
def test_only_header( params):
    xml_str = get_feed( params)
    validate_feed_header(xml_str)


@pytest.mark.parametrize("params", [
    {'date': '1971-01-01T00:00:01', 'location-concept-id': 'u8qF_qpq_R5W'},
    {'date': '1971-01-01T00:00:01', 'location-concept-id': 'Q78b_oCw_Yq2'},
    {'date': '2020-12-01T00:00:01', 'location-concept-id': 'Q78b_oCw_Yq2'},
])
def test_single_entry( params):
    xml_str = get_feed( params)
    validate_feed_header(xml_str)
    validate_feed_single_entry(xml_str)
