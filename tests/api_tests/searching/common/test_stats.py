import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.helper import get_search, compare_multiple

# marks all tests as jobsearch
pytestmark = [pytest.mark.jobsearch]


@pytest.mark.smoke
@pytest.mark.parametrize("limit, stats_limit, expected_number_of_hits, expected_values", [
    (10, 0, 10, 5),
    (10, 10, 10, 10),
    (10, 15, 10, 15)
])
def test_stats( limit, stats_limit, expected_number_of_hits, expected_values):
    params = {'stats': 'region', 'offset': 0, 'limit': limit, 'stats.limit': stats_limit}
    results_json = get_search(params)
    compare_these = []
    compare_these.append((len(results_json['hits']), expected_number_of_hits, "wrong number of hits"))
    compare_these.append((len(results_json['stats'][0]['values']), expected_values, 'wrong stats.values'))
    compare_multiple(compare_these)


def test_stats_details():
    params = {'stats': 'region', 'offset': 0, 'limit': 0, 'stats.limit': 5}
    results_json = get_search(params)

    expected_results = [('Stockholms län', 1227, geo.stockholms_lan, '01'),
                        ('Västra Götalands län', 779, geo.vastra_gotalands_lan, '14'),
                        ('Skåne län', 643, geo.skane_lan, '12'),
                        ('Östergötlands län', 249, geo.ostergotlands_lan, '05'),
                        ('Jönköpings län', 195, geo.jonkopings_lan, '06'),
                        ('Uppsala län', 55, geo.uppsala_lan, '03'),
                        ]
    compare_these = []
    for index, result in enumerate(results_json['stats'][0]['values']):  # results should be sorted
        compare_these.append((result['term'], expected_results[index][0], 'wrong term'))
        compare_these.append((result['count'], expected_results[index][1], 'wrong count'))
        compare_these.append((result['concept_id'], expected_results[index][2], 'wrong concept_id'))
        compare_these.append((result['code'], expected_results[index][3], 'wrong code'))
    compare_multiple(compare_these)
