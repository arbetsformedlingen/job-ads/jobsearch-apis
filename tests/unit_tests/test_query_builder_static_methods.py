import pytest
from dateutil import parser
import datetime

from common import settings
from common.date_and_time_handling import format_start_date_historical, format_end_date_historical
from search.common_search.querybuilder import QueryBuilder
from tests.unit_tests.test_querybuilder import current_month_and_day


def test_bool_clause():
    query_dict = {'bool': {'must': [{'bool': {
        'should': [{'term': {'keywords.enriched.skill.raw': {'value': 'php', 'boost': 9}}},
                   {'term': {'keywords.enriched.skill.raw': {'value': 'python', 'boost': 9}}}]}},
        {'term': {'keywords.enriched.skill.raw': {'value': 'python', 'boost': 9}}}]}}
    freetext_headline = []
    query_string = '+python php'
    result = QueryBuilder._freetext_headline(query_dict, freetext_headline, query_string)
    expected = {'bool': {'must': [{'bool': {'should': [{'term': {'keywords.enriched.skill.raw': {'boost': 9,
                                                                                                 'value': 'php'}}},
                                                       {'term': {'keywords.enriched.skill.raw': {'boost': 9,
                                                                                                 'value': 'python'}}},
                                                       {'match': {'headline.words': {'boost': 5,
                                                                                     'operator': 'and',
                                                                                     'query': 'python '
                                                                                              'php'}}}]}},
                                  {'term': {'keywords.enriched.skill.raw': {'boost': 9,
                                                                            'value': 'python'}}}]}}
    assert result == expected


pytestmark = [pytest.mark.jobsearch, pytest.mark.jobstream, pytest.mark.historical, pytest.mark.integration,
              pytest.mark.unit]


@pytest.mark.parametrize("arg, expected", [
    (True, True),
    ('true', True),
    ('True', True),
    ('TrUe', True),
    (False, False),
    ('false', False),
    ('False', False),
    ('fAlSe', False),
    (None, None),
    ('wrong arg', 'wrong arg'),
    (123, 123),
])
def test_bool_parser(arg, expected):
    query_result = QueryBuilder._parse_boolean_arg(arg)
    assert query_result == expected


@pytest.mark.parametrize("query, expected", [
    ("[python3]", "\\[python3\\]"),
    ("python3", "python3"),
    ("asp.net", "asp\\.net"),
    ("c++", "c\\+\\+")
])
def test_rewrite_word_for_regex(query, expected):
    result = QueryBuilder._rewrite_word_for_regex(query)
    assert result == expected, f"got {result} but expected {expected}"


@pytest.mark.parametrize("querystring, expected", [('xx,', 'xx'),
                                                   ('xx.', 'xx.'),
                                                   ('xx!', 'xx'),
                                                   ('xx?', 'xx'),
                                                   ('yy:', 'yy'),
                                                   ('xx;', 'xx'),
                                                   ('.xx', '.xx'),
                                                   (',xx', 'xx'),
                                                   ('!xx', 'xx'),
                                                   ('?yy', 'yy'),
                                                   (':xx', 'xx'),
                                                   (';xx', 'xx'),
                                                   (';xx', 'xx'),
                                                   (' xx', 'xx'),
                                                   ('xx ', 'xx'),
                                                   ('y y', 'y y'),
                                                   ('x,x ', 'x x'),
                                                   ('x.x ', 'x.x'),
                                                   ('x!x ', 'x!x'),
                                                   ('x?x ', 'x?x'),
                                                   ('y:y ', 'y:y'),
                                                   ('x;x ', 'x;x'),
                                                   ('xx ', 'xx'),
                                                   ('x/y', 'x/y'),
                                                   ('.z/y', '.z/y'),
                                                   ('x/y.', 'x/y.'),
                                                   ('x / y.', 'x / y.'),
                                                   ('y,.!?:; x', 'y . x'),
                                                   ('x,y.z!1?2:3;4 x', 'x y.z!1?2:3;4 x'),
                                                   ('12345x', '12345x'),
                                                   ('.12345', '.12345'),
                                                   ('.12345.', '.12345.'),
                                                   ('.12345.', '.12345.'),
                                                   (',12345', '12345'),
                                                   (',12345,', '12345'),
                                                   ('\\x', '\\x'),
                                                   ('\\x,', '\\x'),
                                                   ('\\x.', '\\x.'),
                                                   ('\\.x.', '\\.x.'),
                                                   ('.\\.x.', '.\\.x.'),
                                                   (',\\.x.', '\\.x.'),
                                                   ])
def test_querystring_char_removal(querystring, expected):
    formatted = QueryBuilder.extract_quoted_phrases(querystring)
    assert formatted[1] == expected


@pytest.mark.parametrize("querystring, expected", [
    ('python "grym kodare"', 'python'),
    ('java "malmö stad"', 'java'),
    ('python -"grym kodare" +"i am lazy"', 'python'),
    ('"python på riktigt" -"grym kodare" +"i am lazy"', '')

])
def test_extract_querystring_phrases(querystring, expected):
    result = QueryBuilder.extract_quoted_phrases(querystring)[1]
    assert result == expected, f"got {result} but expected {expected}"


@pytest.mark.parametrize("querystring, expected", [
    ("\"i am lazy", ({'phrases': ['i am lazy'], 'phrases_must': [], 'phrases_must_not': []}, '')),
    ("python \"grym kodare\" \"i am lazy java",
     ({'phrases': ['grym kodare'], 'phrases_must': [], 'phrases_must_not': []}, 'python  "i am lazy java')),
    ("python \"grym kodare\" +\"i am lazy",
     ({'phrases': ['grym kodare'], 'phrases_must': [], 'phrases_must_not': []}, 'python  +"i am lazy')),
    ("python \"grym kodare\" -\"i am lazy",
     ({'phrases': ['grym kodare'], 'phrases_must': [], 'phrases_must_not': []}, 'python  -"i am lazy'))
])
def test_extract_querystring_phrases_with_unbalanced_quotes(querystring, expected):
    result = QueryBuilder.extract_quoted_phrases(querystring)
    assert result == expected, f"got {result} but expected {expected}"


@pytest.mark.parametrize("collection_id, expected", [(["UdVa_jRr_9DE"],
                                                      {'bool': {'should': [{'term': {
                                                          'collections.concept_id.keyword': {
                                                              'boost': 2.0, 'value': 'UdVa_jRr_9DE'}}}, {
                                                          'term': {'collections.legacy_ams_taxonomy_id': {
                                                              'boost': 2.0, 'value': 'UdVa_jRr_9DE'}}}]}}),
                                                     (["-UdVa_jRr_9DE"],
                                                      {'bool': {
                                                          'must_not': [{'term': {'collections.concept_id.keyword': {
                                                              'value': 'UdVa_jRr_9DE'}}}, {
                                                              'term': {'collections.legacy_ams_taxonomy_id': {
                                                                  'value': 'UdVa_jRr_9DE'}}}]}}),
                                                     ([None], None), ([[]], None),
                                                     (["None_existing_concept_id"], {
                                                         'bool': {
                                                             'should': [{'term': {'collections.concept_id.keyword': {
                                                                 'boost': 2.0, 'value': 'None_existing_concept_id'}}}, {
                                                                 'term': {'collections.legacy_ams_taxonomy_id': {
                                                                     'boost': 2.0,
                                                                     'value': 'None_existing_concept_id'}}}]}})])
def test_build_occupation_collection_query(collection_id, expected):
    query_result = QueryBuilder.build_yrkessamlingar_query(collection_id)
    assert query_result == expected


@pytest.mark.parametrize("from_datetime", ["2018-09-28T00:00:00", '2018-09-28', '', None, [], ])
@pytest.mark.parametrize("to_datetime", ["2018-09-28T00:01", '2018-09-27', '', None, []])
def test_filter_timeframe(from_datetime, to_datetime):
    if not from_datetime and not to_datetime:  # from and to date are empty
        assert QueryBuilder._filter_timeframe(from_datetime, to_datetime) is None
        return
    if from_datetime and to_datetime:
        d = QueryBuilder._filter_timeframe(from_datetime, parser.parse(to_datetime))
        assert d['range']['publication_date']['gte'] == parser.parse(from_datetime).isoformat()
        assert d['range']['publication_date']['lte'] == parser.parse(to_datetime).isoformat()
        return
    if from_datetime:
        d = QueryBuilder._filter_timeframe(from_datetime, to_datetime)
        assert d['range']['publication_date']['gte'] == parser.parse(from_datetime).isoformat()
        return
    if to_datetime:
        d = QueryBuilder._filter_timeframe(from_datetime, parser.parse(to_datetime))
        assert d['range']['publication_date']['lte'] == parser.parse(to_datetime).isoformat()

def current_year():
    now = datetime.datetime.now()
    current_year = now.strftime('%Y')
    return current_year

@pytest.mark.parametrize("to_datetime, expected", [
    ('2018-09-28T00:01', '2018-09-28T00:01:00'),
    ('2018-09-28T00:03:00', '2018-09-28T00:03:00'),
    ('2018-09-28T00', '2018-09-28T00:00:00'),
    ('2018-09-27', '2018-09-27T00:00:00'),
    ('2018-12', f'2018-12-{current_month_and_day()[1]}T00:00:00'),
    (f'{current_year()}-{current_month_and_day()[0]}', f'{current_year()}-{current_month_and_day()[0]}-{current_month_and_day()[1]}T00:00:00'),
])
def test_filter_time_frame_to(to_datetime, expected):
    """
    Test to show the current behavior where the last day is not included since hh:mm:ss will be formatted as 00:00:00
    """
    d = QueryBuilder._filter_timeframe("1970-01-01T00:00:00", parser.parse(to_datetime))
    actual = d['range']['publication_date']['lte']
    assert actual == expected


@pytest.mark.parametrize("start_seasonal_date, end_seasonal_date", [
    ('02-10', '02-11'),
])
def test_add_years_timeframe(start_seasonal_date, end_seasonal_date):
    this_year = datetime.datetime.now().year
    years = [year for year in range(settings.HISTORICAL_START_YEAR, this_year + 1)]
    expects = [{'range': {'publication_date': {
        'gte': format_start_date_historical(str(year) + '-' + start_seasonal_date),
        'lte': format_end_date_historical(str(year) + '-' + end_seasonal_date)
    }}} for year in years]
    q = QueryBuilder._add_years_timeframe(start_seasonal_date, end_seasonal_date)
    actuals = q['bool']['should']
    for actual, expect in zip(actuals, expects):
        assert actual, expect
