import asyncio
import logging
from asyncio import AbstractEventLoop
from enum import Enum

import requests
from flashtext.keyword import KeywordProcessor

from common import settings
from common.utils.decorators import singleton

log = logging.getLogger(__name__)


class SynonymType(Enum):
    COMPETENCE = 'COMPETENCE'
    TRAIT = 'TRAIT'
    GEO = 'GEO'
    OCCUPATION = 'OCCUPATION'


@singleton
class SynonymDictionary(object):
    all_jae_terms = None
    handled_term_types = [SynonymType.COMPETENCE, SynonymType.OCCUPATION, SynonymType.GEO, SynonymType.TRAIT]

    def __init__(self, stoplist=None):

        self.jae_api_host_url = settings.JAE_API_URL

        self.synonym_dictionary_endpoint = f'{self.jae_api_host_url}/synonymdictionary'

        if stoplist is None:
            stoplist = []
        self.stoplist = stoplist

        self.include_misspelled = True

        self.concept_to_term = {}
        self.keyword_processor = KeywordProcessor()
        self.init_keyword_processor(self.keyword_processor)
        self.init_synonym_dictionary(self.keyword_processor)

    def __len__(self):
        return len(self.get_keyword_processor())

    def misspelled_predicate(self, value, include_misspelled):
        if not include_misspelled and value['term_misspelled']:
            return False
        return True

    def get_filtered_synonym_dictionary_terms(self):
        synonyms = asyncio.run(self.fetch_jae_synonyms_multi_threaded())
        return (termobj for termobj in synonyms if termobj['term'] not in self.stoplist)

    def init_synonym_dictionary(self, keyword_processor):
        for concept_obj in self.get_filtered_synonym_dictionary_terms():
            keyword_processor.add_keyword(concept_obj['term'], concept_obj)
            concept_preferred_label = concept_obj['concept'].lower()
            if concept_preferred_label not in self.concept_to_term:
                self.concept_to_term[concept_preferred_label] = []
            self.concept_to_term[concept_preferred_label].append(concept_obj)

    def add_concept(self, term, concept_term, term_type):
        term = term.lower() if term else ""
        concept_term = concept_term.lower() if concept_term else ""
        if term and concept_term and not self.keyword_processor.get_keyword(term):
            log.debug(f'Adding term: {term} and concept term: {concept_term} of type: {term_type}')
            concept_obj = {'term': term, 'concept': concept_term.capitalize(), 'type': term_type}
            self.keyword_processor.add_keyword(term, concept_obj)

            if concept_term not in self.concept_to_term:
                self.concept_to_term[concept_term] = []
            self.concept_to_term[concept_term].append(concept_obj)

    @staticmethod
    def init_keyword_processor(keyword_processor):
        [keyword_processor.add_non_word_boundary(token) for token in list('åäöÅÄÖ()-*')]

    def get_keyword_processor(self):
        return self.keyword_processor

    def get_concepts(self, text, concept_type=None, include_misspelled=True, span_info=False):
        concepts = self.get_keyword_processor().extract_keywords(text, span_info=span_info)
        if concept_type is not None:
            if span_info:
                concepts = list(filter(lambda concept: concept[0]['type'] == concept_type, concepts))
            else:
                concepts = list(filter(lambda concept: concept['type'] == concept_type, concepts))

        concepts = [termobj for termobj in concepts if self.misspelled_predicate(termobj, include_misspelled)]

        return concepts

    def right_replace(self, s, old, new):
        occurrence = 1
        li = s.rsplit(old, occurrence)
        return new.join(li)

    def get_all_terms(self):
        return self.all_jae_terms

    def get_terms_by_type(self, term_type):
        return [term_obj for term_obj in self.all_jae_terms if self.all_jae_terms and term_obj['type'] == term_type]

    async def fetch_jae_synonyms_multi_threaded(self):

        async_tasks = []
        for type_name in self.handled_term_types:
            async_tasks.append(self.call_synonym_dictionary_endpoint(type_name.value))

        synonyms = await asyncio.gather(*async_tasks)
        # Synonyms is a list of four lists. Flatten this to one list.
        all_terms = [item for sublist in synonyms for item in sublist]
        self.all_jae_terms = all_terms
        log.info(f'Loaded {len(self.all_jae_terms)} synonym items')
        return all_terms

    async def call_synonym_dictionary_endpoint(self, synonym_type: str) -> list:
        try:
            log.info(f"Fetching {synonym_type}")
            loop = self.get_or_create_eventloop()
            payload = {'type': synonym_type, 'spelling': 'BOTH'}
            res = await loop.run_in_executor(None, requests.get, self.synonym_dictionary_endpoint, payload)
            return res.json()['items']
        except Exception as ce:
            log.critical(f"Failed to fetch synonyms from API. {ce}")

    def get_or_create_eventloop(self) -> AbstractEventLoop:
        try:
            return asyncio.get_event_loop()
        except RuntimeError as ex:
            if "There is no current event loop in thread" in str(ex):
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                return asyncio.get_event_loop()
