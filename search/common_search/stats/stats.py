import logging
import time
import json

from flask_restx import abort

from common import constants, settings
from common.main_elastic_client import elastic_client
from common.elastic_connection_with_retries import elastic_search_with_retry
from search.common_search.stats.marshal_stats import _marshal_stats_results
from search.common_search.stats.querybuilder_stats import create_historical_stats_aggs_query

log = logging.getLogger(__name__)


def get_stats(args):
    start_time = int(time.time() * 1000)

    taxonomy_type = args.get('taxonomy-type', [])
    type_list = [type.strip() for type in taxonomy_type if type in constants.TAXONOMY_TYPE_LIST] if taxonomy_type else constants.TAXONOMY_TYPE_LIST
    stats_by = args.get('stats-by')
    limit = args.get('limit', 10000)

    query_dsl = {
        'size': 0,
        'aggs': {type: create_historical_stats_aggs_query(type, stats_by, limit) for type in type_list}
    }
    response = elastic_search_with_retry(client=elastic_client(), query=query_dsl, index=settings.ES_INDEX)

    if not response:
        abort(500, 'Failed to establish connection to database')
        return

    log.info(f"ARGS: {args}")
    log.info(f"QUERY: {json.dumps(query_dsl)}")

    aggregations = response.get('aggregations', {})
    stats = _marshal_stats_results(stats_by, type_list, aggregations, limit)
    result_time = int(time.time() * 1000 - start_time)
    log.debug(f"(get_stats) Elastic results after: {result_time} milliseconds.")
    return {
        "query_time_in_millis": response.get("took"),
        "result_time_in_millis": result_time,
        "stats": stats}
