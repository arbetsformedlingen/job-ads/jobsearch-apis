# New endpoint in JobStream: /feed (rss/Atom)

There is a **beta version** of a new endpoint in JobStream f�r showing ads  in [Atom-format](https://en.wikipedia.org/wiki/Atom_%28Web_standard%29)
The accepted parameters are the same as in JobStream. Only the `date` param ('updated after') is mandatory.

## Important differences:
- Response is xml, not json
- Only selected fields from the ad is part of the xml feed. 
- The description text is truncated.


## xml response:
```
<?xml version='1.0' encoding='UTF-8'?>
  <feed xmlns="http://www.w3.org/2005/Atom">
    <id>https://jobstream.api.jobtechdev.se/feed</id>
    <title>JobStream Feed</title>
    <updated>2022-05-13T10:19:58.394000+00:00</updated>
    <link href="https://jobstream.api.jobtechdev.se" rel="self"/>
    <generator uri="https://jobstream.api.jobtechdev.se" version="1.27.0">JobTech</generator>
    <subtitle>Realtidsdata fr�n alla jobbannonser som �r publicerade hos Arbetsf�rmedlingen</subtitle>

    <entry>
      <id>https://jobsearch.api.jobtechdev.se/ad/26019489</id>
      <title>K�ksbitr�den till centrala Stockholm</title>
      <updated>2022-05-13T10:19:58.394000+00:00</updated>
      <content type="html">Zynca s�ker f�r kunds r�kning k�ksbitr�den till deras nya restaurang. Vi s�ker dig som redan �r eller vill bli k�ksbitr�de och ta plats i ett k�k p� en helt fantastisk streetfood restaurang, Du �lskar mat och f�rst�r vikten av kvalitet och mathantverk. Som k�ksbitr�de hos oss kommer du jobba i ett h�gt tempo och d�rf�r �r det extra viktigt att du f�rst�r att kommunikation �r avg�rande f�r ett smidigt och roligt teamwork....
</content>
<published>2022-05-03T08:43:02+00:00</published>
</entry>

<entry>
Repeated for each ad
</entry>  

    </feed>
```
Note that the `id` in the xml feed is the url to the ad/id enpoint in JobSearch where you can collect the entire ad in json-format.

## Feedback and next steps
This is a beta version and an experiment to see if there is a need for this kind of data. If you have feedback, please let us know in our [forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/20)
We will evaluate usage during the autumn of 2022 and decide if we will keep the endpoint or not, and in which ways it can be improved. 
