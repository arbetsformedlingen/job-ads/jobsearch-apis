import pytest

from tests.test_resources.concept_ids.taxonomy_replace.replace_by_dict import employment_types_as_list_of_dict
from tests.test_resources.helper import get_search

EMPLOYMENT_TYPE = "employment-type"



@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.parametrize("replaced_by_info", employment_types_as_list_of_dict)
def test_employment_type_old( replaced_by_info):
    """
    Search for employment type using old concept id and check
    that any hits has either old or 'replaced by' concept id as employment type
    """
    replaced_by = replaced_by_info['replaced_by']
    old = replaced_by_info['old']
    response = get_search(params={EMPLOYMENT_TYPE: old, 'limit': 100})
    assert (hits := response['hits']), "no hits"
    for hit in hits:
        assert isinstance(hit['employment_type'], dict)
        employment_type = hit['employment_type']['concept_id']
        assert employment_type == old or employment_type == replaced_by


@pytest.mark.jobsearch
@pytest.mark.parametrize("replaced_by_info", employment_types_as_list_of_dict)
def test_employment_type_replaced_by( replaced_by_info):
    """
    Search for employment type using 'replaced by' concept id and check
    that any hits has either old or 'replaced by' concept id as employment type
    """
    replaced_by = replaced_by_info['replaced_by']
    old = replaced_by_info['old']
    response = get_search(params={EMPLOYMENT_TYPE: replaced_by, 'limit': 100})
    assert (hits := response['hits']), "no hits"
    for hit in hits:
        assert isinstance(hit['employment_type'], dict)
        employment_type = hit['employment_type']['concept_id']
        assert employment_type == old or employment_type == replaced_by
