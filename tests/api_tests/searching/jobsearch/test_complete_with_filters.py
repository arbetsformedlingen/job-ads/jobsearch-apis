import pytest

from tests.test_resources.helper import get_complete
from tests.test_resources.concept_ids import concept_ids_geo as geo

# mark all tests in this file as @pytest.mark.jobsearch
pytestmark = [pytest.mark.jobsearch]

@pytest.mark.parametrize('query', ['sjukköt', 'sjuksköt', 'servit', 'srvitr', 'progr', 'prgram'])
def test_different_numbers_with_filter( query):
    """
    Check that number of hits is lower when using a geographical filter and a misspelled word
    """
    json_response_no_filter = get_complete(params={'q': query})
    top_value_no_filter = json_response_no_filter['typeahead'][0]['occurrences']
    top_word_no_filter = json_response_no_filter['typeahead'][0]['value']

    json_response_municipality_filter = get_complete(params={'q': query, 'municipality': geo.stockholm})
    top_value_municipality_filter = json_response_municipality_filter['typeahead'][0]['occurrences']
    top_word_municipality_filter = json_response_municipality_filter['typeahead'][0]['value']

    json_response_region_filter = get_complete(params={'q': query, 'region': geo.stockholms_lan})
    top_value_region_filter = json_response_region_filter['typeahead'][0]['occurrences']
    top_word_regionfilter = json_response_region_filter['typeahead'][0]['value']

    msg_city = f"Query: {query}, no filter: {top_word_no_filter}: {top_value_no_filter}, geo filter: {top_word_municipality_filter}: {top_value_municipality_filter} "
    msg_region = f"Query: {query}, no filter: {top_word_no_filter}: {top_value_no_filter}, geo filter: {top_word_regionfilter}: {top_value_region_filter} "

    assert top_value_no_filter > top_value_municipality_filter, msg_city
    assert top_value_no_filter > top_value_region_filter, msg_region

    assert top_value_municipality_filter > 0
    assert top_value_region_filter > 0
