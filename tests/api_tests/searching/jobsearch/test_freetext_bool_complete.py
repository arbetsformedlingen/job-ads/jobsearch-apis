import pytest
import copy

from tests.test_resources.helper import get_complete, get_complete_with_headers
from common.constants import X_FEATURE_FREETEXT_BOOL_METHOD
from tests.test_resources.test_settings import test_headers

pytestmark = [pytest.mark.jobsearch]


@pytest.mark.parametrize("query",
                         ['stockholm sj', 'stor s', 'lärare st', 'special a', 'stockholm  ', '  ', 'programmering  '])
def test_complete_endpoint_with_freetext_bool_method( query):
    """
    test of /complete endpoint with X_FEATURE_FREETEXT_BOOL_METHOD set to 'and' / 'or' / default value
    Verifies that results are identical regardless of bool method
    """
    params = {'q': query, 'limit': 0}

    # no special header, default values are used
    result_default = get_complete(params)

    # use 'and'
    tmp_headers = copy.deepcopy(test_headers)
    tmp_headers[X_FEATURE_FREETEXT_BOOL_METHOD] = 'and'
    result_and = get_complete_with_headers( params, tmp_headers)
    # use 'or
    tmp_headers[X_FEATURE_FREETEXT_BOOL_METHOD] = 'or'
    result_or = get_complete_with_headers( params, tmp_headers)
    # check that results are identical regardless of which bool_method is used
    assert result_default['typeahead'] == result_and['typeahead'] == result_or['typeahead']
