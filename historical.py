import logging
from flask import Flask
from flask_cors import CORS
from jobtech.common.customlogging import configure_logging
from common import appconf, settings
from common.main_elastic_client import elastic_client
from search.rest.model.apis import historical_api
from search.rest.endpoint.historical import querybuilder_container
# Import all Resources that are to be made visible for the app
# PyCharm marks them as unused imports, but they are needed
# noinspection PyUnresolvedReferences
import search.rest.endpoint.historical

app = Flask(__name__)
CORS(app)
configure_logging([__name__.split('.')[0], 'jobtech', 'common', 'sokannonser'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')
log.info(f"Starting: {__name__}")

elastic_client.launch(es_timeout=settings.ES_HISTORICAL_TIMEOUT)
querybuilder_container.launch()

if __name__ == '__main__':
    # Used only when starting this script directly, i.e. for debugging
    appconf.initialize_app(app, historical_api)
    app.run(debug=True)
else:
    # Main entrypoint
    appconf.initialize_app(app, historical_api)
