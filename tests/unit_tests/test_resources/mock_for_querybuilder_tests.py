#  -*- coding: utf-8 -*-
from search.common_search.ads_labels import AdsLabels
from search.common_search.querybuilder import QueryBuilder, QueryBuilderType
from search.common_search.text_to_label import TextToLabel


class MockSynonymDictionary:
    def __init__(self):
        pass



class MockTextToConcept:
    def __init__(self):
        self._synonym_dictionary = MockSynonymDictionary()
        self._extracted_ads_locations = set()

    def text_to_concepts(self, text):
        skills = {
            "python": {
                "term": "python",
                "uuid": "0b6d3a08-3cc3-546d-b8ed-f2de299bafdb",
                "concept": "Python",
                "type": "KOMPETENS",
                "term_uuid": "f60fa7fd-00f7-5803-acd7-1a3eda170397",
                "term_misspelled": False,
                "plural_occupation": False,
                "definite_occupation": False,
                "version": "SYNONYM-DIC-2.0.1.25",
                "operator": ""
            },
            "java": {
                "term": "java",
                "uuid": "c965e8aa-751a-5923-97bd-b8bd6d5e813a",
                "concept": "Java",
                "type": "KOMPETENS",
                "term_uuid": "e3d2a75a-5717-56d2-ad8a-ee4b5baf8530",
                "term_misspelled": False,
                "plural_occupation": False,
                "definite_occupation": False,
                "version": "SYNONYM-DIC-2.0.1.25",
                "operator": "+"
            },
            "php": {
                "term": "php",
                "uuid": "3e3629d1-95f6-5b0e-8f5c-d6a709fd94e2",
                "concept": "Php",
                "type": "KOMPETENS",
                "term_uuid": "216af07e-d210-572f-8885-b13d79b80acc",
                "term_misspelled": False,
                "plural_occupation": False,
                "definite_occupation": False,
                "version": "SYNONYM-DIC-2.0.1.25",
                "operator": "-"
            },
            "rekryteringsutbildning": {
                "term": "rekryteringsutbildning",
                "uuid": "c6f4a0f7-b7d1-5792-ac8b-c9cc2eafdb1d",
                "concept": "Rekryteringsutbildning",
                "type": "KOMPETENS",
                "term_uuid": "d421b124-fe9b-5e17-94b6-615f4365941f",
                "term_misspelled": False,
                "plural_occupation": False,
                "definite_occupation": False,
                "version": "SYNONYM-DIC-2.0.1.25",
                "operator": ""
            }
        }
        occupations = {
            "systemutvecklare": {
                "term": "systemutvecklare",
                "uuid": "df9e7a73-2cc3-5b32-a84e-7e68a527e80e",
                "concept": "Systemutvecklare",
                "type": "YRKE",
                "term_uuid": "7296755c-acf2-5eed-9d4b-e4cd845cd05a",
                "term_misspelled": False,
                "plural_occupation": False,
                "definite_occupation": False,
                "version": "SYNONYM-DIC-2.0.1.25",
                "operator": ""
            }
        }
        response = {
            "skill": [],
            "occupation": [],
            "trait": [],
            "location": [],
            "skill_must": [],
            "occupation_must": [],
            "trait_must": [],
            "location_must": [],
            "skill_must_not": [],
            "occupation_must_not": [],
            "trait_must_not": [],
            "location_must_not": []
        }
        for word in text.split():
            if word.startswith("+"):
                word = word[1:]
                if word in skills:
                    response['skill_must'].append(skills[word])
                if word in occupations:
                    response['occupation_must'].append(occupations[word])
            elif word.startswith("-"):
                word = word[1:]
                if word in skills:
                    response['skill_must_not'].append(skills[word])
                if word in occupations:
                    response['occupation_must_not'].append(occupations[word])
            else:
                if word in skills:
                    response['skill'].append(skills[word])
                if word in occupations:
                    response['occupation'].append(occupations[word])

        return response

class MockAdsLabels(AdsLabels):

    def get_extracted_ads_labels(self):
        return ['nystartsjobb', 'rekryteringsutbildning', 'dummy_label', 'räksmörgås', 'weird_case_label']


mock_ads_labels = MockAdsLabels()

text_to_label = TextToLabel(ads_labels=mock_ads_labels)
mock_querybuilder_jobsearch = QueryBuilder(MockTextToConcept(), text_to_label=text_to_label)
mock_querybuilder_jobsearch.set_qb_type(QueryBuilderType.JOBSEARCH_SEARCH)

mock_querybuilder_complete = QueryBuilder(MockTextToConcept(), text_to_label=text_to_label)
mock_querybuilder_complete.set_qb_type(QueryBuilderType.JOBSEARCH_COMPLETE)

mock_querybuilder_historical = QueryBuilder(MockTextToConcept(), text_to_label=text_to_label)
mock_querybuilder_historical.set_qb_type(QueryBuilderType.HISTORICAL_SEARCH)

mock_querybuilder_historical_stats = QueryBuilder(MockTextToConcept(), text_to_label=text_to_label)
mock_querybuilder_historical_stats.set_qb_type(QueryBuilderType.HISTORICAL_STATS)

all_query_builders = [mock_querybuilder_jobsearch, mock_querybuilder_historical, mock_querybuilder_complete,
                      mock_querybuilder_historical_stats]
